import datetime


##Starting in the top left corner of a 2×2 grid,
##and only being able to move to the right and down,
##there are exactly 6 routes to the bottom right corner.
##
##https://projecteuler.net/project/images/p015.png
##
##How many such routes are there through a 20×20 grid?

moves = {}
def recur(cur_x, cur_y):
    if (cur_x > 0 and cur_y > 0):
        if ((cur_x, cur_y) in moves.keys()):
            return moves[(cur_x, cur_y)]
        else: 
            total = recur(cur_x - 1, cur_y) + recur(cur_x, cur_y - 1)
            moves[(cur_x, cur_y)] = total
            return total
    else:
        return 1

##recur(0,0,2)

start = datetime.datetime.now()
result = recur(20,20)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
