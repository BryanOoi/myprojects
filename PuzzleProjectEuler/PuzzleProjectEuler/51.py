import itertools
import numpy
from bisect import bisect_left
import datetime

##Prime digit replacements
##
##Problem 51
##By replacing the 1st digit of the 2-digit number *3,
##it turns out that six of the nine possible values:
##    13, 23, 43, 53, 73, and 83, are all prime.
##
##By replacing the 3rd and 4th digits of 56**3 with the
##    same digit, this 5-digit number
##is the first example having seven primes among the
##    ten generated numbers, yielding the family:
##    56003, 56113, 56333, 56443, 56663, 56773, and 56993.
##Consequently 56003, being the first member of this family,
##        is the smallest prime with this property.
##
##Find the smallest prime which, by replacing part of the number
##        (not necessarily adjacent digits)
##with the same digit, is part of an eight prime value family.

##def isprime(n):
##    """Returns True if n is prime."""
##    n = int(n)
##    if n == 2:
##        return True
##    if n == 3:
##        return True
##    if n % 2 == 0:
##        return False
##    if n % 3 == 0:
##        return False
##
##    i = 5
##    w = 2
##
##    while i * i <= n:
##        if n % i == 0:
##            return False
##
##        i += w
##        w = 6 - w
##
##    return True
##
##test_arr = []
##for i in range(2, 1000):
##    if isprime(i):
##        test_arr.append(i)
##print(test_arr)

##def sieve(n):
##    primes = [2]
##    sieve_arr = list(range(3,10**n,2))
##    while len(sieve_arr) > 0:
##        primes.append(sieve_arr[0])
##        next_arr = []
##        for i in sieve_arr:
##            if (i % sieve_arr[0] != 0):
##                next_arr.append(i)
##        if len(sieve_arr) == len(next_arr):
##            break
##        else:
##            sieve_arr = list(next_arr)
##    return primes
power = 6
def prime_numbers(limit=10**power):
    '''Prime number generator. Yields the series
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29 ...
    using Sieve of Eratosthenes.
    '''
    yield 2
    sub_limit = int(limit**0.5)
    flags = [True, True] + [False] * (limit - 2)
    # Step through all the odd numbers
    for i in range(3, limit, 2):
        if flags[i]:
            continue
        yield i
        # Exclude further multiples of the current prime number
        if i <= sub_limit:
            for j in range(i*i, limit, i<<1):
                flags[j] = True

prime_list = list(prime_numbers())


def index(a, x):
    'Locate the leftmost value exactly equal to x'
    i = bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    return -1

def digit(num_str):
    count = 0
    prime_arr = []
    for i in range(10):
        new_num = num_str.replace('x',str(i))
##        if new_num[0] != '0' and isprime(new_num):
        if new_num[0] != '0' and index(prime_list, int(new_num)) != -1:
            count += 1
            prime_arr.append(new_num)
    return count, prime_arr

def generate(n):
    total_nums = []
    for i in range(1,10**n):
        number_of_digit = len(str(i))
        number_of_x = n - number_of_digit
        if (number_of_x > 0):
            permute_list = set(list(itertools.permutations(['x' for i in range(number_of_x)] + list(str(i)))))
            for permute in permute_list:
                if (permute[0] != '0'):
                    total_nums.append(''.join(permute))
    return set(total_nums)

def main_logic(n):
    all_list = generate(n)
    largest = 0
    final_arr = []
    for num in all_list:
        total, temp_arr = digit(num)
        if total > 7:
            print (temp_arr)


start = datetime.datetime.now()
result = main_logic(power)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)


    
