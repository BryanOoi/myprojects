import datetime
import re
from decimal import *


##Euler discovered the remarkable quadratic formula:
##
##n2+n+41
##
##It turns out that the formula will produce 40 primes for
##the consecutive integer values 0≤n≤39. However,
##when n=40,402+40+41=40(40+1)+41 is divisible by 41,
##and certainly when n=41,412+41+41 is clearly divisible by 41.
##
##The incredible formula n2−79n+1601 was discovered, which
##produces 80 primes for the consecutive values 0≤n≤79.
##The product of the coefficients, −79 and 1601, is −126479.
##
##Considering quadratics of the form:
##
##n2+an+b, where |a|<1000 and |b|≤1000
##
##where |n| is the modulus/absolute value of n
##e.g. |11|=11 and |−4|=4
##Find the product of the coefficients, a and b,
##for the quadratic expression that produces the maximum number
##of primes for consecutive values of n, starting
##with n=0


regex = re.compile(r'([0-9]+?)\1+')

def dec(limit):
    max_cycle = 0
    max_int = 0
    max_a = 0
    for i in range(1, 1+limit):
        a = float(1/i)
        if len(str(a)) > 15:
            a = Decimal(1) / Decimal(i)
            a = str(a).lstrip('0')[2:]
            match = regex.findall(a)
            if len(match) > 0:
                tot = max([len(re) for re in match])
                if tot > max_cycle:
                    max_cycle = tot
                    max_int = i
    return max_int, max_cycle

start = datetime.datetime.now()
result = dec(1000)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
