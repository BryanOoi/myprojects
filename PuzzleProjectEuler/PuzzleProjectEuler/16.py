import datetime


##2^^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
##
##What is the sum of the digits of the number 2^^1000?

a = str(2**1000)
total = 0
for num in a:
    total += int(num)

print(total)

##recur(0,0,2)

##start = datetime.datetime.now()
##result = recur(20,20)
##end = datetime.datetime.now()
##elapsed = end - start
##print(elapsed)
##print(result)

    
