## Smallest multiple

##2520 is the smallest number that can be divided by each of the numbers
##from 1 to 10 without any remainder.
##
##What is the smallest positive number that is evenly divisible by all
##of the numbers from 1 to 20?

def smallest(n):
    count = n
    while True:
        if div(count, n):
            return count
        count += n

def div(num, n):
    result = True
    for i in range(1, n):
        if (num % i != 0):
            result = False
            break
    return result

print(smallest(20))

    
