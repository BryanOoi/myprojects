import datetime


##By starting at the top of the triangle below and moving to
##adjacent numbers on the row below, the maximum total from top to bottom is 23.
##
##   3
##  7 4
## 2 4 6
##8 5 9 3
##
##That is, 3 + 7 + 4 + 9 = 23.
##
##Find the maximum total from top to bottom of the triangle below:
##
##                            75
##                          95  64
##                        17  47  82
##                      18  35  87  10
##                    20  04  82  47  65
##                  19  01  23  75  03  34
##                88  02  77  73  07  63  67
##              99  65  04  28  06  16  70  92
##            41  41  26  56  83  40  80  70  33
##          41  48  72  33  47  32  37  16  94  29
##        53  71  44  65  25  43  91  52  97  51  14
##      70  11  33  28  77  73  17  78  39  68  17  57
##    91  71  52  38  17  14  91  43  58  50  27  29  48
##  63  66  04  68  89  53  67  30  73  16  69  87  40  31
##04  62  98  27  23  09  70  98  73  93  38  53  60  04  23
##
##NOTE: As there are only 16384 routes, it is possible to
##solve this problem by trying every route. However, Problem 67,
##is the same challenge with a triangle
##containing one-hundred rows; it cannot be solved by brute force,
##and requires a clever method! ;o)
        
  
full= """59
73 41
52 40 09
26 53 06 34
10 51 87 86 81
61 95 66 57 25 68
90 81 80 38 92 67 73
30 28 51 76 81 18 75 44
84 14 95 87 62 81 17 78 58
21 46 71 58 02 79 62 39 31 09
56 34 35 53 78 31 81 18 90 93 15
78 53 04 21 84 93 32 13 97 11 37 51
45 03 81 79 05 18 78 86 13 30 63 99 95
39 87 96 28 03 38 42 17 82 87 58 07 22 57
06 17 51 17 07 93 09 07 75 97 95 78 87 08 53"""


row_list = []
filename = "triangle.txt"
with open(filename) as f:
    for lines in f:
        clean = lines.strip()
        row_list.append(clean.split(' '))

const_depth = 14
top_depth = 15

moves_list = []

def increment_binary_string(s):
    return 

def func(x): 
    return x[0]

def fill_moves_list(depth):
    if depth < 1:
        return
    x = ''.join(['0' for i in range(depth)])
    global moves_list
    moves_list = []
    for _ in range(2**depth):
        moves_list.append(x)
        x = ('{:0' + str(depth) + 'b}').format(1 + int(x, 2))


max_row = len(row_list) - 1
def pyra(row, col):
    top_count = []
    for i in range(top_depth):
        top_count.append([0,0,''])

    for move in moves_list:
        total = int(row_list[row][col])
        move_str = row_list[row][col]
        move_num = 0
        ycount = 1 + row
        xcount = col
        for num in move:
            xcount += int(num)
            total += int(row_list[ycount][xcount])
            move_num = row_list[ycount][xcount]
            move_str += " " + row_list[ycount][xcount]
            ycount += 1
        if total > top_count[0][0]:
            top_count[0] = [total, xcount, move_str]
            top_count.sort(key = func)
    return top_count


def fill_branch():
    fill_moves_list(const_depth)
    const_row = 0
    branches = [[0, 0, '59']]
    next_branch = []
    flag = False
    while True:
        if const_row >= max_row:
            break
        temp_branch_2 = []
        next_branch_2 = []
        
        for branch in branches:
            const_num_index = branch[1]
            top_count = pyra(const_row, const_num_index)
            temp_branch = []

            for i in range(len(top_count)-1,0,-1):
                if top_count[i][1] not in temp_branch:
                    temp_branch.append(top_count[i][1])
                    top_count[i][0] += branch[0]
                    top_count[i][2] = branch[2] + top_count[i][2][2:]
                    next_branch.append(top_count[i])

        const_row += const_depth
        
        if const_row + const_depth > max_row:
            fill_moves_list(max_row - const_row)            
        
        branches = next_branch
        next_branch = []

        branches.sort(key = func)
        branches = list(reversed(branches))

        for branch in branches:
            if branch[1] not in temp_branch_2:
                temp_branch_2.append(branch[1])
                next_branch_2.append(branch)
        branches = next_branch_2
        next_branch_2 = []
        
    a = branches[0][2]

    b = a.split()

    return sum([int(x) for x in b])
    #### Honestly i got the answer by luck.
    ### Changing const_depth from 1 to 15 all gives different answer,
    ####    so I picked the highest one. Calculation for branches[0][0] is wrong,
    ## could contribute to why the answers are wrong/ inconsisten
    





start = datetime.datetime.now()
result = fill_branch()
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
