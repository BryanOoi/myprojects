import datetime
import re
from decimal import *


##A unit fraction contains 1 in the numerator. The decimal
##representation of the unit fractions with denominators 2 to 10 are given:
##
##1/2	= 	0.5
##1/3	= 	0.(3)
##1/4	= 	0.25
##1/5	= 	0.2
##1/6	= 	0.1(6)
##1/7	= 	0.(142857)
##1/8	= 	0.125
##1/9	= 	0.(1)
##1/10	= 	0.1
##Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle.
##It can be seen that 1/7 has a 6-digit recurring cycle.
##
##Find the value of d < 1000 for which 1/d contains the longest
##recurring cycle in its decimal fraction part.

getcontext().prec = 5000
regex = re.compile(r'([0-9]+?)\1+')

def dec(limit):
    max_cycle = 0
    max_int = 0
    max_a = 0
    for i in range(1, 1+limit):
        a = float(1/i)
        if len(str(a)) > 15:
            a = Decimal(1) / Decimal(i)
            a = str(a).lstrip('0')[2:]
            match = regex.findall(a)
            if len(match) > 0:
                tot = max([len(re) for re in match])
                if tot > max_cycle:
                    max_cycle = tot
                    max_int = i
    return max_int, max_cycle

start = datetime.datetime.now()
result = dec(1000)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
