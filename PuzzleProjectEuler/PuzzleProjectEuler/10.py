import datetime
import math

##The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
##
##Find the sum of all the primes below two million.


def isprime(n):
    """Returns True if n is prime."""
    n = int(n)
    if n == 2:
        return True
    if n == 3:
        return True
    if n % 2 == 0:
        return False
    if n % 3 == 0:
        return False

    i = 5
    w = 2

    while i * i <= n:
        if n % i == 0:
            return False

        i += w
        w = 6 - w

    return True

def getprime(nth):
    total = 2
    i = 3
    while nth > i:
        if isprime(i):
            total += i
        i += 2
    return total


start = datetime.datetime.now()
result = getprime(2000000)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)



    
