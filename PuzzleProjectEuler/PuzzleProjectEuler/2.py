def fib(max):
    first = 1
    second = 2
    count = second
    while second < max:
        new = first + second
        first = second
        second = new
        if (second < max and second % 2 == 0):
            count += second
    print(count)

fib(4000000)
    
