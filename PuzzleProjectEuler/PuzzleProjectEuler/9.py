import datetime
import math

##Special Pythagorean triplet
##
##Problem 9
##A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
##
##a**2 + b**2 = c**2
##For example, 3**2 + 4**2 = 9 + 16 = 25 = 5**2.
##
##There exists exactly one Pythagorean triplet for which a + b + c = 1000.
##Find the product abc.


def num_set(n):
    a = math.floor(n/3)
    b = math.floor(n/3)
    c = math.ceil(n/3)
    total_set = []
    for i in range(c, n-2):
        left = n-i
        for j in range(1,math.floor(left/2) + 1):
            new_a = j
            new_b = left - new_a
            if new_a < i and new_b < i:
                total_set.append((new_a, new_b, i))
    
    return total_set

def triplet(n):
    num_ls = num_set(n)
    for tup in num_ls:
        if tup[0]**2 + tup[1]**2 == tup[2]**2:
            return tup[0] * tup[1] * tup[2]

    
start = datetime.datetime.now()
result = triplet(1000)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)



    
