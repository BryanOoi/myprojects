import itertools
from bisect import bisect_left

##Poker hands
##
##Problem 54
##In the card game poker, a hand consists of five cards and are ranked, from lowest to highest,
##in the following way:
##
##High Card: Highest value card.
##One Pair: Two cards of the same value.
##Two Pairs: Two different pairs.
##Three of a Kind: Three cards of the same value.
##Straight: All cards are consecutive values.
##Flush: All cards of the same suit.
##Full House: Three of a kind and a pair.
##Four of a Kind: Four cards of the same value.
##Straight Flush: All cards are consecutive values of same suit.
##Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
##The cards are valued in the order:
##2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
##
##If two players have the same ranked hands then the rank made up of the highest value wins;
##for example, a pair of eights beats a pair of fives (see example 1 below).
##But if two ranks tie, for example, both players have a pair of queens,
##then highest cards in each hand are compared (see example 4 below);
##if the highest cards tie then the next highest cards are compared, and so on.
##
##Consider the following five hands dealt to two players:
##
##Hand	 	Player 1	 	Player 2	 	Winner
##1	 	5H 5C 6S 7S KD
##Pair of Fives
## 	2C 3S 8S 8D TD
##Pair of Eights
## 	Player 2
##2	 	5D 8C 9S JS AC
##Highest card Ace
## 	2C 5C 7D 8S QH
##Highest card Queen
## 	Player 1
##3	 	2D 9C AS AH AC
##Three Aces
## 	3D 6D 7D TD QD
##Flush with Diamonds
## 	Player 2
##4	 	4D 6S 9H QH QC
##Pair of Queens
##Highest card Nine
## 	3D 6D 7H QD QS
##Pair of Queens
##Highest card Seven
## 	Player 1
##5	 	2H 2D 4C 4D 4S
##Full House
##With Three Fours
## 	3C 3D 3S 9S 9D
##Full House
##with Three Threes
## 	Player 1
##The file, poker.txt, contains one-thousand random hands dealt to two players.
##Each line of the file contains ten cards (separated by a single space):
##    the first five are Player 1's cards and the last five are Player 2's cards.
##    You can assume that all hands are valid (no invalid characters or repeated cards),
##    each player's hand is in no specific order, and in each hand there is a clear winner.
##
##How many hands does Player 1 win?

##filename = "poker.txt"
##with open(filename) as f:
##    for line in f:


def check_straight(hand):
    num_hand = list(map(int, hand))
    chk1 = num_hand == list(range(min(num_hand), max(num_hand)+1))
    if not chk1 and 14 in num_hand:
        num_hand = [1 if x==14 else x for x in num_hand]
        return num_hand == list(range(min(num_hand), max(num_hand)+1))
    else:
        return chk1

def check_flush(hand):
   return len(set(hand)) <= 1

dic_num = {'2':'2', '3':'3', '4':'4', '5':'5', '6':'6', '7':'7', '8':'8', '9':'9', 'T':'10', 'J':'11', 'Q':'12', 'K':'13', 'A':'14'}
dic_suit = {'D':'1', 'C':'2', 'H':'3', 'S':'4'}
scoring_num = ['1,1,1,1,1' , '2,1,1,1' , '2,2,1', '3,1,1', '','', '3,2', '4,1']

hand1_wins = 0
hand2_wins = 0

filename = "poker.txt"
with open(filename) as f:
    for item in f:

        cards = item.split(' ')
        hand1 = cards[0:5]
        hand2 = cards[5:10]
        hand1_num = sorted([dic_num[x[0]] for x in hand1])
        hand1_s = sorted([dic_suit[x[1]] for x in hand1])
        hand2_num = sorted([dic_num[x[0]] for x in hand2])
        hand2_s = sorted([dic_suit[x[1]] for x in hand2])
        hand1_num_group = []
        hand2_num_group = []

        highest_two_1 = []
        highest_two_2 = []
        for key, group in itertools.groupby(hand1_num):
            ls1 = list(group)
            highest_two_1.append([len(ls1), int(key)])
            hand1_num_group.append(ls1)
        for key, group in itertools.groupby(hand2_num):
            ls2 = list(group)
            highest_two_2.append([len(ls2), int(key)])
            hand2_num_group.append(ls2)

        hand1_pattern = sorted([str(len(x)) for x in hand1_num_group])
        hand1_score = 0
        for i in range(len(scoring_num)):
            if sorted(scoring_num[i].split(',')) == hand1_pattern:
                hand1_score = i

        if check_straight(hand1_num):
            hand1_score = 4
        if check_flush(hand1_s):
            hand1_score = 5
        if check_straight(hand1_num) and check_flush(hand1_s):
            hand1_score = 8
        if check_straight(hand1_num) and check_flush(hand1_s) and hand1_num[0] == '10':
            hand1_score = 9
        
        hand2_pattern = sorted([str(len(x)) for x in hand2_num_group])
        hand2_score = 0
        for i in range(len(scoring_num)):
            if sorted(scoring_num[i].split(',')) == hand2_pattern:
                hand2_score = i    

        if check_straight(hand2_num):
            hand2_score = 4
        if check_flush(hand2_s):
            hand2_score = 5
        if check_straight(hand2_num) and check_flush(hand2_s):
            hand2_score = 8
        if check_straight(hand2_num) and check_flush(hand2_s) and hand2_num[0] == '10':
            hand2_score = 9

        hand1_win_now = False
        if hand1_score > hand2_score:
            hand1_wins += 1
            hand1_win_now = True
        elif hand2_score > hand1_score:
            hand2_wins += 1
        elif hand2_score == hand1_score:
            tie1 = sorted(highest_two_1, reverse=True)
            tie2 = sorted(highest_two_2, reverse=True)
            if tie1[0][1] > tie2[0][1]:
                hand1_wins += 1
                hand1_win_now = True
            elif tie2[0][1] > tie1[0][1]:
                hand2_wins += 1
            else:
                if tie1[1][1] > tie2[1][1]:
                    hand1_wins += 1
                    hand1_win_now = True
                elif tie2[1][1] > tie1[1][1]:
                    hand2_wins += 1
                else:
                    print("TOTAL TIE ", hand1 + hand2)

print(hand1_wins)
print(hand2_wins)
##    print(hand1, hand2)
##    print(hand1_score, hand2_score)
##    if hand1_win_now:
##        print("HAND 1 WINS")
##    else:
##        print("HAND 2 WINS")

        
##    if (hand1_score == hand2_score):
##        print(hand1, hand2)
##        print(hand1_score, hand2_score)
##        print(sorted(highest_two_1, reverse=True))
##        print(sorted(highest_two_2, reverse=True))



