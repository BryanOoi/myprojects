##A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
##
##Find the largest palindrome made from the product of two 3-digit numbers.

def ispalin(num_str):
    if len(num_str) == 0 or len(num_str) == 1:
        return True
    if (num_str[0] != num_str[len(num_str)-1]):
        return False
    return ispalin(num_str[1:len(num_str)-1])

def palin(n):
    palin_list = []
    last = 0
    for i in range(10**(n-1), 10**n):
        for j in range(10**(n-1), 10**n):
            if ispalin(str(i*j)):
                palin_list.append(i*j)
    print(max(palin_list))    

palin(3)
    
