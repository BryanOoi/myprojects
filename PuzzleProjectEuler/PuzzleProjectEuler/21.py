import datetime


##Let d(n) be defined as the sum of proper divisors of n
##(numbers less than n which divide evenly into n).
##If d(a) = b and d(b) = a, where a ≠ b, then a and b are an
##amicable pair and each of a and b are called amicable numbers.
##
##For example, the proper divisors of 220 are 1, 2, 4, 5, 10,
##11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors
##of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
##
##Evaluate the sum of all the amicable numbers under 10000.

def tri(n):
    return int((n*(n+1))/2)

def div(num):
    till = num
    total_divs = 2
    count = 2
    all_d = [1]
    while count < till:
        if num % count == 0:
            total_divs += 2
            till = int(num/count)
            all_d.append(count)
            all_d.append(int(num/count))
        count += 1
    return sum(all_d)

def res(total):
    dic = {}
    ami = []
    for i in range(total):
        dic[i] = div(i)
    all_keys = dic.keys()

    for key in all_keys:
        if (key not in ami and dic[key] in all_keys and key != dic[key] and key == dic[dic[key]] ):
            ami.append(key)
            ami.append(dic[key])
    return (ami)


start = datetime.datetime.now()
result = sum(res(10000))
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
