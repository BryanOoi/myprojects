import datetime


##If the numbers 1 to 5 are written out in words: one, two, three,
##four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
##
##If all the numbers from 1 to 1000 (one thousand) inclusive were
##written out in words, how many letters would be used?
##
##
##NOTE: Do not count spaces or hyphens. For example, 342
##(three hundred and forty-two) contains 23 letters and 115
##(one hundred and fifteen) contains 20 letters.
##The use of "and" when writing out numbers is in compliance with British usage.

##basic = {1:3, 2:3, 3:5, 4:4, 5:4, 6:3, 7:5, 8:5,
##         9:4, 10:3, 11:6, 12:6, 13:8, 14:8, 15:7, 16:7, 17:9, 18:8, 19:8}
##compound = {20:6, 30:6, 40:5, 50:5, 60:5, 70:7, 80:7, 90:6, 100: 7, 1000:8}
##
##
##def letter_count(n):
##    if (n > 1000):
##        return 0
##    global compound
##    num = str(n)
##    if len(num) == 1:
##        return basic[int(num)]
##    elif len(num) == 2 and num[0] == '1':
##        return basic[int(num)]
##    elif len(num) == 2 and num[0] != '1':
##        if (int(num) in compound.keys()):
##            return compound[int(num)]
##        else:
##            total = compound[int(num[0] + '0')] + letter_count(int(num[1]))
##            compound[int(num)] = total
##            return total
##    elif len(num) == 3:
##        if (int(num) in compound.keys()):
##            return compound[int(num)]
##        else:
##            subtotal = 0
##            if (num[1:] != '00'):
##                subtotal = 3 + letter_count(int(num[1:]))
##            total = basic[int(num[0])] + compound[100] + subtotal
##            compound[int(num)] = total
##            return total
##    elif len(num) == 4:
##        return 3 + compound[int(num)]
##
##def count_loop(limit):
##    total = 0
##    for i in range(limit):
##        total += letter_count(i + 1)
##    return total
        

basic = {1:'one', 2:'two', 3:'three', 4:'four', 5:'five', 6:'six', 7:'seven', 8:'eight',
         9:'nine', 10:'ten', 11:'eleven', 12:'twelve', 13:'thirteen', 14:'fourteen', 15:'fifteen',
         16:'sixteen', 17:'seventeen', 18:'eighteen', 19:'nineteen'}
compound = {20:'twenty', 30:'thirty', 40:'forty', 50:'fifty', 60:'sixty', 70:'seventy', 80:'eighty', 90:'ninety',
            100: 'hundred', 1000:'thousand'}

def letter_count(n):
    if (n > 1000):
        return 0
    global compound
    num = str(n)
    if len(num) == 1:
        return basic[int(num)]
    elif len(num) == 2 and num[0] == '1':
        return basic[int(num)]
    elif len(num) == 2 and num[0] != '1':
        if (int(num) in compound.keys()):
            return compound[int(num)]
        else:
            total = compound[int(num[0] + '0')] + letter_count(int(num[1]))
            compound[int(num)] = total
            return total
    elif len(num) == 3:
        if (num == '100'):
            return 'onehundred'
        if (int(num) in compound.keys()):
            return compound[int(num)]
        else:
            subtotal = ""
            if (num[1:] != '00'):
                subtotal = 'and' + letter_count(int(num[1:]))
            total = basic[int(num[0])] + compound[100] + subtotal
            compound[int(num)] = total
            return total
    elif len(num) == 4:
        return 'one' + compound[int(num)]

def count_loop(limit):
    totalstring = ""
    for i in range(limit):
        totalstring += letter_count(i + 1)
    return totalstring


start = datetime.datetime.now()
result = len(count_loop(1000))
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
