import datetime


##You are given the following information,
##but you may prefer to do some research for yourself.
##
##1 Jan 1900 was a Monday.
##Thirty days has September,
##April, June and November.
##All the rest have thirty-one,
##Saving February alone,
##Which has twenty-eight, rain or shine.
##And on leap years, twenty-nine.
##A leap year occurs on any year evenly divisible by 4,
##but not on a century unless it is divisible by 400.
##How many Sundays fell on the first of the month during
##the twentieth century (1 Jan 1901 to 31 Dec 2000)?
##        
  
function leapYear(year){
  let leap1 = year % 4 == 0;
  let leap2 = year % 100 != 0 || year % 400 == 0;
  let leap = leap1 && leap2;
  return leap
}

function numOfSun(){
  let thirty = [4,6,9,11];
  
  let startYear = 1901;
  let year = startYear;
  const endYear = 2001;
  let totalMonths = 12*(endYear - startYear);
  let month = 1;
  let dayName = 2; //0-6
  let sunCount = 0;

  for (let count=0;count<totalMonths;count++){
    let endDay = 31;
    if (month == 2){
      if (leapYear(year)){
        endDay = 29;
      }
      else{
        endDay = 28;
      }
    }
    else if (thirty.includes(month)){
      endDay = 30;
    }

    dayName += (endDay % 7)
    dayName = dayName % 7

    if (dayName == 6){
      sunCount += 1;
    }
    month += 1;
    if (month == 13){
      month = 1;
      year += 1;
    }

  }
  console.log(sunCount)
}

##start = datetime.datetime.now()
##result = len(count_loop(1000))
##end = datetime.datetime.now()
##elapsed = end - start
##print(elapsed)
##print(result)

    
