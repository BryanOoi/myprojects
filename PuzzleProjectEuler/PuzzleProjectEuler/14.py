import datetime

##Longest Collatz sequence
##
##Problem 14
##The following iterative sequence is defined for the set of positive integers:
##
##n → n/2 (n is even)
##n → 3n + 1 (n is odd)
##
##Using the rule above and starting with 13, we generate the following sequence:
##
##13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
##It can be seen that this sequence (starting at 13 and finishing at 1)
##contains 10 terms. Although it has not been proved yet (Collatz Problem),
##it is thought that all starting numbers finish at 1.
##
##Which starting number, under one million, produces the longest chain?
##
##NOTE: Once the chain starts the terms are allowed to go above one million.

dic_chain = {}

def fill_power_2(limit):
    num = 1
    count = 1
    while num < limit:
        dic_chain[num] = count
        num = num * 2
        count += 1

def col(limit):
    longest_chain = 0
    longest_starting_number = 0
    for i in range(2,limit + 1):
        count = 1
        n = i
##        print("Current num is ", n, count)
        while True:
            if n%2 == 0:
                n = int(n/2)
            else:
                n = int(3*n + 1)
            if n in dic_chain:
                count += dic_chain[n]
##                print("Current num is ", n, count)
                break
            if n == 1:
                count += 1
##                print("Current num is ", n, count)
                break
            else:
                count += 1
##            print("Current num is ", n, count)
        dic_chain[i] = count
        if count > longest_chain:
            longest_chain = count
            longest_starting_number = i
    return longest_chain, longest_starting_number
##        print(i, count)
##        print(dic_chain)


start = datetime.datetime.now()
result = col(1000000)
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
