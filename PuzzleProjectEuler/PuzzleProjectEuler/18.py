import datetime


##By starting at the top of the triangle below and moving to
##adjacent numbers on the row below, the maximum total from top to bottom is 23.
##
##   3
##  7 4
## 2 4 6
##8 5 9 3
##
##That is, 3 + 7 + 4 + 9 = 23.
##
##Find the maximum total from top to bottom of the triangle below:
##
##                            75
##                          95  64
##                        17  47  82
##                      18  35  87  10
##                    20  04  82  47  65
##                  19  01  23  75  03  34
##                88  02  77  73  07  63  67
##              99  65  04  28  06  16  70  92
##            41  41  26  56  83  40  80  70  33
##          41  48  72  33  47  32  37  16  94  29
##        53  71  44  65  25  43  91  52  97  51  14
##      70  11  33  28  77  73  17  78  39  68  17  57
##    91  71  52  38  17  14  91  43  58  50  27  29  48
##  63  66  04  68  89  53  67  30  73  16  69  87  40  31
##04  62  98  27  23  09  70  98  73  93  38  53  60  04  23
##
##NOTE: As there are only 16384 routes, it is possible to
##solve this problem by trying every route. However, Problem 67,
##is the same challenge with a triangle
##containing one-hundred rows; it cannot be solved by brute force,
##and requires a clever method! ;o)
        
  
full= """                   75
                          95  64
                        17  47  82
                      18  35  87  10
                    20  04  82  47  65
                  19  01  23  75  03  34
                88  02  77  73  07  63  67
              99  65  04  28  06  16  70  92
            41  41  26  56  83  40  80  70  33
          41  48  72  33  47  32  37  16  94  29
        53  71  44  65  25  43  91  52  97  51  14
      70  11  33  28  77  73  17  78  39  68  17  57
    91  71  52  38  17  14  91  43  58  50  27  29  48
  63  66  04  68  89  53  67  30  73  16  69  87  40  31
04  62  98  27  23  09  70  98  73  93  38  53  60  04  23"""

a = full.split('\n')
row_list = []

for lines in a:
    clean = lines.strip()
    row_list.append(clean.split('  '))

moves_list = []
top_list = []
bottom_list = []
top_count = [0,0,0,0,0]
bottom_count = [0,0,0,0,0]

def increment_binary_string(s):
    return '{:014b}'.format(1 + int(s, 2))

x = '00000000000000'
for _ in range(2**14):
    moves_list.append(x)
    x = increment_binary_string(x)

max_total = 0
for move in moves_list:
    total = int(row_list[0][0])
    move_num = row_list[0][0]
    ycount = 1
    xcount = 0
    for num in move:
        xcount += int(num)
        total += int(row_list[ycount][xcount])
        move_num += " " + row_list[ycount][xcount]
        ycount += 1
    if total > max_total:
        max_total = total
print(max_total)

##start = datetime.datetime.now()
##result = len(count_loop(1000))
##end = datetime.datetime.now()
##elapsed = end - start
##print(elapsed)
##print(result)

    
