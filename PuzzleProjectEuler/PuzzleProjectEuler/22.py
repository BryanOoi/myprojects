import datetime


##Using names.txt (right click and 'Save Link/Target As...'),
##a 46K text file containing over five-thousand first names,
##begin by sorting it into alphabetical order. Then working out
##the alphabetical value for each name, multiply this value by
##its alphabetical position in the list to obtain a name score.
##
##For example, when the list is sorted into alphabetical order,
##COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in
##the list. So, COLIN would obtain a score of 938 × 53 = 49714.
##
##What is the total of all the name scores in the file?

names = []
alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
filename = "names.txt"
with open(filename) as f:
    for lines in f:
        names = lines.split(',')
names.sort()

name_count = 1
score = 0
for name in names:
    name_total = 0
    for char in name:
        if char != '"':
            name_total += alphabets.index(char) + 1
    score += name_total * name_count
    name_count += 1
            
print(score)
##start = datetime.datetime.now()
##result = sum(res(10000))
##end = datetime.datetime.now()
##elapsed = end - start
##print(elapsed)
##print(result)

    
