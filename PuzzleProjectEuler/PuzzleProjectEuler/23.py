import datetime
import math

##A perfect number is a number for which the sum of its proper divisors
##is exactly equal to the number. For example, the sum of the proper
##divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that
##28 is a perfect number.
##
##A number n is called deficient if the sum of its proper divisors is
##less than n and it is called abundant if this sum exceeds n.
##
##As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the
##smallest number that can be written as the sum of two abundant numbers
##is 24. By mathematical analysis, it can be shown that all integers
##greater than 28123 can be written as the sum of two abundant numbers.
##However, this upper limit cannot be reduced any further by analysis
##even though it is known that the greatest number that cannot be expressed
##as the sum of two abundant numbers is less than this limit.
##
##Find the sum of all the positive integers which cannot be written
##as the sum of two abundant numbers.

max_abu = 28123;
def tri(n):
    return int((n*(n+1))/2)

def div2(num):
    till = num
    total_divs = 2
    count = 2
    all_d = [1]
    while count < till:
        if num % count == 0:
            total_divs += 2
            till = int(num/count)
            if count != int(num/count):
                all_d.append(count)
                all_d.append(int(num/count))
            else:
                all_d.append(count)
        count += 1
    return all_d


def div3(num):
    till = num
    total_divs = 2
    count = 2
    all_d = [1]
    while count < till:
        if num % count == 0:
            total_divs += 2
            till = int(num/count)
            if count != int(num/count):
                all_d.append(count)
                all_d.append(int(num/count))
            else:
                all_d.append(count)
        count += 1
    return sum(all_d)

def div(num: int):
    '''
    Calculates sum of all proper divisors
    :param num:
    :return:
    '''

    sum_divisors = 0
    for n in range(1, int(math.sqrt(num)) + 1):
        if num % n == 0:
            if num // n == n:
                sum_divisors += n
            else:
                sum_divisors += n + (num // n)

    # The sum not includes the number itself
    return sum_divisors - num

def res(total):
    abu = []
    for i in range(1,total):
        if div(i) > i:
            abu.append(i)

    return abu

def tot(all_abu):
    possible_sums = set()

    for i in range(len(all_abu)):
        for j in range(i,len(all_abu)):
            subto = int(all_abu[i]) + int(all_abu[j])
            if subto > max_abu:
                break
            possible_sums.add(subto)

    total = 0
    for k in range(max_abu):
        if k not in possible_sums:
            total += k
            
    return total
start = datetime.datetime.now()
result = tot(res(max_abu))
end = datetime.datetime.now()
elapsed = end - start
print(elapsed)
print(result)

    
