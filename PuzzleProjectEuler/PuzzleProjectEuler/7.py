
##10001st prime

##By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
##we can see that the 6th prime is 13.
##
##What is the 10 001st prime number?

def isprime(n):
    """Returns True if n is prime."""
    n = int(n)
    if n == 2:
        return True
    if n == 3:
        return True
    if n % 2 == 0:
        return False
    if n % 3 == 0:
        return False

    i = 5
    w = 2

    while i * i <= n:
        if n % i == 0:
            return False

        i += w
        w = 6 - w

    return True

def getprime(nth):
    count = 1
    prime = 0
    i = 3
    while nth > count:
        if isprime(i):
            count += 1
            prime = i
        i += 2
    return prime

print(getprime(10001))

    
