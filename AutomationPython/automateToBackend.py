
all_ids = []
#filename = "libraryoppo.txt"
filename = "newones.txt"

#write_file = "fulllist2.txt"
write_file = "newlist.txt"
params = []
variable_names = []
setter_names = []
getter_names = []

with open(filename) as f:
    for line in f:
        string_after_a = line.split('String ',1)
        if len(string_after_a) > 1:
            param_name = string_after_a[1].split(' =',1)[0]
            params.append(param_name)

string_change_to_write = ""
for param in params:
    temp1 = param.split('PARAM_OPPO_',1)[1]
    variable_name = temp1.lower()
    variable_names.append(variable_name)
    string_change_to_write += "String " + variable_name + " = getHtmlStringParam(req, " + param + ");\n"

string_change_to_write += "\n\n"
for i in range(len(variable_names)):
    temp1 = variable_names[i];
    b = temp1.split('_')
    c = [item.replace(item[0],item[0].upper(),1) for item in b]
    setter_name = 'set' + ''.join(c)
    getter_name = 'get' + ''.join(c)
    setter_names.append(setter_name)
    getter_names.append(getter_name)
    string_change_to_write +=  "oppo." +  setter_name + "(" + temp1 + ");\n"

string_change_to_write += "\n\n"
for i in range(len(variable_names)):
    temp1 = variable_names[i];
    string_change_to_write += '"' + temp1 + '", '
    if (i + 1) % 4 == 0:
        string_change_to_write += '\n'

string_change_to_write += "\n\n+ \""
for i in range(len(variable_names)):
    temp1 = variable_names[i];
    string_change_to_write += temp1 + ', '
    if (i + 1) % 4 == 0:
        string_change_to_write += '"\n+ "'
        
string_change_to_write += "\n\n"
for i in range(len(variable_names)):
    temp1 = variable_names[i];
    string_change_to_write += temp1 + ' = ?, '
    if (i + 1) % 4 == 0:
        string_change_to_write += '\n'

string_change_to_write += "\n\n"
for i in range(len(getter_names)):
    temp1 = getter_names[i];
    string_change_to_write += "oppo." + temp1 + '(), '
    if (i + 1) % 4 == 0:
        string_change_to_write += '\n'
        
string_change_to_write += "\n\n"
for i in range(len(setter_names)):
    temp1 = setter_names[i];
    string_change_to_write += "oppo." + temp1 + '(rset.getString(x));\n'

string_change_to_write += "\n\n"
for i in range(len(variable_names)):
    temp1 = variable_names[i];
    if i == len(variable_names) - 1:
        string_change_to_write += 'ADD COLUMN ' + temp1 + ' VARCHAR(100) NULL;\n'
    else:
        string_change_to_write += 'ADD COLUMN ' + temp1 + ' VARCHAR(100) NULL,\n'

string_change_to_write += "\n\n"
for i in range(len(getter_names)):
    temp1 = getter_names[i];
    temp1 = temp1.split('get',1)[1]
    temp1 = temp1.replace(temp1[0],temp1[0].lower(),1)
    string_change_to_write += 'String ' + temp1 + "; "
    if (i + 1) % 3 == 0:
        string_change_to_write += "\n"


text_file = open(write_file, "w")
text_file.write(string_change_to_write)
text_file.close()

    

