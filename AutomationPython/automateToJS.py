all_ids = []
filename = "newones.txt"

open_file = "jsinput.txt"
write_file = "jsoutput.txt"
params = []
variable_names = []
with open(filename) as f:
    for line in f:
        string_after_a = line.split('String ',1)
        if len(string_after_a) > 1:
            param_name = string_after_a[1].split(' =',1)[0]
            params.append(param_name)


with open(open_file, "rt") as fin:
    for line in fin:
        string_after = line.split('id="',1)
        if len(string_after) > 1:
            id_name = string_after[1].split('"',1)[0]
            if 'input' in id_name or 'select' in id_name:
                all_ids.append(id_name)


##with open("out.txt", "wt") as fout:
            
string_change_to_write = ""
for ids in all_ids:
    temp1 = ids.split('oppo-',1)[1]
    temp2 = temp1.split('-input',1)
    if len(temp2) > 1:
        temp3 = temp2[0]
    else:
        temp3 = temp1.split('-select',1)[0]
    variable_name = temp3.replace('-','_')
    variable_names.append(variable_name)
    string_change_to_write += "var " + variable_name + " = checkField($(\"#" + ids +"\").get());\n"

string_change_to_write += "\n"
string_change_to_write += "var valid = "

for i in range(len(variable_names)):
    if i != len(variable_names) - 1:
        string_change_to_write += variable_names[i] + ".isValid() && "
    else:
        string_change_to_write += variable_names[i] + ".isValid();"

string_change_to_write += "\n\n"
string_change_to_write += "var change = "

for i in range(len(variable_names)):
    if i != len(variable_names) - 1:
        string_change_to_write += variable_names[i] + ".hasChange() || "
    else:
        string_change_to_write += variable_names[i] + ".hasChange();"

string_change_to_write += "\n\n"
string_change_to_write += "var data = {"

for i in range(len(variable_names)):
    param_str = "PLACEHOLDER"
    for param in params:
        if variable_names[i].upper() in param:
            param_str = param
    if i != len(variable_names) - 1:
        string_change_to_write += "<%=OpportunityController." + param_str + "%>: " + variable_names[i] + ".getValue(), \n"
    else:
        string_change_to_write += "<%=OpportunityController." + param_str + "%>: " + variable_names[i] + ".getValue() \n }"


string_change_to_write += "\n\n\n"

        
for ids in all_ids:
    temp1 = ids.split('oppo-',1)[1]
    temp2 = temp1.split('-input',1)
    if len(temp2) > 1:
        temp3 = temp2[0]
    else:
        temp3 = temp1.split('-select',1)[0]
    variable_name = temp3.replace('-','_')
    string_change_to_write += "var " + variable_name + ' = $("#' + ids + '").data("origval");\n'

string_change_to_write += "\n"
string_change_to_write += "var data = {"

for i in range(len(variable_names)):
    param_str = "PLACEHOLDER"
    for param in params:
        if variable_names[i].upper() in param:
            param_str = param
    if i != len(variable_names) - 1:
        string_change_to_write += "<%=OpportunityController." + param_str + "%>: " + variable_names[i] + ", \n"
    else:
        string_change_to_write += "<%=OpportunityController." + param_str + "%>: " + variable_names[i] + "\n }"


text_file = open(write_file, "w")
text_file.write(string_change_to_write)
text_file.close()

    

