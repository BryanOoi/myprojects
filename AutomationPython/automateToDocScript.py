doc_script = "Docscript.txt"
all_fields = "ALLFields.txt"
ifs = "ifs.txt"

write_file = "finaldocscript.txt"
string_change_to_write = ""
data_names = ""
y_length = 842
b_change = [4,3]
c_change = [4,1]
l_change = [0,0]

def getter_name(line):
    temp1 = line.split('PARAM_OPPO_',1)[1]
    variable_name = temp1.lower().split(' =',1)[0]
    b = variable_name.split('_')
    c = [item.replace(item[0],item[0].upper(),1) for item in b]
    getter_name = 'get' + ''.join(c)
    full_getter_name = "oppo." +  getter_name + "()"
    return variable_name, full_getter_name

def getLine2(field_lines, string_change_to_write):
    line2 = field_lines.readline().strip()
    if len(line2) < 3:
        string_change_to_write += "\n"
        string_change_to_write += "    stream.close();\n"
        string_change_to_write += "    page_count += 1;\n"
        string_change_to_write += "    page = doc.getPage(page_count);\n"
        string_change_to_write += "    stream = new PDPageContentStream(doc, page, PDPageContentStream.AppendMode.APPEND, true, true);\n"
        string_change_to_write += "    stream.setFont(font, 10);\n"
        line2 = field_lines.readline()
    a1 = line2.split(" ")
    return a1,string_change_to_write

with open(doc_script) as doc_lines, open(all_fields) as field_lines, open(ifs) as choice_box:
    for line in doc_lines:
        line = line.strip()
        
        if 'final' in line:
            full_getter_name = getter_name(line)
            a1,string_change_to_write = getLine2(field_lines, string_change_to_write)
            string_change_to_write += "if (!StringUtil.isNullOrEmpty(" + full_getter_name[1] + ")) {\n"
            string_change_to_write += "    offset_x = " + str(int(a1[0]) + b_change[0]) + ";\n"
            string_change_to_write += "    offset_y = " + str(y_length - int(a1[1]) + b_change[1]) + ";\n"   
            string_change_to_write += "    String "+ full_getter_name[0] + " = " + full_getter_name[1] + ".toUpperCase();\n"
            if a1[2] == 'L':
                string_change_to_write += "    pdfWriteLine(stream, " + full_getter_name[0] + ", offset_x, offset_y);\n"
            else:
                string_change_to_write += "    pdfWriteBox(stream, " + full_getter_name[0] + ", offset_x, offset_y);\n"
            string_change_to_write += "}\n"
            
        if line.isdigit():
            number = int(line)
            line3 = choice_box.readline().strip()
            typeline = line3

            if typeline == 'N' or typeline == 'Y' or typeline == 'NY' or typeline == 'NO':
                line3 = choice_box.readline()
                full_getter_name = getter_name(line3)

                string_change_to_write += "if (!StringUtil.isNullOrEmpty(" + full_getter_name[1] + ")) {\n"
                string_change_to_write += "    String " + full_getter_name[0] + " = " + full_getter_name[1] + ";\n"
                if typeline == 'Y':
                    line3 = choice_box.readline()
                    full_getter_name_next = getter_name(line3)
                    string_change_to_write += "    String " + full_getter_name_next[0] + " = " + full_getter_name_next[1] + ";\n"
                    
                line3 = choice_box.readline().strip()
                
                for i in range(number):
                    a12 = line3.split(",")
                    data_name = a12[2].strip().strip("\'")
                    if typeline == 'Y' or typeline == 'NY':
                        compare_name = a12[3].strip().strip("\'")
                    else:
                        compare_name = a12[4].strip().strip("\'")
                    compare_name = compare_name.replace('"','\\"')
                    data_names += 'public static final String ' + data_name + ' = "' + compare_name + '";\n'
                    a1,string_change_to_write = getLine2(field_lines, string_change_to_write)
                    
                    if i == 0 or typeline == 'Y' or typeline == 'NY':
                        string_change_to_write += "    if (" + full_getter_name[0] + ".contains(DataMaster." + data_name + ")) {\n"
                        string_change_to_write += "        offset_x = " + str(int(a1[0]) + c_change[0]) + ";\n"
                        string_change_to_write += "        offset_y = " + str(y_length - int(a1[1]) + c_change[1]) + ";\n"
                        string_change_to_write += "        pdfWriteLine(stream, \"X\", offset_x, offset_y);\n"
                        string_change_to_write += "    }\n"
                    else :
                        string_change_to_write += "    else if (" + full_getter_name[0] + ".contains(DataMaster." + data_name + ")) {\n"
                        string_change_to_write += "        offset_x = " + str(int(a1[0]) + c_change[0]) + ";\n"
                        string_change_to_write += "        offset_y = " + str(y_length - int(a1[1]) + c_change[1]) + ";\n"   
                        string_change_to_write += "        pdfWriteLine(stream, \"X\", offset_x, offset_y);\n"
                        string_change_to_write += "    }\n"
                    if i != number - 1:
                        line3 = choice_box.readline().strip()

                if typeline != 'NY' and typeline != 'NO':
                    a1,string_change_to_write = getLine2(field_lines, string_change_to_write)
                    string_change_to_write += "    else {\n"
                    string_change_to_write += "        offset_x = " + str(int(a1[0]) + l_change[0]) + ";\n"
                    string_change_to_write += "        offset_y = " + str(y_length - int(a1[1]) + l_change[1]) + ";\n"
                    if typeline == 'N':
                        string_change_to_write += "        pdfWriteLine(stream, " + full_getter_name[0] + ", offset_x, offset_y);\n"
                    elif typeline == 'Y':
                        string_change_to_write += "        pdfWriteLine(stream, " + full_getter_name_next[0] + ", offset_x, offset_y);\n"
                    string_change_to_write += "    }\n"
                    
                string_change_to_write += "}\n"



write_file = open(write_file, "w")
##write_file.write(string_change_to_write + "\n\n\n\n\n" + data_names)
write_file.write(string_change_to_write)
write_file.close()






    

