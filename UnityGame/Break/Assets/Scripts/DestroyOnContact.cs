﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour
{
    float minSpeedRed = 7.0f;
    float minSpeedYellow = 10.0f;
    float minSpeedGreen = 15.0f;
    void OnCollisionEnter2D(Collision2D other)
    {
        float magnitude = other.relativeVelocity.magnitude;
        Rigidbody2D rigidbody = other.gameObject.GetComponent<Rigidbody2D>();

        if (gameObject.tag == "Red" && magnitude >= minSpeedRed) {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x * 0.9f, rigidbody.velocity.y * 0.9f);
            Destroy(gameObject);
        }
        if (gameObject.tag == "Yellow" && magnitude >= minSpeedYellow) {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x * 0.6f, rigidbody.velocity.y * 0.6f);
            Destroy(gameObject);
        }
        if (gameObject.tag == "Green" && magnitude >= minSpeedGreen) {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x * 0.3f, rigidbody.velocity.y * 0.3f);
            Destroy(gameObject);
        }

    }
}
