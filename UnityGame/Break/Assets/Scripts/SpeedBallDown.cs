﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBallDown : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        Rigidbody2D rigidbody = other.gameObject.GetComponent<Rigidbody2D>();
        rigidbody.velocity = new Vector2(rigidbody.velocity.x * 0.7f, rigidbody.velocity.y * 0.7f);

    }
}
