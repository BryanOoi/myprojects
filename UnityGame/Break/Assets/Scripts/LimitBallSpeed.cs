﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitBallSpeed : MonoBehaviour
{
    public float maxVelocity;
    float sqrMaxVelocity;
    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        SetMaxVelocity(maxVelocity);
    }


    void SetMaxVelocity(float maxVelocity)
    {
        this.maxVelocity = maxVelocity;
        sqrMaxVelocity = maxVelocity * maxVelocity;
    }


    void FixedUpdate()
    {
        var v = rb.velocity;
        if (v.sqrMagnitude > sqrMaxVelocity)
        { 
            rb.velocity = v.normalized * maxVelocity;
        }
    }
}
