﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;

}

public class MoveOctagon : MonoBehaviour
{
    public Rigidbody2D rb;
    public float movementSpeed;
    public float angularSpeed;

    public Boundary boundary;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {


    }

    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        rb.transform.rotation = rb.transform.rotation * Quaternion.Euler(0.0f, 0.0f, moveHorizontal * -angularSpeed);
        Vector2 movement = new Vector2(0, moveVertical);
        rb.velocity = movement * movementSpeed;

        rb.position = new Vector2
        (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)
        );

        //if (Input.GetKey(KeyCode.LeftControl))
        //{
        //    float moveHorizontal = Input.GetAxis("Horizontal");
        //    rb.transform.rotation = rb.transform.rotation * Quaternion.Euler(0.0f, 0.0f, moveHorizontal * -angularSpeed);

        //    rb.velocity = new Vector2(0, 0);

        //    rb.position = new Vector2
        //    (
        //        Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
        //        Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)
        //    );
        //}

        //else {

        //    float moveHorizontal = Input.GetAxis("Horizontal");
        //    float moveVertical = Input.GetAxis("Vertical");

        //    Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        //    rb.velocity = movement * movementSpeed;

        //    rb.position = new Vector2
        //    (
        //        Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
        //        Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)
        //    );
        //}


    }
}
