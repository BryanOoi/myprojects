﻿var itemValueList = {};
var tempShowValueList = {};
var itemsFound = {};
var haveList = [];
var timeFirstLoad = 0;
var timeForNextUpdate = 3600;
var largestItemNo = 2;


var element = document.getElementById("rs_table");
var para = document.createElement("tbody");
para.id = "tempresult";
para.style.cssText = 'display: none;';
element.appendChild(para);

function saveTextAsFile(time) {
    var textToSave = JSON.stringify(tempShowValueList);
    var textToSaveAsBlob = new Blob([textToSave], { type: "text/plain" });
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = time;

    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);

    downloadLink.click();
}

function destroyClickedElement(event) {
    document.body.removeChild(event.target);
}

function returnGoodValues(haveText, value, link, a, bestprice) {
    if (a != 99 && a <= bestprice) {
        console.log(tempShowValueList[haveText]);
        console.log("Have : " + haveText + "\n Want : " + a + "\n Link : " + link + "\n Best price is " + bestprice + " grab it quick.");
    }
    else if (a != 99 && a > bestprice)
        console.log("Not Worth It When This Guy is Selling " + haveText + " for " + a + " when it costs " + bestprice);
}

$(document).ajaxSuccess(
  function (event, request, settings) {
      let searchParams2 = new URLSearchParams(settings.url);
      var wrongAjax = searchParams2.get("w0");
      if (!(wrongAjax == null || wrongAjax == ""))
          return;
      var contents = $("#trade_results").find("tr.new");
      var arrayLength = contents.length;
      for (var k = 0; k < arrayLength; k++) {
          var haveText = contents[k].getElementsByTagName("td")[1].innerHTML;
          var value = contents[k].getElementsByTagName("td")[2].innerHTML;
          var link = contents[k].getElementsByTagName("td")[0].innerHTML;
          var a = 99;
          if (value == "key")
              value = "1xkey";
          if (!value.includes(",") && value.includes("xkey")) {
              var int = value.replace("xkey", "");
              a = parseInt(int);
          }
          if (!haveText.includes(",") && (a != 99) && !haveText.includes("crate")) {
              if (haveText in itemValueList) {
                  returnGoodValues(haveText, value, link, a, itemValueList[haveText])
              }
              else if (a != 99) {
                  try {
                      let searchParams = new URLSearchParams(settings.url);
                      var time = searchParams.get("_");
                      if (time - timeForNextUpdate > timeFirstLoad) {
                          if (timeFirstLoad != 0) {
                              itemValueList = {};
                              tempShowValueList = {};
                              saveTextAsFile(time);
                          }
                          timeFirstLoad = time;
                      }
                      addtoItemValueList(haveText, time, a, link, "Buy");
                      //returnGoodValues(haveText, value, link, a, itemValueList[haveText])
                  }
                  catch (err) {
                  }

              }
          }
          else if (a == 99) {
              if (!value.includes(",")) {
                  var b = 99;
                  if (haveText == "key")
                      haveText = "1xkey";
                  if (!haveText.includes(",") && haveText.includes("xkey")) {
                      var int2 = haveText.replace("xkey", "");
                      b = parseInt(int2);
                  }
                  if (value in itemValueList && b != 99) {
                      if (b >= itemValueList[value]) {
                          console.log(tempShowValueList[value]);
                          console.log("SELL ------- This guy is buying " + value + " for " + b + " when it costs " + itemValueList[value] + ". Sell it quick. \nSELL ----  Link : " + link);
                      }
                      else
                          console.log("SELL ------- Not Worth It When This Guy is Buying " + value + " for " + b + " when it costs " + itemValueList[value]);
                  }
                  else if (b != 99 && !value.includes("crate")) {
                      try {
                          let searchParams = new URLSearchParams(settings.url);
                          var time = searchParams.get("_");
                          addtoItemValueList(value, time, b, link, "Sell");
                      }
                      catch (err) { }
                  }
              }
          }


      }
  }
);

// sort by name

var itemValueList = {};
var itemValueListSell = {};
var tempShowValueList = {};
var tempShowValueListSell = {};
var itemsFound = {};
var processAgain = {};
var processAgainIter = {};
var priceByItems = [];
var priceByItemsSell = [];
var haveList = [];
var timeFirstLoad = 0;
var timeForNextUpdate = 3600;
var largestItemNo = 2;
var maxUnknownDifference = 2;
var tempItemNo = 0;

var element = document.getElementById("rs_table");
var para = document.createElement("tbody");
para.id = "tempresult";
para.style.cssText = 'display: none;';
element.appendChild(para);

function sum(obj) {
    var sum = 0;
    for (var el in obj) {
        if (obj.hasOwnProperty(el)) {
            sum += parseFloat(obj[el]);
        }
    }
    return sum;
}

function evaluatePrice(itemToFind, time, a, link, BorS, iteration, data) {
    var itempricetofind = 2;
    var searchitemvalue = 1;
    if (BorS == "h") {
        itempricetofind = 1;
        searchitemvalue = 2;
    }
    //tbody[@id='toClick']/a
    document.getElementById("tempresult").innerHTML = data;
    var tempcontents = $("#tempresult").find("tr");
    var temparrayLength = tempcontents.length;
    var tempValueList = {};
    var largestval = 0;
    if (temparrayLength > 10) {
        for (var i = 0; i < temparrayLength; i++) {
            var puretext = tempcontents[i].getElementsByTagName("td")[itempricetofind].innerHTML;
            puretext = puretext.replace("<span style=\"font-weight:bold\">", "");
            puretext = puretext.replace("</span>", "");


            //debugger;
            if (puretext == itemToFind) {
                //if (puretext == "nitro crate cc")
                //    debugger;
                var tempvalue = tempcontents[i].getElementsByTagName("td")[searchitemvalue].innerHTML;
                tempvalue = tempvalue.replace("<span style=\"font-weight:bold\">", "");
                tempvalue = tempvalue.replace("</span>", "");
                var tempa = 1000;
                if (tempvalue == "key")
                    tempvalue = "1xkey";
                if (!tempvalue.includes(",") && tempvalue.includes("xkey")) {
                    var int = tempvalue.replace("xkey", "");
                    tempa = parseInt(int);
                }
                if (tempa != 1000) { // reject anytime tempa cant be parsed
                    if (!(tempa in tempValueList))
                        tempValueList[tempa] = 1;
                    else
                        tempValueList[tempa] = tempValueList[tempa] + 1;
                    if (largestval < tempa)
                        largestval = tempa;
                }
                else if (tempa == 1000) {
                    var multiText = tempvalue.split(",");
                    if (!tempvalue.includes("offer")) {
                        var totalKeys = 0;
                        var countUnknown = 0;
                        for (var z = 0; z < multiText.length; z++) {
                            var trimmed = multiText[z].trim();
                            if (!(trimmed in itemValueList)) {
                                if (!trimmed.includes("key"))
                                    countUnknown += 1;
                            }
                        }
                        if (countUnknown < maxUnknownDifference) {

                            var allKeysUpdated = true;
                            for (var z = 0; z < multiText.length; z++) {
                                trimmed = multiText[z].trim();
                                if (trimmed == "key")
                                    trimmed = "1xkey";
                                if (!(trimmed in itemValueList)) {
                                    if (trimmed.includes("xkey")) {
                                        var keyInt = trimmed.replace("xkey", "");
                                        totalKeys += parseInt(keyInt);

                                    }
                                    else if (trimmed == itemToFind) {
                                        allKeysUpdated = false;
                                        break;
                                    }
                                    else {
                                        var lengthBefore = Object.keys(itemValueList).length;
                                        try { addtoItemValueList(trimmed, time, "", "", BorS, iteration + 1); }
                                        catch (err) { }
                                        var lengthAfter = Object.keys(itemValueList).length;
                                        if (lengthBefore == lengthAfter) {
                                            allKeysUpdated = false;
                                            break;
                                        }
                                        else {
                                            if (trimmed in itemValueList)// will break all the time if BorS is == 'h'
                                                totalKeys += itemValueList[trimmed];
                                            else
                                                break;
                                        }
                                    }
                                }
                                else if (trimmed in itemValueList) {
                                    totalKeys += itemValueList[trimmed];
                                }
                            }
                            if (allKeysUpdated == true) {
                                if (!((totalKeys + "*") in tempValueList))
                                    tempValueList[totalKeys + "*"] = 1;
                                else
                                    tempValueList[totalKeys + "*"] = tempValueList[totalKeys + "*"] + 1;
                                if (BorS == "w") {
                                    priceByItems.push({ "Item": itemToFind, "totalKeys": totalKeys, "Value": tempvalue });
                                }
                                else {
                                    priceByItemsSell.push({ "Item": itemToFind, "totalKeys": totalKeys, "Value": tempvalue });
                                }
                            }
                        }
                    }
                }
            }
        }
        delete tempValueList["NaN"];

        if (largestval != 0) {
            if (BorS == "w") {
                tempShowValueList[itemToFind] = tempValueList;
                itemValueList[itemToFind] = largestval;
            }
            else {
                tempShowValueListSell[itemToFind] = tempValueList;
                itemValueListSell[itemToFind] = largestval;
            }
        }
    }

}

var tempInList = {};
function addtoItemValueList(itemToFind, time, a, link, BorS, iteration) {
    var tempSliced = itemToFind.substring(2, itemToFind.length);
    if (haveList.includes(tempSliced)) {
        itemToFind = tempSliced;
    }

    if (!(itemToFind in tempInList))
        tempInList[itemToFind] = 1;
    else
        tempInList[itemToFind] = tempInList[itemToFind] + 1;

    
    var test1 = itemToFind in haveList ;
    var test2 = (!(itemToFind in itemValueList));
    var test3 = (itemToFind in haveList && !(itemToFind in itemValueList));
    var test4 = test1 && test2;

    if (haveList.includes(itemToFind) && !(itemToFind in itemValueList)) 
        return;
        
    if (iteration > 3)
        return;


    haveList.push(itemToFind);

    if (itemToFind in processAgain) {
        evaluatePrice(itemToFind, time, a, link, BorS, iteration, processAgain[itemToFind]);
    }
    else {

        var requestURL = "https://www.rl-trades.com/?ajax=trades&" + BorS + "0=%22" + encodeURIComponent(itemToFind) + "%22&show=1&_=" + time;
        $.ajaxSetup({ async: false });
        $.get(requestURL, function (data, status) {
            evaluatePrice(itemToFind, time, a, link, BorS, iteration, data);
        });
    }
    if (iteration == 0) {
        //processAgain = {};
        //processAgainIter = {};
        //haveList = [];
    }
}

function findItemsListPrice(item, totalKeys) {
    var tempArr = [];
    for (var i = 0; i < priceByItems.length; i++) {
        if (totalKeys == 0) { 
            if (priceByItems[i]["Item"] == item) {
                tempArr.push(priceByItems[i]);
            }
        }
        else {
            if (priceByItems[i]["Item"] == item && priceByItems[i]["totalKeys"] == totalKeys) {
                tempArr.push(priceByItems[i]);
            }
        }
    }
    console.log(tempArr);
}

addtoItemValueList("mantis", 1495513805799, 0, "", "h", 0);


var element = document.getElementById("rs_table");
var para = document.createElement("tbody");
para.id = "toClick";
para.style.cssText = 'display: none;';
element.appendChild(para);

document.getElementById("toClick").innerHTML = ""

test1 = document.getElementsByClassName("new")[0].getElementsByTagName("td")[0].innerHTML

document.getElementById("toClick").innerHTML = test1
