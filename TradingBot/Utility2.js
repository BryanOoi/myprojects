﻿var itemMustBuy = {};
var tempShowValueList = {};
var haveList = {};
var timeFirstLoad = 0;
var timeForNextUpdate = 3600;
var largestItemNo = 2;

itemMustBuy["parallax [black market]"] = 6;
itemMustBuy["hexed [black market]"] = 8;
itemMustBuy["biomass [black market]"] = 4;
itemMustBuy["heatwave [black market]"] = 16;
itemMustBuy["slipstream [black market]"] = 8;
itemMustBuy["bubbly [black market]"] = 8;
itemMustBuy["hex tide [black market]"] = 8;
itemMustBuy["saffron octane"] = 8;
itemMustBuy["titanium white octane"] = 8;
itemMustBuy["crimson octane"] = 8;
itemMustBuy["20xx"] = 29;
itemMustBuy["black tunica"] = 2;
itemMustBuy["black sunburst"] = 2;
itemMustBuy["black dieci"] = 3;
itemMustBuy["crimson tachyon"] = 14;
itemMustBuy["titanium white tachyon"] = 9;
itemMustBuy["titanium white neo-thermal"] = 8;
itemMustBuy["crimson neo-thermal"] = 6;
itemMustBuy["crimson hexphase"] = 5;
itemMustBuy["titanium white hexphase"] = 6;
itemMustBuy["crimson spiralis"] = 3;
itemMustBuy["titanium white spiralis"] = 3;
itemMustBuy["titanium white chakram"] = 4;
itemMustBuy["black chakram"] = 6;
itemMustBuy["sky blue spiralis"] = 2;
itemMustBuy["sky blue fsl"] = 3;
itemMustBuy["titanium white fsl"] = 3;
itemMustBuy["sky blue zomba"] = 11;
itemMustBuy["grey zomba"] = 9;
itemMustBuy["crimson zomba"] = 10;
itemMustBuy["forest green zomba"] = 10;
itemMustBuy["crimson voltaic"] = 25;
itemMustBuy["forest green voltaic"] = 13;
itemMustBuy["purple voltaic"] = 9;
itemMustBuy["titanium white zomba"] = 9;

var itemValueList = JSON.parse(JSON.stringify(itemMustBuy));


var element = document.getElementsByClassName("contents")[0];
var para = document.createElement("tbody");
para.id = "tempresult";
para.style.cssText = 'display: none;';
element.appendChild(para);

var para2 = document.createElement("tbody");
para2.id = "toClick";
para2.style.cssText = 'display: none;';
element.appendChild(para2);

var para3 = document.createElement("tbody");
para3.id = "toClickRlt";
para3.style.cssText = 'display: none;';
element.appendChild(para3);

var para4 = document.createElement("tbody");
para4.id = "toClickSteam";
para4.style.cssText = 'display: none;';
element.appendChild(para4);

function saveTextAsFile(time)
{
    var textToSave = JSON.stringify(tempShowValueList);
    var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = time;
 
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
 
    downloadLink.click();
}
 
function destroyClickedElement(event)
{
    document.body.removeChild(event.target);
}

function returnGoodValues(haveText,value,link,a, bestprice) {
    if (a != 99 && a <= bestprice && a != 1){
		console.log(tempShowValueList[haveText]);
		console.log("Have : " + haveText + "\n Want : " + a + "\n Link : " + link + "\n Best price is " + bestprice + " grab it quick.");

		if (haveText in itemMustBuy) {
			console.log("THIS IS A MUST BUY ITEM");
			document.getElementById("toClick").innerHTML = link;
			setTimeout(function () { document.getElementById("toClick").innerHTML = ""; }, 3000);						
		}		

	}	
	else if (a != 99 && a > bestprice)
		console.log("");
}
function addtoItemValueList(itemToFind, time, a, link, BorS) {
    
    var requestURL = "https://www.rl-trades.com/?ajax=trades&h0=key&w0=%22" + encodeURIComponent(itemToFind) + "%22&show=1&_=" + time;
    $.get(requestURL, function (data, status) {
        
        document.getElementById("tempresult").innerHTML = data;
        var tempcontents = $("#tempresult").find("tr");
        var temparrayLength = tempcontents.length;
        var tempValueList = {};
        var largestval = 0;
        if (temparrayLength > 10) {
            for (var i = 0; i < temparrayLength; i++) {
                var puretext = tempcontents[i].getElementsByTagName("td")[2].innerHTML;
                puretext = puretext.replace("<span style=\"font-weight:bold\">", "");
                puretext = puretext.replace("</span>", "");

                //debugger;
                if (puretext == itemToFind) {
                    var tempvalue = tempcontents[i].getElementsByTagName("td")[1].innerHTML;
                    tempvalue = tempvalue.replace("<span style=\"font-weight:bold\">", "");
                    tempvalue = tempvalue.replace("</span>", "");
                    var tempa = 1000;
                    if (tempvalue == "key")
                        tempvalue = "1xkey";
                    if (!tempvalue.includes(",") && tempvalue.includes("xkey")) {
                        var int = tempvalue.replace("xkey", "");
                        tempa = parseInt(int);
                    }
                    if (tempa != 1000) {
                        if (!(tempa in tempValueList))
                            tempValueList[tempa] = 1;
                        else
                            tempValueList[tempa] = tempValueList[tempa] + 1;
                        if (largestval < tempa)
                            largestval = tempa;
                    }
                }
            }
            if (largestval != 0) {

				tempShowValueList[itemToFind] = tempValueList;
				if (!(itemToFind in itemMustBuy))
					itemValueList[itemToFind] = largestval;
                
                if (BorS == "Buy") {
                    console.log(tempValueList);
                    console.log("The largest value people will buy the item " + itemToFind + " is " + largestval + " with " + tempValueList[largestval] + " people buying.");  
                    if (itemValueList[itemToFind] < a)
                        console.log("");
                    else if (a != 1) { 
                        console.log("Have : " + itemToFind + "\n Want : " + a + "\n Link : " + link + "\n Best price is " + itemValueList[itemToFind] + " grab it quick.");

                        if (itemToFind in itemMustBuy) {
                            console.log("THIS IS A MUST BUY ITEM");

							document.getElementById("toClick").innerHTML = link;
							setTimeout(function () { document.getElementById("toClick").innerHTML = ""; }, 3000);						
                        }
                    }
                }
                else if (BorS == "Sell") {
                    if (itemValueList[itemToFind] <= a) {
                        console.log(tempValueList);
                        console.log("SELL ---- This guy is buying " + itemToFind + " for " + a + " when it costs " + itemValueList[itemToFind] + ". Sell it quick. \nSELL ----  Link : " + link);
                        // if (link.includes("league.com")) {
                            // document.getElementById("toClick").innerHTML = link;
                            // setTimeout(function () { document.getElementById("toClick").innerHTML = ""; }, 3000);
                        // }
                    }
                    else
                        console.log("");
                }
            }
        }
    });
}

$(document).ajaxSuccess(
  function (event, request, settings) {
      let searchParams2 = new URLSearchParams(settings.url);
      var wrongAjax = searchParams2.get("w0");
      if (!(wrongAjax == null || wrongAjax == ""))
          return;
      var contents = $(".tb").find("tr.new");
      var arrayLength = contents.length;
      for (var k = 0; k < arrayLength; k++) {
          var haveText = contents[k].getElementsByTagName("td")[1].innerHTML;
          var value = contents[k].getElementsByTagName("td")[3].innerHTML;
          var link = contents[k].getElementsByTagName("td")[0].innerHTML;
		  //console.log('havetext = ' + haveText);
          var a = 99;
          if (value == "key")
              value = "1xkey";
          if (!value.includes(",") && value.includes("k") && value.length == 2)
          {
              var int = value.replace("k", "");
              a = parseInt(int);
          }
          if (!haveText.includes(",") && (a != 99) && !haveText.includes("crate"))
          {
              // if (haveText in itemValueList && haveText in tempShowValueList) {
                  // returnGoodValues(haveText, value,link, a, itemValueList[haveText])
			  // }
              if (haveText in itemValueList) {
                  returnGoodValues(haveText, value,link, a, itemValueList[haveText])
			  }			  
              else if (a != 99)
              {
                  try {
                      let searchParams = new URLSearchParams(settings.url);
                      var time = searchParams.get("_");
					  if (time - timeForNextUpdate > timeFirstLoad) {
						  if (timeFirstLoad != 0) {
							  itemValueList = {};
							  tempShowValueList = {}; 
							  saveTextAsFile(time);
						  }
						  timeFirstLoad = time;
					  }
					  if (haveText in itemMustBuy)
							addtoItemValueList(haveText, time, a, link, "Buy");
                      //returnGoodValues(haveText, value, link, a, itemValueList[haveText])
                  }
                  catch (err) {
                  }
                  
              }
          }
          else if (a == 99) {
              if (!value.includes(",")) {
                  var b = 99;
                  if (haveText == "key")
                      haveText = "1xkey";
                  if (!haveText.includes(",") && haveText.includes("xkey")) {
                      var int2 = haveText.replace("xkey", "");
                      b = parseInt(int2);
                  }
                  if (value in itemValueList && b != 99) {
                      if (b >= itemValueList[value] + 1) {
                          console.log(tempShowValueList[value]);
                          console.log("SELL ------- This guy is buying " + value + " for " + b + " when it costs " + itemValueList[value] + ". Sell it quick. \nSELL ----  Link : " + link);
                          // if (link.includes("league.com")) {
                              // document.getElementById("toClick").innerHTML = link;
                              // setTimeout(function () { document.getElementById("toClick").innerHTML = ""; }, 3000);
                          // }
                      }
                      else
                          console.log("");
                  }
                  else if (b != 99 && !value.includes("crate")) {
                      try {
                          let searchParams = new URLSearchParams(settings.url);
                          var time = searchParams.get("_");
						  if (haveText in itemMustBuy)
								addtoItemValueList(value, time, b, link, "Sell");
                      }
                      catch (err) { }
                  }
              }
          }

          
      }
  }
);