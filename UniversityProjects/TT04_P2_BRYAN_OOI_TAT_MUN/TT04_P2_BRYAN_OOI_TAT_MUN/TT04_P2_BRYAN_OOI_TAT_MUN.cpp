/**********|**********|**********|
Program: TT04_P2_BRYAN_OOI_TAT_MUN.cpp 
Course: Bachelor of Computer Science
Year: 2015/16 Trimester 1
Name: Bryan Ooi Tat Mun
ID: 1141328133
Email: bryanotm@hotmail.com
Phone: 012-5866639
**********|**********|**********/


#include <iostream>
#include <ctype.h>
#include <cstdlib>
#include <fstream>
#include <cstring>

using namespace std;

int temp1, temp2;
int mainmenu = true;
int newgame = true;	
int has_played = false;
int x_in_numbers = false;
int no_of_x = 2;
int no_of_o = 2;
int turn = 0;
int y_coordinate;
int pos_x;
int pos_y = 9;
int inputx;
char inputy;
char option;
char input[20];
char command;
char command2[20];
char line = '|';
char spaceb = ' ';
char dash = '-';
char plust = '+';
char currentplayer = 'X';
char g_x = 'X';
char g_o = 'O';
char board[18][18];
char numbers[8] = {'8','7','6','5','4','3','2','1'};
char letters[8] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'} ;
char buffer[2];
int legal = false;
int falsemove = false;
bool super1x = true;
bool super1o = true;
bool super2x = true;
bool super2o = true;
bool dosuper2x = false;
bool dosuper2o = false;
ifstream readsavefile;
ofstream writesavefile;
char filename[16];
// getting the index of an size 8 array given an element
int getposition(char letters[],int value) {
		for (int i = 0; i <= 7 ; i++)
		{
			if (letters[i] == value)
				return i;
		}		
	}	
// translate 18x18 coordinates into 8x8
int tboardx(int x) {
	x = (x + 1) * 2;
	return x;
}
int tboardy(int y) {	
	y = (y * 2) + 1;
	return y;
}
//inserting outlines into the 18x18 array
void initboard() {

		/*  Initialising the outlines of the board  */
	for (int i = 0; i <= 16; i++ ){ //ending at i = 16 to have space for the x-coordinates below the board
		for (int j = 1; j <= 17; j++){ // starting from j = 1 to have space for the y-coordinates on the left of the board.
			board[i][j] = dash;
			
			if (i % 2 != 0 && j % 2 != 0)  // all j conditions are having starting from j=1 in mind 
				board[i][j] = line;
			else if (j % 2 != 0 && i % 2 == 0 )
				board[i][j] = plust;
			else if (j % 2 == 0 && i % 2 != 0 )
				board[i][j] = spaceb;
			
		}		

	}
	
		// to initialise the coordinate guidelines on the left and below the board
	for (int i = 0; i <= 17; i++){
		if (i % 2 != 0){
			board[i][0] =  numbers[(i - 1) / 2] ; // conversion of i to display properly in a 8x8 board, from a 18x18 array
			board[17][i] = spaceb; // the space is needed to fill in the 18 x 18 array (when displaying the board, the space replaces the one in past memory) 
			}
		else if (i % 2 == 0){
			board[i][0] = spaceb;
			board[17][i] = letters[(i - 1) / 2]; 
			}
	}
	
	board[17][0] = spaceb; // Starting pieces


	
}	
// count the scores and change player
void countmarkers(int &x, int &y) {
	x = 0;
	y = 0;
	for (int i = 0; i < 18; i++) {
		for (int j = 0; j < 18; j++) {
			if ((board[i][j] == 'X') || (board[i][j] == 'x')) 
				x += 1;
			else if ((board[i][j] == 'O') || (board[i][j] == 'o'))
				y += 1;
		}
	}
	if (currentplayer == 'X') {                  // has_played is to make sure you place the marker before inputing n
			currentplayer = 'O';
			
		}
	else if (currentplayer = 'O'){
			currentplayer = 'X';

		}
	
}
// drawing the board
void drawboard () {
	
	
	for (int i = 0; i <= 17; i++ ){
		for (int j = 0; j <= 17; j++){
			cout << board[i][j];						
		}
		cout << endl;
	}	
}
// check for legality of move AND flip it in that direction, depending the the bool 'flip' in the parameter. Int m and n are the direction to check/flip to. 'Player' is to check legality for different players in a turn.
int checkAllDirections (int y , int x , int m , int n, bool flip = true, char player = currentplayer) {
	int allmoves[6][2];
	int count = 0;
	char notcurrentplayer;
	int count_towards = 1;
	int copyx ;
	int copyy ;
	bool superpiece = false;
	if (player == 'X')
		notcurrentplayer = 'O';
	else if (player == 'O')
		notcurrentplayer = 'X';
	
		
	if ((board[tboardy(y + m)][tboardx(x + n)] == notcurrentplayer) || (board[tboardy(y + m)][tboardx(x + n)] == tolower(notcurrentplayer))) {
		while (count_towards >= 0) {
		x = x + n ;
		y = y + m ;
		
		if ((board[tboardy(y)][tboardx(x)] != notcurrentplayer) && (board[tboardy(y)][tboardx(x)] != spaceb) && (board[tboardy(y)][tboardx(x)] != player) && (board[tboardy(y)][tboardx(x)] != 'x') && (board[tboardy(y)][tboardx(x)] != 'o'))
			break;
				
		if ((board[tboardy(y)][tboardx(x)] == notcurrentplayer) ||  (board[tboardy(y)][tboardx(x)] == tolower(notcurrentplayer)))  {
			allmoves[count][0] = y;
			allmoves[count][1] = x;
			count ++;
			if (board[tboardy(y)][tboardx(x)] == tolower(notcurrentplayer)) {
				copyx = x;
				copyy = y;
				superpiece = true;
			}
		}
		else if (board[tboardy(y)][tboardx(x)] == spaceb) {
			break;	
		}	
		else if ((board[tboardy(y)][tboardx(x)] == player) ||  (board[tboardy(y)][tboardx(x)] == tolower(player))) {
			legal = true;
			 if (flip == true) {
				 for (int i = 0; i < count; i++) {
					 board[tboardy(allmoves[i][0])][tboardx(allmoves[i][1])] = player;
				 }
				 if (superpiece == true)
					board[tboardy(copyy)][tboardx(copyx)] = tolower(notcurrentplayer);
			 }
			break;
		}
		}

	}
	

}
// flip markers in all directions and also check for legality
int flipmarkers(int y, int x, bool flip = true, char player = currentplayer) {

	legal = false;
		
	// going right
	checkAllDirections (y , x , 0 , 1,flip,player);

	// going left
	checkAllDirections (y , x , 0 , -1,flip,player);
	
	// going down
	checkAllDirections (y , x , 1 , 0,flip,player);
    
	//going up
	checkAllDirections (y , x , -1 , 0,flip,player);
	
	// going down-right
	checkAllDirections (y , x , 1 , 1,flip,player);

	/// going down-left
	checkAllDirections (y , x , 1 , -1,flip,player);

	// going up-right
	checkAllDirections (y , x , -1 , 1,flip,player);

	// going up-left
	checkAllDirections (y , x , -1 , -1,flip,player);


	if (legal == false )
		return false;
}	
// checking if a given move is legal in the position
int ifmovePossible(int y, int x, char player) {
	if (y > 7 || y < 0 || x > 7 || x < 0) { // gate checking for invalid coordinates
			return false;
		}  
	if (flipmarkers(y ,x, false, player) == false) { // gate checking for move legality
			return	false;		
		}
	if 	(board[tboardy(y)][tboardx(x)] != spaceb) {
		    return false;
	}
	else
		return true;
}
// checking is there are any legal moves left for the player
int ifanylegalmoves(char player) {
	for (int i = 0; i <= 7; i++) {
		for (int j = 0; j<= 7; j++ ) {
			if (ifmovePossible(i, j, player))
				return true;
		}		
	}	
}
int ifsuper1played(char player) {
	if (player == 'X' && super1x == true)
		return true;
	else if (player == 'O' && super1o == true)
		return true;	
	else
		return false;
} 	
int ifsuper2played(char player) {
	if (player == 'X' && super2x == true)
		return true;
	else if (player == 'O' && super2o == true)
		return true;	
	else
		return false;
} 	
int dosuper2(char player ) {
	if ((player == 'X') && (super2o == false) && (dosuper2o == true)) {
		dosuper2o = false;
		return true;
	}	
	else if ((player == 'O') && (super2x == false) && (dosuper2x == true)) {
		dosuper2x = false;
		return true;		
	}
	else
		return false;
}
int main()
{	 
	while (mainmenu == true){
	
	/* Main menu */
		system("cls");
		cout << endl;
		cout << "Type 'n' for New game, 'l' to Load game, 'h' for Help or 'e' to Exit " << endl;
		cout << endl;
		cout << "New Game" << endl;
		cout << "Load Game" << endl;
		cout << "Help" << endl;
		cout << "Exit" << endl;
		cout << endl;
		cin >> option;
		cout << endl;
		
	option = tolower(option); // to handle both upper and lower case
	if (option == 'n' || option == 'l'){
	newgame = true;
		/*  Initialising the outlines of the board  */
		if (option == 'n') {
			initboard();
			board[tboardy(4)][tboardx(3)] = 'X';
			board[tboardy(4)][tboardx(4)] = 'O';
			board[tboardy(3)][tboardx(3)] = 'O';
			board[tboardy(3)][tboardx(4)] = 'X';

		}
		
		else if (option == 'l') {
			system("cls");
			initboard();
			cout << endl << "Input the name of the save file" << endl << endl;
				cin >> filename;
				readsavefile.open(filename);
				if (readsavefile.fail()) {
					cout << endl << "Save file not detected, please enter the file extention as well. E.g. 'name.txt'" << endl;
					break;
				}
				for (int i = 0 ; i <= 7; i++ ) {
					for (int j = 0 ; j <= 7; j++ ) {
						readsavefile >> board[tboardy(i)][tboardx(j)];
						if (board[tboardy(i)][tboardx(j)] == '0')
							board[tboardy(i)][tboardx(j)] = ' ';
					}			
				}
				readsavefile >> currentplayer  >> super1o  >> super1x  >> super2o  >> super2x;
				readsavefile.close();
				countmarkers(no_of_x, no_of_o);
				countmarkers(no_of_x, no_of_o);
		}
		/* Starting of new game from the main menu */ 
		while (newgame == true){
		{	
			system("cls");
			inputx = ' '; // to reset the essential variables for a new loop
			command = ' ';
			strcpy(command2, " "); 
			falsemove = false; //
			
			/* Displaying the board*/
			drawboard();
			/* */ 
			if (ifanylegalmoves('O') != true && ifanylegalmoves('X') != true) {  // checking for winning condition
				cout << endl << "No legal moves detected"<< endl;
				if (no_of_o < no_of_x) {
					cout << endl << "X wins with " << no_of_x << " pieces to " << no_of_o << " pieces." << endl << endl;
				}
				else if (no_of_o > no_of_x) {
					cout << endl << "O wins with " << no_of_o << " pieces to " << no_of_x << " pieces." << endl << endl;
				}
				else 
					cout << endl << "It is a tie with " << no_of_o << "pieces each." << endl;
				system("pause");
				no_of_x = 2;
				no_of_o = 2;
				currentplayer = 'X';
				super1x = true;
				super1o = true;
				super2x = true;
				super2o = true;
				break;
			}
			
			// Options for play and display score
			cout << endl;
			cout << "Score:     O = " << no_of_o << "   X = "<< no_of_x << endl;
			cout << "Current Player : " << currentplayer << endl << endl << 
			"'p' to Place piece " << endl << "'n' for Next player " << endl << "'s' to Save " << endl << "'super1' for 1st superpower"  << endl << "'super2' for 2nd superpower" << endl <<  "'m' for Main menu "<< endl << endl; 
			cin >> command2;
			cout << endl;
			if (strlen(command2) == 1) 
				command = command2[0];
			//
			
			// gate checking for invalid command
			while ((command != 'p') && (command != 'm') && (command != 'n' ) && (command != 's' ) && (strlen(command2) != 6 )) { 
				cout << "Please input the correct command" << endl << endl;
				cin >> command2;
				cout << endl;
				if (strlen(command2) == 1) 
					command = command2[0];				
			}
			//
			
			if (command == 'p' || ((strcmp(command2, "super1") == 0) && ifsuper1played(currentplayer)) || ((strcmp(command2, "super2") == 0) && ifsuper2played(currentplayer))){ // Start of placing pieces, if super1 chosen, place pieces as usual

				/* Super power section*/
				if (strcmp(command2, "super2") == 0)   { 
					if (currentplayer == 'O') {
						super2o = false;
					}
					else if (currentplayer == 'X') {
						super2x = false;
					}
					cout << "You opponent next turn will not be able to flip anything with his piece" << endl << endl;

				}
				// How superpower 1 works 
				/* 
				The function checkAllDirections will treat 'x' and 'o' as a regular piece, but will detect and save the 'x' or 'o' piece's coordinate. The function then reassign the 'x' or 'o' piece back into it's
				spot after the flipping is done.
				*/
				//
				// How superpower 2 works 
				/* 
				Check if a player used super2, dosuper2 allows one turn  for the opponent piece not to flip by making it false in another function. dosuper2 will never be true for the 2nd time. E.g.
				Turn 0: dosuper2o == false; super2o == true;
				Turn 1: Player O uses super2, dosuper2o == true , super2o == false; 
				Turn 2:  Player X places pieces , dosuper2o == false, super2o == false;
				- dosuper2o == false, super2o == false for the rest of the game.
				- The function dosuper2 only return true for one turn only in a game, which is when dosuper2o is true; it will never be true for the second time in a game because the game will not go through
				when super2o is false
				*/
				//
				else if (strcmp(command2, "super1") == 0)
					cout << endl << "Your next piece placed can't be flipped" << endl << endl ;
				
				/* */
				
				if (ifanylegalmoves(currentplayer) != false) {									
					while (ifmovePossible(inputx, y_coordinate, currentplayer) == false) { // the block of code where you input the moves
							if (dosuper2o == true || dosuper2x == true ) {
								cout << endl << "Your piece can be placed as usual but will not flip anything." << endl << endl;
							}
							if (falsemove == false)
								cout << endl << "Enter the x-coordinate followed by the y-coordinate" << endl << endl;
							else if (falsemove == true)
								cout << endl << "Please input the correct x and y -coordinates or ensure it is a legal move!" << endl << endl;
							cin >> input;
							inputy = input[0];
							buffer[0] = input[1];
							inputx = atoi(buffer);
							y_coordinate = getposition(letters, inputy);
							inputx = 8 - inputx	;
							falsemove = true;
					}
				}
				
				
				else {
					cout << endl << "You have no legal moves, your turn will be passed." << endl << endl;
					system("pause");
				}
				
				if (strcmp(command2, "super2") == 0)   { 
					if (currentplayer == 'O') {
						dosuper2o = true;
					}
					else if (currentplayer == 'X') {
						dosuper2x = true;
					}
					cout << "You opponent next turn will not be able to flip anything with his piece" << endl << endl;

				}
					
				//				
				if (strcmp(command2, "super1") == 0) { // insert indestructable piece for super1 and make the player cant use it again
					board[tboardy(inputx)][tboardx(y_coordinate)] = tolower(currentplayer);
					if (currentplayer == 'X')
						super1x = false;
					else if (currentplayer == 'O')
						super1o = false;
				}
				else
					board[tboardy(inputx)][tboardx(y_coordinate)] = currentplayer;
				//
				
				if (dosuper2(currentplayer) == false) // if dosuper2 activated, do not flip for the opponent 
					flipmarkers(inputx ,y_coordinate); // this flips pieces when super2 isnt played

				countmarkers(no_of_x, no_of_o);		// counts piece up for regular play		
			}
			 
			else if (command == 's') {// save file
				cout << endl << "Name your file" << endl << endl;
				cin >> filename;
				writesavefile.open(filename);
				for (int i = 0 ; i <= 7; i++ ) {
					for (int j = 0 ; j <= 7; j++ ) {
						if (board[tboardy(i)][tboardx(j)] == spaceb)
							writesavefile << '0';
						else
							writesavefile << board[tboardy(i)][tboardx(j)];			
					}			
				}
				writesavefile << ' ' << currentplayer << ' ' << super1o << ' ' << super1x << ' ' << super2o << ' ' << super2x;
				writesavefile.close();
			}
			
			else if (command == 'm') // to quit to the main menu
				{system("cls");
				newgame = false;
				no_of_x = 2;
				no_of_o = 2;
				currentplayer = 'X';
				super1x = true;
				super1o = true;
				super2x = true;
				super2o = true;
				break;
				}
				
			
			else if (command == 'n' ){ // to go to the next player, the 'o' and 'x' pieces count upwards and the next marker changes here 
					if (ifanylegalmoves(currentplayer) == false) 
						countmarkers(no_of_x, no_of_o);
					else {
						cout << "You cant skip if there is still a legal move for you." << endl << endl;
						system("pause");
					}
				}
			
			else if ((strcmp(command2, "super1") == 0) && (ifsuper1played(currentplayer) == false)) { // display text if the superpower has already been played
				cout << "This superpower has already been played by the current player" << endl << endl;
				system("pause");
			}
			
			else if ((strcmp(command2, "super2") == 0) && (ifsuper2played(currentplayer) == false)) { // display text if the superpower has already been played
				cout << "This superpower has already been played by the current player" << endl << endl;
				system("pause");
			}

			cout << endl;
		}
		}
	}
	else if (option == 'h'){ // help menu
		system("cls");
		
		cout << " HELP " << endl;
		cout << "--------" << endl;
		cout << "The game follows the basic rules of othello" << endl << endl;
		cout << "Superpower 1 allows you to place a piece as usual, the only difference is that your piece will not flip permanently" << endl << endl;
		cout << "Superpower2 allows you to make a move, and during your opponent turn he wont be able to flip any pieces over for one turn, but he can still place the piece as usual." << endl << endl;
		system("pause");
	}
	
	else if (option == 'e') // exit the program
		mainmenu = false;
	
	else // catch invalid inputs in 'mainmenu'
		cout << "Please enter a valid option";
	cout << endl;
	}
}