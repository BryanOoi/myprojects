#include "Culture.h"

Culture :: Culture(string name = "Null", int id = 0, int typeID = 0, double entranceFee= 0): Attraction(name, id, typeID), entranceFee(entranceFee){}
void Culture :: display()/* Shows the important info for the attraction*/ { 
	cout << "This cultural attraction has an entrance fee of RM " << entranceFee << endl;
	cout << getDetail() << endl;
	}
	
double Culture :: getentranceFee(){return entranceFee;};