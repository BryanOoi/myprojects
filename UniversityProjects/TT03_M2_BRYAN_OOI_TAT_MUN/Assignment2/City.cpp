#include "City.h"


City :: City(string name = "Null", int id = 0) : name(name), id(id) {}

string City :: getName(){return name;}

void City :: insertAttraction(Attraction *a){
	/* Insert a pointer of an attraction class into the 'attractions' vector*/
	attractions.insert(attractions.getLength() + 1,a);
}

// void City :: insertConnectingCity(int a){ConnectedTo.insert(1,a);}

// int* City :: returnConnectingCity(int &finalSize){
	// int size = 1; 
	// int* arrayofIds = NULL;
	// arrayofIds = new int[size];
	// for(int i = 0 ; i < ConnectedTo.getLength(); i++){
		// arrayofIds[i] = ConnectedTo[i];
		// size += 1;
	// }
	// finalSize = size - 1;
	// return arrayofIds;
// };

LinkedList<Attraction*> City :: getAttraction(){return attractions;}

int City :: getid(){return id;}

void City :: setid(int idToSet){id = idToSet;};

void City :: changeCName(string changename){name = changename;}

bool City :: operator >(City city){
	if (name > city.getName())
		return true;
	else
		return false;
		
};