#ifndef _Attraction
#define _Attraction
#include <string>

using namespace std; 

class Attraction { // abstract class. Child classes are Sport, Culture and Shopping.
private:
	int id;
	string name;
	int typeID;
	string details;
public:
	virtual void display() = 0; 
	Attraction(string name,int id,int typeID);
	int getid();
	int gettypeID();
	string getname();
	string getDetail();
	void addDetail(string addeddetail);
	void setid(int idToSet);
	bool operator >(Attraction* attraction); // for sorting
};

#include "Attraction.cpp"
#endif