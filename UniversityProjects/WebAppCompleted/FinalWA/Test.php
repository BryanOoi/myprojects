<?php

echo password_hash("rasmuslerdorf", PASSWORD_BCRYPT)."\n";

$hash = password_hash("rasmuslerdorf", PASSWORD_BCRYPT);

if (password_verify('rasmuslerdorf', $hash)) {
    echo 'Password is valid!';
} else {
    echo 'Invalid password.';
}
?>