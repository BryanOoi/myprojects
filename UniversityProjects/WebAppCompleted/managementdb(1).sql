-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2017 at 05:02 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `managementdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(255) NOT NULL,
  `event_type` varchar(255) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_venue` varchar(255) NOT NULL,
  `event_StartDate` date NOT NULL,
  `event_EndDate` date NOT NULL,
  `event_StartTime` time NOT NULL,
  `event_EndTime` time NOT NULL,
  `event_desc` longtext NOT NULL,
  `event_website` varchar(255) NOT NULL,
  `event_organizer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_type`, `event_name`, `event_venue`, `event_StartDate`, `event_EndDate`, `event_StartTime`, `event_EndTime`, `event_desc`, `event_website`, `event_organizer`) VALUES
(1, 'EventType', 'EventName', 'EventVenue', '0000-00-00', '0000-00-00', '00:00:00', '00:00:00', 'EventDesc', 'EventWebsite', 'Jay the power Ranger'),
(2, 'Sports', 'test', 'test', '2014-02-09', '2014-02-10', '23:59:00', '23:59:00', ' Enter event description here. ', 'http://www.domainname.com', 'Jay the powerpuff girl'),
(3, 'Sports', 'test', 'test', '2014-02-09', '2014-02-10', '23:59:00', '23:59:00', ' Enter event description here. ', 'http://www.domainname.com', ''),
(4, 'Sports', 'test', 'test', '2014-02-09', '2014-02-10', '23:59:00', '23:59:00', ' Enter event description here. ', 'http://www.domainname.com', 'jayjay'),
(5, 'Sports', 'ds', 'd', '2018-01-01', '2014-02-10', '01:00:00', '23:59:00', ' dsad', 'www.test.com', 'bryan');

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `user_id` varchar(255) NOT NULL,
  `event_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`user_id`, `event_id`) VALUES
('1', '1'),
('1', '2'),
('1', '4'),
('12345', '4'),
('164173889', '4'),
('365720824', '5'),
('920511168', '4'),
('userID2', '1'),
('userID2', '2');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `username`, `password`) VALUES
(1, 'jayjay', '123'),
(12345, 'abc', '123'),
(20121154, 'jayjay2', '12321312321312'),
(164173889, 'daniel2', '123'),
(365720824, 'bryan2', '$2y$10$poKGoKJ3Mv5Cbj8fEQ91XuVY93V05AT0EySjHSeIP2qFZ/AzhPVqe'),
(378772582, 'jayjay', '12'),
(554790038, 'jayjay', 'asdasdsad'),
(562071227, '` OR 1=1; /*', '$2y$10$kBuB4x6Zamt1xkp.e6HOieq19ovbKNcDI6ZAokV1gTYejpfCTsJXe'),
(635396727, 'asd', '123a'),
(712136229, 'test;--', '$2y$10$svw32e2BJkgfE6y9zI0GnOPealMKqLD2MQzgfMJMYCMXE4lwYXMEC'),
(739599303, 'Bryan', '$2y$10$pwU1ble3j9zkHcsFI4/w3u0E.0538ncsZoaU9nV2Hb8HQlMgoAoum');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`user_id`,`event_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=739599304;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
