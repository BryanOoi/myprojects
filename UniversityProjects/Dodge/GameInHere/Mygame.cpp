/********************************************
Course : TGD2251 Game Physics
Session: Trimester 1, 2016/17
ID and Name #1 : 1141328133 Bryan Ooi Tat Mun
Contacts #1 : 011-55046057 bryanotm@hotmail.com

********************************************/
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream> 

const float pi = 3.14159f;
const int gameWidth = 800;
const int gameHeight = 600;
bool stopGame = false;
int score = 0;

class Score{
	
public:
    sf::Text CurrentScore;
	sf::Font font;
	
	Score(){
		
	
		//std::string tempValue = std::string(intStr);
		//std::string tempValue = std::to_string(score);
		font.loadFromFile("resources/sansation.ttf");
		CurrentScore.setFont(font);
		CurrentScore.setCharacterSize(10);
		CurrentScore.setPosition(5.f, 5.f);
		CurrentScore.setFillColor(sf::Color::White);
		CurrentScore.setString("Current Score: 0" );			
		
	}
	
	void incrementScore(int& score2){
		score2++;
		std::stringstream ss;
		ss << score2;
		std::string tempValue = ss.str();			
		CurrentScore.setString("Current Score: " + tempValue);		
	}
	
	void resetScore(){
		
		CurrentScore.setString("Current Score: 0" );
	}

	
};

Score Displayscore;

class Vehicle{
private:
	
	int OutlineThickness;
	sf::Color OutlineColor;
	sf::Color FillColor;
		


	
	
	
public:
	int shipVel = 10;
	float shipAngleVel = 10;
	sf::CircleShape ship;
	float shipAngle         = 0.f;
	int shipRadius = 50;

	
	bool changeBulletDir ;
	int shootingAngle ;
	sf::RectangleShape bullet;
	int bulletVel = 10;
	bool isAlive = true;
	
	sf::Texture VehicleImage;
	
	sf::Clock timeBulletOnScreen;
	
	
    Vehicle() // set ballRadius
    {
		
		OutlineThickness = 3;
		OutlineColor = sf::Color::Black;
		FillColor = sf::Color::Red;
		
		VehicleImage.loadFromFile("resources/texture.png");	
		
		ship.setPointCount(3);
		ship.setRadius(shipRadius);
		ship.setOutlineThickness(OutlineThickness);
		ship.setOutlineColor(OutlineColor);
		ship.setFillColor(FillColor);
		ship.setOrigin(shipRadius , shipRadius );
		ship.setPosition(gameWidth -100,  gameHeight - 100 );
		ship.setTexture(&VehicleImage);


		
		bullet.setSize(sf::Vector2f(50, 10));
		//bullet.setRadius(shipRadius);
		//bullet.setOrigin(shipRadius , shipRadius );
		bullet.setFillColor(sf::Color::White);
		bullet.setPosition(900,900 );
		

			
		
    };
		
		
	void move(){
		
		sf::Event event;


		
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Up))
		{
			ship.move(shipVel,0);
		}
		
		
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Right))
		{
			ship.rotate(shipAngleVel);
		}		
				
		
		
	}
	
	void shoot(){
		
		
		float newX = std::sin((ship.getRotation()) * (pi/180.0) );
		float newY = std::cos((ship.getRotation()) * (pi/180.0)) ;		
		//std::cout << ship.getRotation() << " " << newX << " " << newY << " " << ship.getPosition().x << " " << ship.getPosition().y << " " << bullet.getPosition().x << " " << bullet.getPosition().y << std::endl;
		
		
		bullet.setPosition(ship.getPosition().x, ship.getPosition().y);
		bullet.move(newX  * shipRadius, -newY * shipRadius); // draw it at the vertice of ship
		

		bullet.setRotation(ship.getRotation() - 90);
		
		
		changeBulletDir = true;

		
	}
	
	void moveBullet(float timeElapsed){
		float factor = bulletVel * timeElapsed;
		
		//bullet.move(std::sin((ship.getRotation()) * (pi/180.0)) * factor, std::cos((ship.getRotation()) * (pi/180.0)) * factor);
		if (changeBulletDir == true)
			shootingAngle = ship.getRotation();
		bullet.move(std::cos((shootingAngle - 90) * (pi/180.0) ) * bulletVel
					,std::sin((shootingAngle- 90) * (pi/180.0) ) * bulletVel);
							
	}


	
};

class Target{
	
public:
	sf::RectangleShape target;
	Target(){
		target.setSize(sf::Vector2f(50, 10));
		//bullet.setRadius(shipRadius);
		//bullet.setOrigin(shipRadius , shipRadius );
		target.setFillColor(sf::Color::Red);
		target.setPosition(gameWidth - 10,gameHeight/2 + 25 );
		target.rotate(-90);
	}
};

class cBall{
private:
	
	int OutlineThickness;
	sf::Color OutlineColor;
	sf::Color FillColor;

	
	
	
public:
	sf::CircleShape ball;
	float ballAngle         = 0.f;
	float ballSpeed   = 400.f;
	float ballRadius ;
	bool static continuePlaying;
	
	
	// load  collision ball sound
	
	
	
    cBall(int ballRad) // set ballRadius
    {
		
		
		OutlineThickness = 3;
		OutlineColor = sf::Color::Black;
		FillColor = sf::Color::White;
		ballRadius = ballRad;
		
		ball.setRadius(ballRad);
		ball.setOutlineThickness(OutlineThickness);
		ball.setOutlineColor(OutlineColor);
		ball.setFillColor(FillColor);
		ball.setOrigin(ballRadius , ballRadius  );
    };
	
	sf::Sound loadSound(){
		sf::SoundBuffer ballSoundBuffer;
		ballSoundBuffer.loadFromFile("resources/ball.wav");
		sf::Sound ballSound(ballSoundBuffer);	
		return ballSound;
	}
	
	void moveBall(float deltaTime){
		float factor = ballSpeed * deltaTime;
		ball.move(std::cos(ballAngle) * factor, std::sin(ballAngle) * factor);
		
	}
	
	bool ballIntersectsRect(sf::CircleShape circle, sf::RectangleShape rect)
	{
		float circleDistance_x = abs(circle.getPosition().x - rect.getPosition().x);
		float circleDistance_y = abs(circle.getPosition().y - rect.getPosition().y);

		if (circleDistance_x > (rect.getSize().x/2 + circle.getRadius())) { return false; }
		if (circleDistance_y > (rect.getSize().y/2 + circle.getRadius())) { return false; }

		if (circleDistance_x <= (rect.getSize().x/2)) { return true; } 
		if (circleDistance_y <= (rect.getSize().y/2)) { return true; }

		float cornerDistance_sq = std::pow((circleDistance_x - rect.getSize().x/2),2) +
							 std::pow((circleDistance_y - rect.getSize().y/2),2);

		return (cornerDistance_sq <= std::pow((circle.getRadius()),2));
	}

	bool ballIntersectsVehicle(sf::CircleShape circle,sf::CircleShape circle2){
		int rSquared = circle.getRadius() + circle2.getRadius();
		int distance = sqrt(
						((circle.getPosition().x - circle2.getPosition().x) * (circle.getPosition().x - circle2.getPosition().x))
						+ ((circle.getPosition().y - circle2.getPosition().y) * (circle.getPosition().y - circle2.getPosition().y))
						);
		return distance < rSquared;		
		
	}

	bool checkCollisions(int gameWidth,int gameHeight,sf::Sound ballSound, Vehicle ship, Target target ){
		const float pi = 3.14159f;
		

		

		// Check collisions between the ball and the screen
		if (ball.getPosition().x  < 0.f)
		{	
			
			ballSound.play();
			
			//cball.ballAngle = -cball.ballAngle;
			ballAngle = pi - ballAngle;
			ball.setPosition(ballRadius - (0.8 * ballRadius), ball.getPosition().y);
			return true;
			//pauseMessage.setString("You lost!\nPress space to restart or\nescape to exit");
		}
		if (ball.getPosition().x + ballRadius > gameWidth)
		{
			
			ballSound.play();
			//cball.ballAngle = -cball.ballAngle;
			ballAngle = pi - ballAngle;
			ball.setPosition(gameWidth - ballRadius - 0.1f, ball.getPosition().y);
			return true;
			//pauseMessage.setString("You won!\nPress space to restart or\nescape to exit");
			
		}
		
		if (ballIntersectsRect(ball,target.target)){ // collision between target and ball
			ballAngle = ((std::rand() % 270) + 180) * pi / 360;
			ballSpeed += 10;
			// score++;
			// Displayscore.CurrentScore.setString("DSADSA");
			Displayscore.incrementScore(score);
			 // arg is from global var
		}
		
		if (ball.getPosition().y  < 0.f)
		{
			
			ballSound.play();
			ballAngle = -ballAngle;
			ball.setPosition(ball.getPosition().x, ballRadius - (0.8 * ballRadius));
			return true;
			
		}
		if (ball.getPosition().y + ballRadius > gameHeight)
		{
		
			ballSound.play();
			ballAngle = -ballAngle;
			ball.setPosition(ball.getPosition().x, gameHeight - ballRadius - 0.1f);
			return true;
			
			
		}

		if (ballIntersectsRect(ball,ship.bullet)){ // collision between bullet and ball
			
			// ship.shootingAngle;
			// int resultantAngle = ballAngle * (180/pi) - ship.shootingAngle;
			//
			// ballAngle = resultantAngle % 360 * (pi/180);
			
			// sf::Vector2<float> ballAngleToAdd (std::cos(ballAngle-pi/2),std::sin(ballAngle-pi/2));
			// sf::Vector2<float> bulletAngleToAdd (std::cos(ship.shootingAngle*pi/180),std::sin(ship.shootingAngle*pi/180));
			// sf::Vector2<float> resultantAngle = ballAngleToAdd + bulletAngleToAdd;
			// std::cout << ballAngle << " ";
			// ballAngle = std::atan2(resultantAngle.y,resultantAngle.x);
			// std::cout << ballAngle << std:: endl;
			
			ballAngle = ship.shootingAngle * pi/180 - pi/2;
			
		}
		
		if (ballIntersectsVehicle(ball,ship.ship)){
			//std::cout << "Dead" << std::endl;
			stopGame = true;
		}
		
		//ship.isAlive = true;
		return false;
		
	}

};




int main()
{

	sf::Font font;
	
	std::srand(static_cast<unsigned int>(std::time(NULL)));
	
	// create window
    sf::RenderWindow window(sf::VideoMode(gameWidth, gameHeight, 60), "SFML Pong",
                            sf::Style::Titlebar | sf::Style::Close);
    window.setVerticalSyncEnabled(true);		
	
	// load sound
	
	sf::SoundBuffer ballSoundBuffer;
	ballSoundBuffer.loadFromFile("resources/ball.wav");
	sf::Sound ballSound(ballSoundBuffer);	
	
	// load text font 
	
	font.loadFromFile("resources/sansation.ttf");	
	
	// initialise pause message
	sf::Text pauseMessage;
	pauseMessage.setFont(font);
	pauseMessage.setCharacterSize(40);
	pauseMessage.setPosition(170.f, 150.f);
	pauseMessage.setFillColor(sf::Color::White);
	pauseMessage.setString("Welcome to Dodge!\nPress space to start the game");			



	cBall cball(50.0f); // ball radius as parameter
	cBall cball2(50.0f);
	cBall cball3(50.0f);
	Vehicle ship;
	Target target;

	

	sf::Clock clock;
	bool isPlaying = false;
	while (window.isOpen())
		{
			// Handle events
			sf::Event event;
			while (window.pollEvent(event))
			{
				// Window closed or escape key pressed: exit
				if ((event.type == sf::Event::Closed) ||
				   ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape)))
				{
					window.close();
					break;
				}

				// Space key pressed: play
				if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
				{
					if (!isPlaying)
					{
						// (re)start the game
						isPlaying = true;
						stopGame = false;
						clock.restart();

						// Reset the position of the vehicle and ball

						cball.ball.setPosition(gameWidth / 2, 1.0/2 * gameHeight );
						cball2.ball.setPosition(gameWidth / 2, 1.0/2 * gameHeight );
						cball3.ball.setPosition(gameWidth / 2, 1.0/2 * gameHeight );
						cball.ballSpeed = 400.0f;
						cball2.ballSpeed = 400.0f;
						cball3.ballSpeed = 400.0f;
						ship.ship.setPosition(gameWidth -100,  gameHeight - 100 );
						ship.ship.setRotation(0);
						ship.bullet.setPosition(900,900 );
						score = 0;
						Displayscore.resetScore();
						

						// Reset the ball angle
						do
						{
							// Make sure the ball initial angle is not too much vertical
							cball.ballAngle = (std::rand() % 360) * 2 * pi / 360;
							cball2.ballAngle = (std::rand() % 360) * 2 * pi / 360;
							cball3.ballAngle = (std::rand() % 360) * 2 * pi / 360;
							// cball.ballAngle = 0;
							// cball2.ballAngle = 0;
							// cball3.ballAngle = 0;							
						}
						while (std::abs(std::cos(cball.ballAngle)) < 0.7f);
					}
				}
			}

			if (isPlaying)
			{
				float deltaTime = clock.restart().asSeconds();

			

				if (event.type == sf::Event::KeyPressed){
					if (event.key.code == sf::Keyboard::Up){
						int displaceX = std::cos((ship.ship.getRotation() - 90) * (pi/180.0) ) * ship.shipVel;
						int displaceY = std::sin((ship.ship.getRotation() - 90) * (pi/180.0) ) * ship.shipVel;
						//ship.ship.move(std::cos((ship.ship.getRotation() - 90) * (pi/180.0) ) * ship.shipVel
										//,std::sin((ship.ship.getRotation() - 90) * (pi/180.0) ) * ship.shipVel);
						if (!(ship.ship.getPosition().x + displaceX < 0.f || ship.ship.getPosition().x + displaceX + ship.shipRadius > gameWidth || ship.ship.getPosition().y + displaceY  < 0.f || ship.ship.getPosition().y + displaceY +  ship.shipRadius > gameHeight))
							ship.ship.move(displaceX,displaceY); // detection between edge of screen with ship
					}
					if (event.key.code == sf::Keyboard::Right){
						ship.ship.rotate(ship.shipAngleVel);
						ship.changeBulletDir = false;
					}
					if (event.key.code == sf::Keyboard::Left){
						ship.ship.rotate(-ship.shipAngleVel);
						ship.changeBulletDir = false;
						
					}
					if (event.key.code == sf::Keyboard::Down){
						//ship.bullet.move(10,10);
						ship.shoot();
						
					}
				}
				
				
				// Move the ball
				cball.moveBall(deltaTime);
				cball2.moveBall(deltaTime);
				cball3.moveBall(deltaTime);
				
				//Move the bullet
				ship.moveBullet(deltaTime);
				
				
				//Check collisions between the ball and the screen
				if (cball.checkCollisions(gameWidth,gameHeight,ballSound,ship,target))
					ballSound.play();
				if (cball2.checkCollisions(gameWidth,gameHeight,ballSound,ship,target))
					ballSound.play();
				if (cball3.checkCollisions(gameWidth,gameHeight,ballSound,ship,target))
					ballSound.play();
				
				// check if dead
				isPlaying = !stopGame;
				
				// Game Over Message
				if (stopGame){
					std::stringstream ss;
					ss << score;
					std::string tempValue = ss.str();
					pauseMessage.setString("GameOver\n Your Score is "+tempValue + "\n Space to continue");					
				}
				
				ship.move();

			}

			// Clear the window
			window.clear(sf::Color(50, 200, 50));

			if (isPlaying)
			{
				// Draw the paddles and the ball

				window.draw(cball.ball);
				window.draw(cball2.ball);
				window.draw(cball3.ball);
				window.draw(ship.ship);
				window.draw(ship.bullet);
				window.draw(target.target);
				window.draw(Displayscore.CurrentScore);
			}
			else
			{
				// Draw the pause message

				window.draw(pauseMessage);
			}

			// Display things on screen
			window.display();
		}
	
}

