/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tocassignment;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;

/**
 * @author Patrick
 */
public class CNFtoCYK extends javax.swing.JFrame {
    public CNFtoCYK() {
        initComponents();
    }

	public static class Converter
	{
		// INSTANCE VARIABLES//
		char[] Alphabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
				'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' }; // alphabet array
		ArrayList<Character> currectChar = new ArrayList<Character>();
		ArrayList<String> CNF = new ArrayList<String>();// final string
		int size;// size of temp array from string manipulation

		public Converter()
		{
                            //initComponents();
			this.CNF = new ArrayList<String>();
		}

		public ArrayList<String> convert(ArrayList<String> original)
		{
			ArrayList<Line> temp = new ArrayList<Line>();
			size = original.size();

			for (int i = 0; i < original.size(); i++)
			{
				if (!Character.isLetter(original.get(i).charAt(0)))
				{
					return CNF = null;
				} else if (!Character.isUpperCase(original.get(i).charAt(0)))
				{
					return CNF = null;
				} else if (original.get(i).charAt(1) != '-')
				{
					return CNF = null;
				} else if (original.get(i).charAt(2) != '>')
				{
					return CNF = null;
				} else if (original.get(0).charAt(3) == '\\')
				{
					CNF.add("S0->\\");
					String One = original.get(0).substring(0, original.get(0).indexOf('>') + 1);
					CNF.add(One);
					return CNF;
				}

				// add to temp arrayList
				temp.add(new Line(original.get(i)));
				// populate Variable tracking arrayList
				for (int l = 0; l < original.get(i).length(); l++)
				{
					if (Character.isUpperCase(original.get(i).charAt(l)))
					{
						if (!currectChar.contains(original.get(i).charAt(l)))
						{
							currectChar.add(original.get(i).charAt(l));
						}

					}
				}
			}
			// Populate Section of Line array with each lines respective
			for (int i = temp.size() - 1; i >= 0; i--)
			{
				int begin = temp.get(i).str.indexOf('>') + 1;

				if (temp.get(i).indexesOfDelimiters.size() == 0)
				{
					temp.get(i).sectionsOfLine.add(new Section(temp.get(i).str.substring(begin)));
				} else if (temp.get(i).indexesOfDelimiters.size() == 1)
				{
					temp.get(i).sectionsOfLine
							.add(new Section(temp.get(i).str.substring(begin, temp.get(i).indexesOfDelimiters.get(0))));
					temp.get(i).sectionsOfLine
							.add(new Section(temp.get(i).str.substring(temp.get(i).indexesOfDelimiters.get(0) + 1)));
				} else if (temp.get(i).indexesOfDelimiters.size() > 1)
				{
					temp.get(i).sectionsOfLine
							.add(new Section(temp.get(i).str.substring(begin, temp.get(i).indexesOfDelimiters.get(0))));

					for (int k = 0; k < temp.get(i).indexesOfDelimiters.size(); k++)
					{
						if (k + 1 < temp.get(i).indexesOfDelimiters.size())
						{
							temp.get(i).sectionsOfLine.add(
									new Section(temp.get(i).str.substring(temp.get(i).indexesOfDelimiters.get(k) + 1,
											temp.get(i).indexesOfDelimiters.get(k + 1))));
						} else
						{
							temp.get(i).sectionsOfLine.add(
									new Section(temp.get(i).str.substring(temp.get(i).indexesOfDelimiters.get(k) + 1)));
						}
					}
				}
			}
			//no Epsilons
			char tempChar = '0';
			boolean flag = false;
			boolean check = true;
			while (checkForEpsilon(temp) && check)
			{
				for (int i = temp.size() - 1; i >= 0; i--)
				{
					for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
					{

						int tempIndex = temp.get(i).sectionsOfLine.get(j).characters.indexOf('\\');

						if (tempIndex != -1 && i == 0)
						{
							tempChar = temp.get(i).str.charAt(0);
							temp.get(i).sectionsOfLine.remove(j);
							size = temp.size();
							check = false;

						} else if (tempIndex != -1 && i != 0)
						{
							tempChar = temp.get(i).str.charAt(0);
							temp.get(i).sectionsOfLine.remove(j);
							size = temp.size();
							flag = true;
							break;
						}
					}
					if (flag)
					{
						flag = false;
						break;
					}
				}

				if (tempChar != '0')
				{
					for (int k = 0; k < temp.size(); k++)
					{
						for (int j = 0; j < temp.get(k).sectionsOfLine.size(); j++)
						{

							if (temp.get(k).sectionsOfLine.get(j).characters.contains(tempChar))
							{
								int tempIndex = temp.get(k).sectionsOfLine.get(j).characters.indexOf(tempChar);

								if (tempIndex != -1)
								{

									if (tempIndex == 0 && temp.get(k).sectionsOfLine.get(j).characters.size() == 1)
									{

										if (k != 0)
										{
											temp.get(k).sectionsOfLine.add(new Section("\\"));
										}

									} else if (tempIndex == 0
											&& temp.get(k).sectionsOfLine.get(j).characters.size() == 2)
									{
										String tempString = temp.get(k).sectionsOfLine.get(j).characters.toString()
												.replaceAll("[,\\s\\[\\]]", "");
										temp.get(k).sectionsOfLine.add(new Section("" + tempString.substring(1)));

									} else if (tempIndex != 0
											&& temp.get(k).sectionsOfLine.get(j).characters.size() == 2)
									{
										String tempString = temp.get(k).sectionsOfLine.get(j).characters.toString()
												.replaceAll("[,\\s\\[\\]]", "");
										temp.get(k).sectionsOfLine.add(new Section("" + tempString.substring(0, 1)));

									} else if (temp.get(k).sectionsOfLine.get(j).characters.size() > 2)
									{
										String tempString = temp.get(k).sectionsOfLine.get(j).characters.toString()
												.replaceAll("[,\\s\\[\\]]", "");

										ArrayList<Integer> indexListOfTempChar = new ArrayList<Integer>();

										int index = tempString.indexOf(tempChar);
										while (index >= 0)
										{
											indexListOfTempChar.add(index);

											index = tempString.indexOf(tempChar, index + 1);

										} // while

										if (indexListOfTempChar.size() == 1)
										{
											if (tempIndex == 0)
											{
												temp.get(k).sectionsOfLine
														.add(new Section("" + tempString.substring(1)));
											} else if (tempIndex != 0 && tempIndex == tempString.length())
											{
												temp.get(k).sectionsOfLine
														.add(new Section("" + tempString.substring(0, tempIndex)));
											} else if (tempIndex != 0)
											{
												temp.get(k).sectionsOfLine
														.add(new Section("" + tempString.substring(0, tempIndex)
																+ tempString.substring(tempIndex + 1)));
											}
										}

										if (indexListOfTempChar.size() > 1)
										{
											if (indexListOfTempChar.get(0) != tempString.charAt(0))
											{
												temp.get(k).sectionsOfLine.add(new Section(
														"" + tempString.substring(0, indexListOfTempChar.get(1))));
											}

											for (int t = 0; t < indexListOfTempChar.size(); t++)
											{
												if (t + 1 < indexListOfTempChar.size())
												{
													temp.get(k).sectionsOfLine.add(new Section(
															"" + tempString.substring(indexListOfTempChar.get(t),
																	indexListOfTempChar.get(t + 1))));
													temp.get(k).sectionsOfLine.add(new Section(
															"" + tempString.substring(indexListOfTempChar.get(t) + 1,
																	indexListOfTempChar.get(t + 1) + 1)));
												} else
												{
													if (tempString.charAt(indexListOfTempChar.get(t)) != tempString
															.charAt(tempString.length() - 1))
													{
														temp.get(k).sectionsOfLine.add(new Section(
																"" + tempString.substring(indexListOfTempChar.get(t))));
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			removeDuplicates(temp);
			fixStrings(temp);
			// Remove Start Variable from Variables to lead to it
			for (int i = 0; i < temp.size(); i++)
			{
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					if (i == 0)
					{
						if (temp.get(i).sectionsOfLine.get(j).part.length() == 1)
						{
							if (temp.get(i).sectionsOfLine.get(j).part.charAt(0) == temp.get(0).str.charAt(0))
							{
								temp.get(i).sectionsOfLine.remove(j);
							}
						}
					} else
					{
						if (temp.get(i).sectionsOfLine.get(j).part.length() == 1)
						{
							if (temp.get(i).sectionsOfLine.get(j).part.charAt(0) == temp.get(0).str.charAt(0))
							{
								temp.get(i).sectionsOfLine.remove(j);
								temp.get(i).sectionsOfLine.addAll(temp.get(0).sectionsOfLine);
							}
						}
					}
				}
			}

			fixStrings(temp);
			// Remove Sections of 3 Characters or More
			for (int i = 0; i < temp.size(); i++)
			{
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					if (temp.get(i).sectionsOfLine.get(j).part.length() > 2)
					{
						int math = temp.get(i).sectionsOfLine.get(j).part.length() / 2;
						int count = 0;
						while (count < math)
						{
							char newCharLHS = getNextChar();

							temp.add(new Line("" + newCharLHS + "->"
									+ temp.get(i).sectionsOfLine.get(j).part.substring(
											temp.get(i).sectionsOfLine.get(j).part.length() - 2 - (count * 2),
											temp.get(i).sectionsOfLine.get(j).part.length() - (count * 2))));

							String sample = temp.get(i).sectionsOfLine.get(j).part.substring(
									temp.get(i).sectionsOfLine.get(j).part.length() - 2 - (count * 2),
									temp.get(i).sectionsOfLine.get(j).part.length() - (count * 2));

							temp.get(i).sectionsOfLine.set(j, new Section(""
									+ temp.get(i).sectionsOfLine.get(j).part.substring(0, math * 2 - 1) + newCharLHS));

							for (int q = 0; q < temp.size(); q++)
							{
								for (int k = 0; k < temp.get(q).sectionsOfLine.size(); k++)
								{
									if (temp.get(q).sectionsOfLine.get(k).part.length() > 2)
									{

										if (temp.get(q).sectionsOfLine.get(k).part.contains(sample))
										{
											temp.get(q).sectionsOfLine.set(k, new Section(""
													+ temp.get(q).sectionsOfLine.get(k).part.substring(0, math * 2 - 1)
													+ newCharLHS));
										}

									}
								}
							}

							updateDelimiters(temp);

							fixStrings(temp);
							updateLatestCharUsed(temp);

							count++;
						}
					}
				}
			}

			// Replace single terminals paired with non terminal with a Variable
			// Replace double terminal pairs with Variable
			for (int i = 0; i < temp.size(); i++)
			{
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					if (temp.get(i).sectionsOfLine.get(j).part.length() == 1)
					{
						if (Character.isUpperCase(temp.get(i).sectionsOfLine.get(j).characters.get(0)))
						{
							for (int h = 0; h < temp.size(); h++)
							{
								if (temp.get(h).str.charAt(0) == temp.get(i).sectionsOfLine.get(j).characters.get(0))
								{
									if (temp.get(h).sectionsOfLine.size() == 1
											&& temp.get(h).sectionsOfLine.get(0).part.length() == 1)
									{
										if (Character.isLowerCase(temp.get(h).sectionsOfLine.get(0).part.charAt(0)))
										{
											temp.get(i).sectionsOfLine.set(j,
													new Section("" + temp.get(h).sectionsOfLine.get(0).part.charAt(0)));
										}
									}
								}
							}
						}
					}
					if (temp.get(i).sectionsOfLine.get(j).part.length() == 2)
					{
						boolean upper = false;
						boolean lower = false;
						int lowerIndex = -1;
						for (int k = 0; k < temp.get(i).sectionsOfLine.get(j).characters.size(); k++)
						{
							if (Character.isUpperCase(temp.get(i).sectionsOfLine.get(j).characters.get(k)))
							{
								upper = true;

							}
							if (Character.isLowerCase(temp.get(i).sectionsOfLine.get(j).characters.get(k)))
							{
								lower = true;
								lowerIndex = k;
							}
						}
						if (upper && lower)
						{
							char newCharLHS = getNextChar();

							char sample = '0';

							temp.add(new Line("" + newCharLHS + "->"
									+ temp.get(i).sectionsOfLine.get(j).characters.get(lowerIndex)));
							sample = temp.get(i).sectionsOfLine.get(j).characters.get(lowerIndex);

							if (lowerIndex == 0)
							{

								temp.get(i).sectionsOfLine.set(j, new Section(
										"" + newCharLHS + temp.get(i).sectionsOfLine.get(j).part.substring(1)));
							} else
							{
								temp.get(i).sectionsOfLine.set(j, new Section(
										"" + temp.get(i).sectionsOfLine.get(j).part.substring(0, 1) + newCharLHS));
							}

							upper = false;
							lower = false;

							for (int m = 0; m < temp.size(); m++)
							{
								for (int n = 0; n < temp.get(m).sectionsOfLine.size(); n++)
								{
									if (temp.get(m).sectionsOfLine.get(n).part.length() == 2
											&& (temp.get(m).sectionsOfLine.get(n).characters.get(0) == sample
													|| temp.get(m).sectionsOfLine.get(n).characters.get(1) == sample))
									{

										boolean upper1 = false;
										boolean lower1 = false;
										int lowerIndex1 = -1;
										for (int k = 0; k < temp.get(m).sectionsOfLine.get(n).characters.size(); k++)
										{

											if (Character
													.isUpperCase(temp.get(m).sectionsOfLine.get(n).characters.get(k)))
											{

												upper1 = true;

											}
											if (Character
													.isLowerCase(temp.get(m).sectionsOfLine.get(n).characters.get(k)))
											{

												lower1 = true;
												lowerIndex1 = k;

											}
										}
										if (upper1 && lower1)
										{

											if (lowerIndex1 == 0)
											{

												temp.get(m).sectionsOfLine.set(n, new Section("" + newCharLHS
														+ temp.get(m).sectionsOfLine.get(n).part.substring(1)));
											} else
											{
												temp.get(m).sectionsOfLine.set(n,
														new Section(""
																+ temp.get(m).sectionsOfLine.get(n).part.substring(0, 1)
																+ newCharLHS));
											}

										}

									}
									upper = false;
									lower = false;
								}
							}

							updateDelimiters(temp);

							fixStrings(temp);
							updateLatestCharUsed(temp);

						}

						int found0m = -1;
						int found1m = -1;
						if (!upper && lower)
						{
							for (int m = 0; m < temp.size(); m++)
							{
								for (int n = 0; n < temp.get(m).sectionsOfLine.size(); n++)
								{
									if (temp.get(m).sectionsOfLine.get(n).part.length() == 1
											&& temp.get(m).sectionsOfLine.size() == 1)
									{
										if (temp.get(m).sectionsOfLine.get(n).characters
												.get(0) == temp.get(i).sectionsOfLine.get(j).characters.get(0))
										{
											found0m = m;
										}
										if (temp.get(m).sectionsOfLine.get(n).characters
												.get(0) == temp.get(i).sectionsOfLine.get(j).characters.get(1))
										{
											found1m = m;
										}

									}
								}
							}

							if (found0m != -1)
							{

								temp.get(i).sectionsOfLine.set(j, new Section("" + temp.get(found0m).str.charAt(0)
										+ temp.get(i).sectionsOfLine.get(j).part.substring(1)));

							} else
							{
								char newChar = getNextChar();
								temp.get(i).sectionsOfLine.set(j, new Section(
										"" + newChar + temp.get(i).sectionsOfLine.get(j).part.substring(1)));

								fixStrings(temp);
								updateLatestCharUsed(temp);

								updateDelimiters(temp);

								fixStrings(temp);
								updateLatestCharUsed(temp);

							}
							if (found1m != -1)
							{

								temp.get(i).sectionsOfLine.set(j,
										new Section("" + temp.get(i).sectionsOfLine.get(j).part.substring(0, 1)
												+ temp.get(found1m).str.charAt(0)));
							} else
							{
								char newChar = getNextChar();
								temp.get(i).sectionsOfLine.set(j, new Section(
										"" + temp.get(i).sectionsOfLine.get(j).part.substring(0, 1) + newChar));

								updateDelimiters(temp);

								fixStrings(temp);
								updateLatestCharUsed(temp);

							}
						}
					}
				}
			}
			// Check for single Variables and replace it if alternative exist
			for (int i = 0; i < temp.size(); i++)
			{
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					if (temp.get(i).sectionsOfLine.get(j).part.length() == 1)
					{
						if (Character.isUpperCase(temp.get(i).sectionsOfLine.get(j).characters.get(0)))
						{
							for (int h = 0; h < temp.size(); h++)
							{
								if (temp.get(h).str.charAt(0) == temp.get(i).sectionsOfLine.get(j).characters.get(0))
								{
									if (temp.get(h).sectionsOfLine.size() == 1
											&& temp.get(h).sectionsOfLine.get(0).part.length() == 2)
									{
										if (Character.isUpperCase(temp.get(h).sectionsOfLine.get(0).part.charAt(0))
												&& Character
														.isUpperCase(temp.get(h).sectionsOfLine.get(0).part.charAt(1)))
										{
											temp.get(i).sectionsOfLine.set(j,
													new Section("" + temp.get(h).sectionsOfLine.get(0).part.charAt(0)
															+ temp.get(h).sectionsOfLine.get(0).part.charAt(1)));
										}
									}
								}
							}
						}
					}
				}
			}

			removeDuplicates(temp);

			fixStrings(temp);
			// Make start new Start Variable
			String start = "" + "S0->";
			for (int j = 0; j < temp.get(0).sectionsOfLine.size(); j++)
			{
				if (temp.get(0).sectionsOfLine.size() == 1)
				{
					start = start + temp.get(0).sectionsOfLine.get(j).part;

				} else if (temp.get(0).sectionsOfLine.size() > 1 && j == temp.get(0).sectionsOfLine.size() - 1)
				{
					start = start + temp.get(0).sectionsOfLine.get(j).part;

				} else if (temp.get(0).sectionsOfLine.size() > 1)
				{
					start = start + temp.get(0).sectionsOfLine.get(j).part + "|";

				}
				if (original.get(0).contains("\\") && j == temp.get(0).sectionsOfLine.size() - 1)
				{
					start = start + "|\\";
				}
			}

			CNF.add(start);
			// add to final
			for (int i = 0; i < temp.size(); i++)
			{
				String st = "" + temp.get(i).str.substring(0, temp.get(i).str.indexOf('>') + 1);
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					if (temp.get(i).sectionsOfLine.size() == 1)
					{
						st = st + temp.get(i).sectionsOfLine.get(j).part;
					} else if (temp.get(i).sectionsOfLine.size() > 1 && j == temp.get(i).sectionsOfLine.size() - 1)
					{
						st = st + temp.get(i).sectionsOfLine.get(j).part;
					} else if (temp.get(i).sectionsOfLine.size() > 1)
					{
						st = st + temp.get(i).sectionsOfLine.get(j).part + "|";
					}
				}
				CNF.add(st);
			}

			return CNF;
		}// Convert

		/*
		 * getNextChar
		 * Find the next free Variable for the Left Hand Side
		 * return Next available char in alphabet
		 */
		public char getNextChar()
		{
			// LOCAL VARIABLES//
			char nextChar = 'A';
			int count = 0;

			while (count < Alphabet.length)
			{
				if (currectChar.contains(nextChar))
				{
					nextChar = Alphabet[count];
					count++;
				} else
				{
					return nextChar;
				}
			}

			return '0';
		}// getNextChar

		/*
		 * check Epsilon
		 */
		public boolean checkForEpsilon(ArrayList<Line> temp)
		{
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					int tempIndex = temp.get(i).sectionsOfLine.get(j).characters.indexOf('\\');

					if (tempIndex != -1)
					{
						return true;
					}
				}
			}
			return false;
		}// checkForEpsilon

		/*
		 * fixStrings
		 * Run through the Sections of each line and update the string variable
		 * of each line based on the Sections
		 */
		public void fixStrings(ArrayList<Line> temp)
		{
			for (int i = 0; i < temp.size(); i++)
			{
				String st = "" + temp.get(i).str.substring(0, temp.get(i).str.indexOf('>') + 1);
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					if (temp.get(i).sectionsOfLine.size() == 1)
					{
						st = st + temp.get(i).sectionsOfLine.get(j).part;
					} else if (temp.get(i).sectionsOfLine.size() > 1 && j == temp.get(i).sectionsOfLine.size() - 1)
					{
						st = st + temp.get(i).sectionsOfLine.get(j).part;
					} else if (temp.get(i).sectionsOfLine.size() > 1)
					{
						st = st + temp.get(i).sectionsOfLine.get(j).part + "|";
					}
				}
				temp.get(i).str = st;
			}
		}// fixStrings

		/*
		 * updateLatestCharUsed
		 * Run through Lines and see which Variables are used
		 * @param temp arrayList of Lines
		 */
		public void updateLatestCharUsed(ArrayList<Line> temp)
		{
			for (int b = 0; b < temp.size(); b++)
			{
				for (int l = 0; l < temp.get(b).str.length(); l++)
				{
					if (Character.isUpperCase(temp.get(b).str.charAt(l)))
					{
						if (!currectChar.contains(temp.get(b).str.charAt(l)))
						{
							currectChar.add(temp.get(b).str.charAt(l));
						}

					}
				}
			}
		}// updateLatestCharUsed

		/*
		 * updateDelimiters
		 * update the delimiters list for the new lines creates for new chars on
		 * the LHS
		 * @param temp arrayList of Lines
		 */
		public void updateDelimiters(ArrayList<Line> temp)
		{
			int begin = temp.get(temp.size() - 1).str.indexOf('>') + 1;

			if (temp.get(temp.size() - 1).indexesOfDelimiters.size() == 0)
			{
				temp.get(temp.size() - 1).sectionsOfLine
						.add(new Section(temp.get(temp.size() - 1).str.substring(begin)));
			} else if (temp.get(temp.size() - 1).indexesOfDelimiters.size() == 1)
			{
				temp.get(temp.size() - 1).sectionsOfLine.add(new Section(temp.get(temp.size() - 1).str.substring(begin,
						temp.get(temp.size() - 1).indexesOfDelimiters.get(0))));
				temp.get(temp.size() - 1).sectionsOfLine.add(new Section(temp.get(temp.size() - 1).str
						.substring(temp.get(temp.size() - 1).indexesOfDelimiters.get(0) + 1)));
			} else if (temp.get(temp.size() - 1).indexesOfDelimiters.size() > 1)
			{
				temp.get(temp.size() - 1).sectionsOfLine.add(new Section(temp.get(temp.size() - 1).str.substring(begin,
						temp.get(temp.size() - 1).indexesOfDelimiters.get(0))));
				temp.get(temp.size() - 1).sectionsOfLine.add(new Section(temp.get(temp.size() - 1).str
						.substring(temp.get(temp.size() - 1).indexesOfDelimiters.get(0) + 1)));
				for (int k = 1; k < temp.get(temp.size() - 1).indexesOfDelimiters.size(); k++)
				{
					temp.get(temp.size() - 1).sectionsOfLine.add(new Section(temp.get(temp.size() - 1).str.substring(
							temp.get(temp.size() - 1).indexesOfDelimiters.get(k - 1) + 1,
							temp.get(temp.size() - 1).indexesOfDelimiters.get(k))));
				}
			}
		}// updateDelimiters

		/*
		 * removeDuplicates
		 * Remove duplicate Sections from temp arrayList Lines
		 * @param temp arraylist of Lines
		 */
		public void removeDuplicates(ArrayList<Line> temp)
		{
			// Remove duplicates
			for (int i = 0; i < temp.size(); i++)
			{
				for (int j = 0; j < temp.get(i).sectionsOfLine.size(); j++)
				{
					String sti = temp.get(i).sectionsOfLine.get(j).part;

					for (int k = j + 1; k < temp.get(i).sectionsOfLine.size(); k++)
					{
						if (sti.equals(temp.get(i).sectionsOfLine.get(k).part))
						{
							temp.get(i).sectionsOfLine.remove(k);
						}
					}
				}
				if (temp.get(i).sectionsOfLine.size() > 1)
				{
					if (temp.get(i).sectionsOfLine.get(temp.get(i).sectionsOfLine.size() - 1).part
							.equals(temp.get(i).sectionsOfLine.get(temp.get(i).sectionsOfLine.size() - 2).part))
					{
						temp.get(i).sectionsOfLine.remove(temp.get(i).sectionsOfLine.size() - 1);
					}
				}
			}

		}// removeDeplicates

        private void initComponents() {
            throw new UnsupportedOperationException("aaaaaaaaaaa"); //To change body of generated methods, choose Tools | Templates.
        }

	}


	public static class Line
	{
		public String str;
		ArrayList<Integer> indexesOfDelimiters;
		ArrayList<Section> sectionsOfLine;
		public Line(String str)
		{
			this.str = str;
			this.indexesOfDelimiters = getIndexOf('|');
			this.sectionsOfLine = new ArrayList<Section>();
		}
		public ArrayList<Integer> getIndexOf(char s)
		{
			ArrayList<Integer> indexList = new ArrayList<Integer>();

			int index = str.indexOf(s);
			while (index >= 0)
			{
				indexList.add(index);

				index = str.indexOf(s, index + 1);
			} // while

			return indexList;

		}

	}
	public static class Section
	{
		ArrayList<Character> characters;
		String part;

		public Section(String str)
		{
			this.part = str;
			this.characters = getChars();
		}
		public ArrayList<Character> getChars()
		{
			ArrayList<Character> charList = new ArrayList<Character>();

			if (part.length() <= 0)
			{
				return null;
			}
			for (int j = 0; j < part.length(); j++)
			{
				charList.add(part.charAt(j));
			}

			return charList;

		}

	}


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField1 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jButton1.setText("Convert");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jScrollPane2.setViewportView(jTextArea2);

        jLabel1.setText("Input:");

        jTextArea3.setEditable(false);
        jTextArea3.setColumns(20);
        jTextArea3.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextArea3.setRows(5);
        jTextArea3.setText("Input in the form of:\nS->ASA|aB\nA->B|S\nB->b|\\\\\n \\\\ = epsilon");
        jScrollPane3.setViewportView(jTextArea3);

        jLabel2.setText("Output:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel3.setText("CFG to CNF");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(jTable1);

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton2.setText("Check");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Back");
        jButton3.setToolTipText("");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButton1)
                                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)))
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(493, 493, 493)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jButton2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(169, 169, 169)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jLabel4))
                .addGap(23, 23, 23))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    

    public ArrayList<String> getAllCombinations(String[] a, String[] b){
        ArrayList<String> output = new ArrayList<>();  

        for(String i : a) {
            for(String j : b) {
                if (!(output.contains(i+j)))
                    output.add(i+j);
            }
        }
        return output;
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
                
                jTextArea2.setText("");
                CNFtoCYK.Converter aConverter = new CNFtoCYK.Converter();


		ArrayList<String> finalCNF =  new ArrayList<String>();

                ArrayList<String> orig = new ArrayList<>(Arrays.asList(jTextArea1.getText().split("\\n")));
                
                if("".equals(orig.get(0)))
                {
                      jTextArea2.setText("Input is empty."+"\n"); 
                }
                else
                {
             		finalCNF = aConverter.convert(orig);

                for(int i = 0; i < finalCNF.size() ; i++)
                {
                    jTextArea2.setText(jTextArea2.getText() + finalCNF.get(i) + "\n"); 
                }       
                }


//		
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:

        
        String RG = jTextArea2.getText();
        String Language = jTextField1.getText();
        String[] Lines = RG.split("\\n");
 
        
        Object[] objs = new Object[Language.length()];
        for (int i = 0; i < Language.length() ; i = i + 1) {
            objs[i] = Language.charAt(i);
        }
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            objs
        ));
        
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();        

        Object[] temp = new Object[Language.length()];
        for (int i = 0; i < Language.length() ; i = i + 1) {  
            temp[i] = "";
            for(int j = 0; j < Lines.length ; j = j + 1) {
                
                if (Lines[j].contains(""+Language.charAt(i))){
                    //temp[i] = Lines[j].split("\\-")[0] + "," ;
                    temp[i] = temp[i].toString() + Lines[j].split("\\-")[0] + "," ;
                }
            }
            temp[i] = temp[i].toString().substring(0, temp[i].toString().length() - 1); // Remove last comma hahaha
        }
        model.addRow(temp);
        
        for (int i = 1; i < Language.length() ; i = i + 1) {
            Object[] nexttemp = new Object[Language.length() - i];
            for (int j = 0; j < Language.length() - i ; j = j + 1){
                for (int k = i + j; k > j ; k = k - 1){
                    String a = model.getValueAt(k-j-1, j).toString();
                    String b = model.getValueAt(i+j-k, k).toString();
                    if (!"".equals(a) && !"".equals(b)) {
                        String[] aaa = a.split("\\,");
                        String[] bbb = b.split("\\,");
                        for(String comparetwo:getAllCombinations(aaa, bbb)){
                            for (int l = 0; l < Lines.length ; l = l + 1){
                                if (Lines[l].contains(comparetwo)){
                                    if (nexttemp[j] == null)
                                        nexttemp[j] = "";
                                    String[] removeDupeArr = nexttemp[j].toString().split("\\,");
                                    if (!(Arrays.asList(removeDupeArr).contains(Lines[l].split("\\-")[0])))
                                        nexttemp[j] = nexttemp[j].toString() + Lines[l].split("\\-")[0] + ",";
                                    
                                }
                            }   
                        }
                    } 
                }
            }
            for (int m = 0; m < nexttemp.length; m++){
                if (nexttemp[m] == null){
                    nexttemp[m] = "";
                }
                else 
                    nexttemp[m] = nexttemp[m].toString().substring(0, nexttemp[m].toString().length() - 1); // Remove last comma hahaha
            }
            model.addRow(nexttemp);
        }
        System.out.println(model.getValueAt(Language.length()-1, 0).toString().contains(Lines[0].split("\\-")[0]));
        if (model.getValueAt(Language.length()-1, 0).toString().contains(Lines[0].split("\\-")[0])){ // if last row contains S
            jLabel4.setText("String is accepted by CNF");
        }
        else {
            jLabel4.setText("Not accepted by CNF");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        MainMenu s = new MainMenu();
        s.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CNFtoCYK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CNFtoCYK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CNFtoCYK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CNFtoCYK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CNFtoCYK().setVisible(true);
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
