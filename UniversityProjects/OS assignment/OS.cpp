#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

	vector<int> processList; // gantz cart processes generated
	vector<int> criticaltime; // the time displayed below the gantz chart
	
	
	
void drawLine(int size){ // just draw lines based on the vector size
	
	for (int i = 0; i < size; i++){
		cout << "+---";
	}
	cout << "+" << endl;
}

string D(int i) { 
	// convert to be in table form


	stringstream ss;
	ss << i;
	string str = ss.str();
	return "P" + str + "|" ;
}

void putDataintoTableFCFS(vector<int> arrivalTimes,vector<int> burstTimes,vector<int> priority,vector<int>& processList,vector<int>& criticaltime) {
	
	// FCFS priority pre-empt algorithm
	// processList and criticalTime in parameters are to return the values generated from this function
	
	



	
	
	int totalTime = 0;
	
   vector<int>::iterator v = burstTimes.begin(); // add all burst times to generate time all processes is completed
   while( v != burstTimes.end()) {
      totalTime += *v;
      v++;
   }	
	
	// initialise first start values , then check if there is a need to pre-empt
	


	vector<int> processQueue;


	int currentProcess = 0;
	vector<int> remainingTimes(burstTimes);
	processList.push_back(currentProcess);
	criticaltime.push_back(arrivalTimes[0]); // assuming first arrival time is lowest 
	
	
	for (int i = 1; i < arrivalTimes.size(); i++) {
		if (arrivalTimes[i] == arrivalTimes[0])
			processQueue.push_back(i);
	}	

	
	for (int i = arrivalTimes[min_element(arrivalTimes.begin(), arrivalTimes.end()) - arrivalTimes.begin()]  + 1 /*start with smallest of arrivaltime*/ ; i< totalTime + arrivalTimes[0] ; i++){ // loop through all possible time for processes to run
		
		remainingTimes[currentProcess] -- ;

		
		
		
		
		for (int j=0; j < arrivalTimes.size() ;j++){
			
			//if (arrivalTimes[j] != arrivalTimes[currentProcess]) {
			if (i == arrivalTimes[j] ){ // looking for next arrivalTime
				if (arrivalTimes[j] != arrivalTimes[currentProcess]) {
					if (priority[currentProcess] < priority[j] & remainingTimes[currentProcess] != 0 ){ // compare priority with last processs , pre-empt if new process priority is higher , old process is put into queue
						
						processQueue.push_back(currentProcess); // pre-empt old process
						currentProcess = j; // run new process
						
						//  record criticaltime and pre-empted process
						criticaltime.push_back(i); 
						processList.push_back(currentProcess);
					}
					else {
						// continue with current process
						processQueue.push_back(j);
					}	
				}
				else {
					processQueue.push_back(j);
				}
			}
			
			// if (arrivalTimes[j] == arrivalTimes[currentProcess]){
				// processQueue.push_back(j);
			// }
		}
		
		if (remainingTimes[currentProcess] == 0){
			
			// when the current process is completely finished running, take process from the earliest process in queue.
			currentProcess = processQueue[0];
			
			// record criticaltime and completed process 
			criticaltime.push_back(i);
			processList.push_back(currentProcess);
			
			// delete earliest queue entry
			processQueue.erase(processQueue.begin());
		}		

		// cout << "Current Time : " << i << endl;
		// cout << "Current Process : " << currentProcess + 1<< endl;
		// cout << "remainingTime : " << remainingTimes[currentProcess] << endl;
		// cout << "Items in queue: ";
		// for (int j=0; j < processQueue.size() ;j++){
			// cout << processQueue[j] + 1  << " ";
		// }
		// cout << endl<< endl;	



	}
	
	criticaltime.push_back(totalTime + arrivalTimes[0]); // just to add the completed time 
	
	
	
	
}

void putDataintoTableRR(vector<int> arrivalTimes,vector<int> burstTimes,int quantum,vector<int>& processList,vector<int>& criticaltime) {
	



	int originalquantum = quantum; // just to return the quantum to original value
	
	int totalTime = 0;
	
   vector<int>::iterator v = burstTimes.begin(); // add all burst times to generate time all processes is completed
   while( v != burstTimes.end()) {
      totalTime += *v;
      v++;
   }	
	
	// initialise first start values , then check if there is a need to pre-empt
	


	vector<int> processQueue;


	int currentProcess = 0;
	vector<int> remainingTimes(burstTimes);
	processList.push_back(currentProcess);
	criticaltime.push_back(arrivalTimes[0]);
	
	for (int i = 1; i < arrivalTimes.size(); i++) {
		if (arrivalTimes[i] == arrivalTimes[0])
			processQueue.push_back(i);
	}	

	
	for (int i = arrivalTimes[min_element(arrivalTimes.begin(), arrivalTimes.end()) - arrivalTimes.begin()]  + 1 /*start with smallest of arrivaltime*/ ; i< totalTime + arrivalTimes[0]  ; i++){
		
		remainingTimes[currentProcess] -- ;
		quantum--;
		
		
		
		
		for (int j=0; j < arrivalTimes.size() ;j++){

			
			
			if (i == arrivalTimes[j] && remainingTimes[currentProcess] != 0  ){ // looking for next arrivalTime
				
				 // for special condition when new arrival and remainingTime == 0
					processQueue.insert(processQueue.begin(),j);
					//processQueue.push_back(currentProcess);
					
				
				
				//currentProcess = j;
				//criticaltime.push_back(i);
				//processList.push_back(currentProcess);
				//quantum = originalquantum;
			} // todo: detect remaining time, sync it
			
			else if (i == arrivalTimes[j] && remainingTimes[currentProcess] == 0) { // edge case
				processQueue.erase(remove(processQueue.begin(), processQueue.end(), currentProcess), processQueue.end());
				currentProcess = j;
				criticaltime.push_back(i);
				processList.push_back(currentProcess);
				quantum = originalquantum;				
			}
			
		}
		
		if (remainingTimes[currentProcess] == 0 ){
			currentProcess = processQueue[0];
			criticaltime.push_back(i);
			processList.push_back(currentProcess);
			processQueue.erase(processQueue.begin());
			quantum = originalquantum;
		}		

		if (quantum == 0 && remainingTimes[currentProcess] != 0){
			processQueue.push_back(currentProcess);
			
			currentProcess = processQueue[0];
			processQueue.erase(processQueue.begin());
			criticaltime.push_back(i);
			processList.push_back(currentProcess);
			//processQueue.erase(processQueue.end());
			quantum = originalquantum;
		}
		// cout << "Current Time : " << i << endl;
		// cout << "Current Process : " << currentProcess + 1<< endl;
		// cout << "remainingTime : " << remainingTimes[currentProcess] << endl;
		// cout << "Quantum : " << quantum << endl ;
		// cout << "Items in queue: ";
		// for (int k=0; k < processQueue.size() ;k++){
			// cout << processQueue[k] + 1  << " ";
		// }
		// cout << endl<< endl;		

	}
	
	criticaltime.push_back(totalTime + arrivalTimes[0]);

	
	
}	

void putDataintoTableSJF(vector<int> arrivalTimes,vector<int> burstTimes,vector<int>& processList,vector<int>& criticaltime) {
	
	// vector<int> arrivalTimes = {1,3,7,8};
	// vector<int> burstTimes= {3,7,2,2};

	
	
	
	int totalTime = 0;
	
   vector<int>::iterator v = burstTimes.begin();
   while( v != burstTimes.end()) {
      totalTime += *v;
      v++;
   }	
	
	// initialise first start values , then check if there is a need to pre-empt
	


	vector<int> processQueue ;
	processQueue.assign (arrivalTimes.size(),999); 


	int currentProcess = 0;
	vector<int> remainingTimes(burstTimes);
	processList.push_back(currentProcess);
	criticaltime.push_back(arrivalTimes[0]);

	//int temp = min_element(arrivalTimes.begin(), arrivalTimes.end()) ;

	for (int i = 1; i < arrivalTimes.size(); i++) {
		if (arrivalTimes[i] == arrivalTimes[0])
			processQueue[i] = remainingTimes[i];
	}		
	
	for (int i = arrivalTimes[min_element(arrivalTimes.begin(), arrivalTimes.end()) - arrivalTimes.begin()]  + 1 ; i< totalTime + arrivalTimes[0] ; i++){
		
		remainingTimes[currentProcess] -- ;

		
		
		
		
		for (int j=0; j < arrivalTimes.size() ;j++){
			if (i == arrivalTimes[j]){ // looking for next arrivalTime
				if (arrivalTimes[j] != arrivalTimes[currentProcess]) {
					if (remainingTimes[currentProcess] > remainingTimes[j] & remainingTimes[currentProcess] != 0){ // compare remainingTimes with last processs
						processQueue[currentProcess] = remainingTimes[currentProcess] ;
						currentProcess = j;
						criticaltime.push_back(i);
						processList.push_back(currentProcess);
					}
					else {
						processQueue[j] = remainingTimes[j];
					}
				}
				 else {
					 processQueue.push_back(j);
				 }				
			}
		}
		
		if (remainingTimes[currentProcess] == 0){
			
			int pos = min_element(processQueue.begin(), processQueue.end()) - processQueue.begin();
			currentProcess = pos;
			processQueue[pos] = 999;
			criticaltime.push_back(i);
			processList.push_back(currentProcess);
		}		


		// cout << "Current Time : " << i << endl;
		// cout << "Current Process : " << currentProcess + 1<< endl;
		// cout << "remainingTime : " << remainingTimes[currentProcess] << endl;
		// cout << "Items in queue: ";
		// for (int j=0; j < processQueue.size() ;j++){
			// cout << processQueue[j] + 1  << " ";
		// }
		// cout << endl<< endl;	

	}
	
	criticaltime.push_back(totalTime + arrivalTimes[0]);

	
	
}

void putDataintoTable3Level(vector<int> arrivalTimes,vector<int> burstTimes,vector<int> priority,int quantum,vector<int>& processList,vector<int>& criticaltime) {
	
	// vector<int> arrivalTimes = {1,4,8,11,13};
	// vector<int> burstTimes= {5,3,8,2,3};
	// vector<int> priority = {3,2,6,1,2};
	// int quantum = 3;
	
	int originalquantum = quantum;
	
	int totalTime = 0;
	
   vector<int>::iterator v = burstTimes.begin();
   while( v != burstTimes.end()) {
      totalTime += *v;
      v++;
   }	
   
	// vector<int> processList;
	// vector<int> criticaltime;   
	vector<int> ThreeLevelQueue1;
	vector<int> ThreeLevelQueue2;
	vector<int> ThreeLevelQueue3;


	int currentProcess = 0;
	int currentQueue ;
	vector<int> remainingTimes(burstTimes);
	processList.push_back(currentProcess);
	criticaltime.push_back(arrivalTimes[0]);
	
	if (priority[0] < 3){

		currentQueue = 1;
	}
	else if (priority[0] < 5 && priority[0] >= 3){

		currentQueue = 2;
	}
	else{

		currentQueue = 2;
	}
	
	for (int i = 1; i < arrivalTimes.size(); i++) {
		if (arrivalTimes[i] == arrivalTimes[0]){
			if (priority[0] < 3){
				ThreeLevelQueue1.push_back(i);
			}
			else if (priority[0] < 5 && priority[0] >= 3){

				ThreeLevelQueue2.push_back(i);
			}
			else{

				ThreeLevelQueue3.push_back(i);
			}			
		}
	}		
	
	for (int i = arrivalTimes[min_element(arrivalTimes.begin(), arrivalTimes.end()) - arrivalTimes.begin()]  + 1 /*start with smallest of arrivaltime*/ ; i< totalTime + arrivalTimes[0] ; i++){
		if (currentQueue == 1) {

			remainingTimes[currentProcess] -- ;
			quantum--;
			
			
			
			
			for (int j=0; j < arrivalTimes.size() ;j++){
				if (i == arrivalTimes[j] && remainingTimes[currentProcess] == 0 && priority[j] < 3) { // for special condition when arrivalTime == 0 and remainingTime == 0
					ThreeLevelQueue1.erase(remove(ThreeLevelQueue1.begin(), ThreeLevelQueue1.end(), currentProcess),ThreeLevelQueue1.end());
				}
				
				


				if (i == arrivalTimes[j]){ // looking for next arrivalTime
					if (priority[j] < 3){ // compare priority with last processs
						if (remainingTimes[currentProcess] != 0 ) // for special condition when arrivalTime == 0 and remainingTime == 0
							ThreeLevelQueue1.insert(ThreeLevelQueue1.begin(),j);
							//ThreeLevelQueue1.push_back(currentProcess);
							
						else if (remainingTimes[currentProcess] == 0)	{
							
							currentProcess = j;
							criticaltime.push_back(i);
							processList.push_back(currentProcess);
							quantum = originalquantum;
						}

					}
					else if (priority[j] < 5 && priority[j] >= 3){ // check priority
						ThreeLevelQueue2.push_back(j);

					}
					
					else {

						ThreeLevelQueue3.push_back(j);
						
					}
					
				}


				
			}
			
		
			
			if (remainingTimes[currentProcess] == 0 ){
				if (ThreeLevelQueue1.size() > 0){
					currentProcess = ThreeLevelQueue1[0];
					criticaltime.push_back(i);
					processList.push_back(currentProcess);
					ThreeLevelQueue1.erase(ThreeLevelQueue1.begin());
					quantum = originalquantum;					
					
				}
				else{
					currentQueue = 2;
					currentProcess = ThreeLevelQueue2[0];
					criticaltime.push_back(i);
					processList.push_back(currentProcess);
					ThreeLevelQueue2.erase(ThreeLevelQueue2.begin());						
				}

			}		

			if (quantum == 0 && remainingTimes[currentProcess] != 0 && currentQueue == 1){
				ThreeLevelQueue1.push_back(currentProcess);
				
				currentProcess = ThreeLevelQueue1[0];
				ThreeLevelQueue1.erase(ThreeLevelQueue1.begin());
				criticaltime.push_back(i);
				processList.push_back(currentProcess);
				
				quantum = originalquantum;
			}
			// cout << "Current Time : " << i << endl;
			// cout << "Current Process : " << currentProcess + 1<< endl;
			// cout << "Current Queue : " << currentQueue << endl;
			// cout << "remainingTime : " << remainingTimes[currentProcess] << endl;
			// cout << "Quantum : " << quantum << endl ;
			// cout << "Items in queue: ";
			// for (int j=0; j < ThreeLevelQueue1.size() ;j++){
				// cout << ThreeLevelQueue1[j] + 1  << " ";
			// }
			// cout << endl<< endl;		

			
			
		}
		
		else if (currentQueue == 2) {
			
				
				remainingTimes[currentProcess] -- ;

				
				
				
				
				for (int j=0; j < arrivalTimes.size() ;j++){
					if (i == arrivalTimes[j]){ // looking for next arrivalTime
						if (priority[j] < 3){ // compare priority with last processs
							ThreeLevelQueue2.push_back(currentProcess);
							currentQueue = 1;
							currentProcess = j;
							criticaltime.push_back(i);
							processList.push_back(currentProcess);
							quantum = originalquantum;
							
							
							
						}
						else if (priority[j] < 5 && priority[j] >= 3){ // check priority
							if (priority[currentProcess] < priority[j] & remainingTimes[currentProcess] != 0 ){ // compare priority with last processs
								ThreeLevelQueue2.push_back(currentProcess);
								currentQueue = 2;
								currentProcess = j;
								criticaltime.push_back(i);
								processList.push_back(currentProcess);
							}
							else {
								ThreeLevelQueue2.push_back(j);
							}

						}
						
						else {

							ThreeLevelQueue3.push_back(j);
							
						}
						
					}
				}
				
				if (remainingTimes[currentProcess] == 0){
					if (ThreeLevelQueue2.size() > 0) { 
						currentProcess = ThreeLevelQueue2[0];
						criticaltime.push_back(i);
						processList.push_back(currentProcess);
						ThreeLevelQueue2.erase(ThreeLevelQueue2.begin());						
						
						
					}
					else{ // detect when Q3 move to Q2
						currentQueue = 3;
						currentProcess = ThreeLevelQueue3[0];
						criticaltime.push_back(i);
						processList.push_back(currentProcess);
						ThreeLevelQueue3.erase(ThreeLevelQueue3.begin());							
					}
					

				}		

			// cout << "Current Time : " << i << endl;
			// cout << "Current Process : " << currentProcess + 1<< endl;
			// cout << "Current Queue : " << currentQueue << endl;
			// cout << "remainingTime : " << remainingTimes[currentProcess] << endl;
			// cout << "Items in queue: ";
			
			// for (int j=0; j < ThreeLevelQueue2.size() ;j++){
				// cout << ThreeLevelQueue2[j] + 1  << " ";
			// }
			// cout << endl<< endl;


			
			
			
		}
		
		else if (currentQueue == 3) {
			currentQueue = 2;
			ThreeLevelQueue2 = ThreeLevelQueue3;
			ThreeLevelQueue3.clear();			
			
			
		}
		
	
		
		
		
	}
			criticaltime.push_back(totalTime + arrivalTimes[0]);
	

	
}
	
void drawTable(){  // draw Table
	
	drawLine(processList.size());
	
	cout << '|' ;
	
	for (int i = 0; i < processList.size(); i++){
		cout.width(4);
		cout << D(processList[i] + 1);	
	}
	cout << endl;
	drawLine(processList.size());
	
	for (int i = 0; i < criticaltime.size(); i++){
		if (criticaltime[i] > 9)
			cout << criticaltime[i] << "  " ;	
		
		else
			cout << criticaltime[i] << "   " ;	
	}	


}

void calculateWaitingAndTurnaroundTime(vector<int> arrivalTimes,vector<int> burstTimes,vector<int> processList,vector<int> criticaltime,int size){
	vector<int> CompletionTime ;
	CompletionTime.assign (size,999); 
	
	vector<int> TurnaroundTime ;
	
	vector<int> WaitingTime;
	
	for (int i = processList.size() - 1;i >= 0; i--) {
		if(CompletionTime[processList[i]] == 999)
			CompletionTime[processList[i]] = criticaltime[i + 1];
	}
	
	// cout << endl << "CompletionTimes : " ;
	// for (int j = 0; j < 5 ; j ++)
		// cout << CompletionTime[j] << " " ;
	
	for (int k = 0; k < size ; k++)
		TurnaroundTime.push_back(CompletionTime[k] - arrivalTimes[k]);
	
	cout << endl <<  "TurnaroundTimes : " ;
	for (int l = 0; l < size ; l ++)
		cout << "P" << l + 1 << ":" << TurnaroundTime[l] << " " ;

	cout <<  endl << "Average TurnaroundTime : ";
	
	float averageTTime ;
	
	for (int n : TurnaroundTime)
		averageTTime += n;
	
	averageTTime = accumulate(TurnaroundTime.begin(), TurnaroundTime.end(), 0.0f);
	
	cout << averageTTime / size ;
	
	for (int m = 0; m < size ; m++)
		WaitingTime.push_back(TurnaroundTime[m] - burstTimes[m]);
	
	cout << endl <<  "WaitingTime : " ;
	for (int n = 0; n < size ; n ++)
		cout << "P" << n + 1 << ":"  << WaitingTime[n] << " " ;		
	
	cout <<  endl << "Average WaitingTime : ";
	
	float averageWTime ;
	
	for (int n : WaitingTime)
		averageWTime += n;
	
	cout << averageWTime / size ;
	
	
	
}

// int main()
// {
	// vector<int> arrivalTimes ;//= {1,3,7,8,12};
	// vector<int> burstTimes ;//= {3,4,6,2,5};
	// vector<int> priority ;//= {3,2,6,1,2};
	// int quantum  ;//= 4;
	
	// cout << "WARNING! No error checking, please enter values carefully! Enter 999 if any arrivaltimes, bursttimes, priority or quantum isnt needed" << endl << endl;
	// cout << "PLEASE enter the smallest arrival time as its first value!!!" << endl;
	
	// int a;
	// cout << "Enter arrivalTimes in integer , enter 999 to stop" << endl;
	// while (a != 999) {
		// cin >> a;
		// if (a != 999)
			// arrivalTimes.push_back(a);

	// }
	
	// int b;
	// cout << "Enter burstTimes in integer , enter 999 to stop" << endl;
	// while (b != 999) {
		// cin >> b;
		// if (b != 999)
			// burstTimes.push_back(b);

	// }

	// int c;
	// cout << "Enter priority in integer , enter 999 to stop" << endl;
	// while (c != 999) {
		// cin >> c;
		// if (c != 999)
			// priority.push_back(c);

	// }


	// cout << "Enter quantum" << endl;
	// cin >> quantum;
	
	
	// int choice;
	// cout << "Enter 1 for FCFS-based pre-emptive Priority , 2 for Round Robin Scheduling, 3 for Three-level Queue Scheduling and 4 for Preemptive SJF scheduling" << endl;
	// processList.clear();
	// criticaltime.clear();
	
	// cin >> choice;
	
	// if (choice == 1)
		// putDataintoTableFCFS(arrivalTimes,burstTimes,priority,processList,criticaltime);
	// else if (choice == 2)
		// putDataintoTableRR(arrivalTimes,burstTimes,quantum,processList,criticaltime);
	// else if (choice == 3)
		// putDataintoTable3Level(arrivalTimes,burstTimes,priority,quantum,processList,criticaltime);
	// else if (choice == 4)
		// putDataintoTableSJF(arrivalTimes,burstTimes,processList,criticaltime);
	
	

	
	

	
	// drawTable();
	

	
	// calculateWaitingAndTurnaroundTime(arrivalTimes,burstTimes,processList,criticaltime,arrivalTimes.size());
	
	
	
// }

int main(){
	vector<int> arrivalTimes = {0,0,4,9,7,13};
	vector<int> burstTimes = {8,6,15,13,9,5};
	vector<int> priority = {2,1,5,4,3,1};
	int quantum  = 3;	
	
	putDataintoTableSJF(arrivalTimes,burstTimes,processList,criticaltime);
	drawTable();
	
	calculateWaitingAndTurnaroundTime(arrivalTimes,burstTimes,processList,criticaltime,arrivalTimes.size());
}

