#ifndef _City
#define _City
#include <string>
#include <iostream>
#include "Attraction.h"
#include "LinkedList.h"

using namespace std; 

class City {
private:
	int id;
	string name;
	LinkedList<int> ConnectedTo;

	
public:
	LinkedList<Attraction*> attractions;
	bool visited = false;
	City(string name, int id );
	string getName();
	void insertAttraction(Attraction *a);
	void insertConnectingCity(int a);
	int* returnConnectingCity(int &finalSize);
	LinkedList<Attraction*> getAttraction();
	int getid();
	void setid(int idToSet);
	void changeCName(string changename);
	bool operator >(City city);
};

#include "City.cpp"
#endif


