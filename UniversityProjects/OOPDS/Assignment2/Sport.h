#ifndef _Sport
#define _Sport
#include "Attraction.h"
#include <iostream>

using namespace std;

class Sport: public Attraction {
private:
	int ageLimit;
public:
	Sport(string name, int id, int typeID, int ageLimit);
	virtual void display();
	int getageLimit();
};

#include "Sport.cpp"
#endif