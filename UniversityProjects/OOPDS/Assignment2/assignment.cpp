/*************************************
Program: assignment.cpp
Course: OOPDS
Year: 2015/16 Trimester 2
Name: Bryan Ooi Tat Mun
ID: 1141328133
Lecture: TC01
Lab: TT03
Email: bryanotm@hotmail.com
Phone: 012-5866639
*************************************/


#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <fstream>
#include "City.h"
#include "Sport.h"
#include "Culture.h"
#include "Shopping.h"
#include "LinkedList.h"
#include "ArrayStack.h"
using namespace std;

LinkedList<City> cities;
int** matrix; // for flight adjacency matrix
ArrayStack<int> flight; // to display the path between indirect flights.

template <typename itemType>
void BubbleSortName(LinkedList<itemType>& List, string type = "cities"){
	int size = cities.getLength();
	for (int i = 0; i < List.getLength(); i++){
		for (int j = 0; j < List.getLength() - 1; j++){
			if (List[j] > List[j + 1]){
				City temp;
				temp = List[j];
				List.setEntry(j+1,List[j + 1]);
				List.setEntry(j+2,temp);
					if (type == "cities") { // swap flight info for cities.
						
						int* temp = NULL;
						temp = new int[size];
						
						for (int k = 0; k < size ; k++) // storing into temp
							temp[k] = matrix[j][k];
						
						for (int k = 0; k < size ; k++) // swapping rows
							matrix[j][k] = matrix[j + 1][k];
						for (int k = 0; k < size ; k++)
							matrix[j + 1][k] = temp[k];	

						for (int k = 0; k < size ; k++){ // swapping columns
							int temp2 = matrix[k][j];
							matrix[k][j] = matrix[k][j + 1] ;
							matrix[k][j+1] = temp2;							
						}
					}
			}
		}
	}
	for (int i = 0; i < List.getLength(); i++){ // to ensure the index of cities and cityid is related
		City* temp = new City;
		*temp = List[i];
		temp->setid(i + 1);
		cities.setEntry(i + 1,*temp);
		delete temp;				
	}
}

int** createMatrix(){ 
	int size = cities.getLength();
	int** matrix = new int*[size];
	for(int i = 0; i < size; ++i)
		matrix[i] = new int[size];
	
	for (int i = 0; i < size ; i++){
		for (int j = 0; j < size ; j++)
			matrix[i][j] = 0;
	}
	return matrix;
}

void enterMatrix(int cityid1,int cityid2){
	matrix[cityid1-1][cityid2-1] = 1;
}

void LoadFile(string filename){
	string line;
	ifstream myfile (filename);
	// istringstream is to get the integer value from getline.
	
	if (myfile.is_open())
	{
		int sizeOfCities;
		getline(myfile,line);
		istringstream iss(line);
		iss >> sizeOfCities;
		int sizeOfAttractions[sizeOfCities];
		int noOfSports, noOfCulture, noOfShopping;
		LinkedList<City> tempcities;
		
		
		for (int i = 0; i < sizeOfCities; i++){ // getting cities data
			getline(myfile,line);
			tempcities.insert(i+1, City(line,i+1));
		}
		
		for (int i = 0; i < sizeOfCities; i++){ 
			getline(myfile,line);
			istringstream iss(line);
			iss >> sizeOfAttractions[i];
		}
		
		
		for (int i = 0; i < sizeOfCities; i++){ // getting all the attraction data. Put it into an attraction list, then put into cities list.
			LinkedList<Attraction*> tempattractions;
			for (int j = 0; j < sizeOfAttractions[i]; j++){
				int temptypeid;
				getline(myfile,line);
				istringstream iss(line);
				iss >> temptypeid;
				
				if (temptypeid == 1){ 

					
					int tempid;
					getline(myfile,line);
					istringstream iss2(line);
					iss2 >> tempid;
					
					string tempname;
					getline(myfile,line);
					tempname = line;
					
					string tempdetail;
					getline(myfile,line);
					tempdetail = line;
					
					int tempageLimit;
					getline(myfile,line);
					istringstream iss3(line);
					iss3 >> tempageLimit;	
					
					Attraction* tempsport = new Sport(tempname, tempid, temptypeid, tempageLimit);
					tempsport->addDetail(tempdetail);
					tempattractions.insert(j+1,tempsport);

				}
				
				else if (temptypeid == 2){
					
					int tempid;
					getline(myfile,line);
					istringstream iss2(line);
					iss2 >> tempid;
					
					string tempname;
					getline(myfile,line);
					tempname = line;
					
					string tempdetail;
					getline(myfile,line);
					tempdetail = line;
					
					double tempentranceFee;
					getline(myfile,line);
					istringstream iss3(line);
					iss3 >> tempentranceFee;	
					
					Attraction* tempculture = new Culture(tempname, tempid, temptypeid, tempentranceFee);
					tempculture->addDetail(tempdetail);
					tempattractions.insert(j+1,tempculture);

				}
				
				
				else if (temptypeid  == 3){
		
					int tempid;
					getline(myfile,line);
					istringstream iss2(line);
					iss2 >> tempid;
					
					string tempname;
					getline(myfile,line);
					tempname = line;
					
					string tempdetail;
					getline(myfile,line);
					tempdetail = line;
					
					int tempmallsize;
					getline(myfile,line);
					istringstream iss3(line);
					iss3 >> tempmallsize;
					
					Shopping* tempshopping = new Shopping(tempname, tempid, temptypeid);
					tempshopping->addDetail(tempdetail);
					for (int m = 0; m < tempmallsize; m++){
						string tempmallname;
						getline(myfile,line);
						tempmallname = line;
						tempshopping->addMalls(tempmallname);	
					}
					
					tempattractions.insert(j+1,tempshopping);

				}
				City* temp = new City;
				*temp = tempcities[i];
				temp->attractions = tempattractions;
				tempcities.setEntry(i+1,*temp);
				delete temp;
			}				
		}
		int* storeOldSize = new int;
		*storeOldSize = cities.getLength();
		
		cities = tempcities;
		
		int** newMatrix;
		newMatrix = createMatrix();				
 
		for (int i = 0; i < cities.getLength() ; i++){ // getting flight data
			for (int j = 0; j < cities.getLength() ; j++){
				int tempmatrixdata;
				getline(myfile,line);
				istringstream iss2(line);
				iss2 >> tempmatrixdata;
				newMatrix[i][j] = tempmatrixdata;
			}
		}
		
		for (int i = 0; i < *storeOldSize ; i++){
			delete[] matrix[i];
		}
		delete storeOldSize;
		delete[] matrix;
		
		matrix = newMatrix;
	
		myfile.close();
	
	
	}	



}

void displayCities(){
	for (int i=0 ;  i<cities.getLength() ; i++) {
		cout << cities[i].getid() << ". " << cities[i].getName() << endl;
	}
	//cout << cities.getLength();
}

string getCityNameFromID(int id){
	for (int i = 0; i < cities.getLength(); i++){
		if (cities[i].getid() == id)
			return cities[i].getName();
	}
}

LinkedList<Attraction*> returnAttraction(int citychoice, int typechoice){ // displaying and returning attraction list
	int a; 
	LinkedList<Attraction*> attractions;
	for (int i=0; i<cities.getLength() ; i++ ) {
		if (cities[i].getid() == citychoice)
			a =  i;
	}
	for (int i=0; i<cities[a]. getAttraction().getLength(); i++) {
		if (cities[a]. getAttraction()[i] -> gettypeID() == typechoice || typechoice == 4){
			cout << "ID: " << cities[a]. getAttraction()[i]-> getid() << ", Name: " << cities[a].getAttraction()[i]-> getname() << endl;
			attractions.insert(1,cities[a]. getAttraction()[i]);
		}
	}
	return attractions;
}

bool displayAttractionInfo(int attractionchoice, LinkedList<Attraction*> attractions, bool display){
	/* Retrieves the 'display()' method from the appropriate vector attractions. 2nd purpose is to check if user input for attractionchoice is contained in the vector attractions.
	Set display to true for 1st purpose and display to false for 2nd purpose */
	bool flag = false;
	for (int i=0; i<attractions.getLength(); i++) {
		if (attractions[i]->getid() == attractionchoice){
			if (display == true)
				attractions[i]->display(); 
			flag = true;
		}
	}
	return flag;
}

// Helper functions for checking flight connections
void unvisitAll(){
	for (int i=0 ;  i<cities.getLength() ; i++) {
		City temp = cities[i];
		temp.visited = false;
		cities.setEntry(i+1,temp);
	}	
}

void markVisited(int originCity){
	for (int i=0 ;  i<cities.getLength() ; i++) {
		if (cities[i].getid() == originCity){
			City temp = cities[i];
			temp.visited = true;
			cities.setEntry(i+1,temp);
		}
	}
}

int getNextCity(int topCity,int nextCity){
	// int size; // will be the last index of the temp2 array
	// int* temp2 = cities[topCity-1].returnConnectingCity(size);
	// for (int i = 0; i < size; i++){
		// if (cities[temp2[i] - 1].visited == false)
			// return cities[temp2[i] - 1].getid();
	// }
	
	int* temp3 = matrix[topCity-1];
	int size = cities.getLength();
	for (int i = 0; i < size; i++){
		if (temp3[i] == 1) {
			if (cities[i].visited == false)
				return cities[i].getid();
		}	
	}
	return 99;
}

bool checkIfConnected(int cityid1,int cityid2){
	bool success;
	ArrayStack<int> aStack;
	unvisitAll(); // Clear marks on all cities
	// // Push origin city onto aStack and mark it as visited
	aStack.push(cityid1);
	markVisited(cityid1);
	int topCity = aStack.peek();
	while (!aStack.isEmpty() && (topCity != cityid2))
		{
		// The stack contains a directed path from the origin city
		// at the bottom of the stack to the city at the top of
		// the stack
		// Find an unvisited city adjacent to the city on the
		// top of the stack
		int nextCity = getNextCity(topCity, nextCity);
		if (nextCity == 99)
			aStack.pop(); // No city found; backtrack
		else // Visit city
			{
			aStack.push(nextCity);
			markVisited(nextCity);
		} // end if
		if (!aStack.isEmpty())
			topCity = aStack.peek();
	} // end while
	flight = aStack;
	return !aStack.isEmpty();
	
}

void adminOptions();



void MainMenu(LinkedList<City>& cities){
	/* Display a text-based main menu complete with error checking for the user inputs*/
	int citychoice, typechoice, attractionchoice;
	bool next;
	while (true) {
		next = false;
		system("cls");
		cout << endl;
		BubbleSortName(cities);// Sort city names alphabetically
		cout << "Enter a city id for more info." << endl;
		displayCities();
		cout << "Enter 96 to check which city connects others in direct flight" << endl;
		cout << "Enter 97 to ask if two cities are connected by flight" << endl;
		cout << "Enter 98 for admin options" << endl;
		cout << "Enter 99 to exit the program" << endl << endl;
		while (!(cin >> citychoice) /* error checking for non-int */&& cout << endl << endl || citychoice < 0 || (citychoice >= cities.getLength() + 1 && (citychoice < 96 || citychoice > 99))) { 
			cout << endl << "Enter a valid option" << endl << endl;
			cin.clear();
			cin.ignore();
		}
		if (citychoice <= cities.getLength() && citychoice > 0)
			next = true;
		
		if (citychoice == 96){
			cout << "Enter a city id." << endl << endl;
			int temp;
			while (!(cin >> temp) /* error checking for non-int */&& cout << endl << endl << endl || temp < 0 || (temp >= cities.getLength() + 1 )) { 
				cout << endl << "Enter a valid option" << endl << endl;
				cin.clear();
				cin.ignore();
			}
			
			int* temp3 = matrix[temp-1];
			int sizeCities = cities.getLength();
			cout << "The following cities are connected by flight: ";
			for (int i = 0; i < sizeCities; i++){
				if (temp3[i] == 1) {
					cout << cities[i].getName() << "... ";	
				}	
			}
			cout << endl << endl;
			system("pause");
		} 
		
		if (citychoice == 97){
			cout << "Enter two city ids." << endl;
			int cityid1, cityid2;
			while (!(cin >> cityid1 && cin >> cityid2) /* error checking for non-int */&& cout << endl << endl || cityid1 < 0 || cityid2 < 0 || (cityid1 >= cities.getLength() + 1) || (cityid2 >= cities.getLength() + 1) || cityid1 == cityid2) { 
				cout << endl << "Enter a valid option" << endl << endl;
				cin.clear();
				cin.ignore();
		    }
			if (matrix[cityid1 - 1][cityid2 - 1] == 1)
				cout << "There is a direct flight between these cities" << endl;
			else if (matrix[cityid1 - 1][cityid2 - 1] == 0 && checkIfConnected(cityid1, cityid2)){
				ArrayStack<int> tempflight;
				while (!flight.isEmpty()){ // reverse the order of the stack so the flight part comes out right.
					tempflight.push(flight.peek());
					flight.pop();
				}
				flight = tempflight;
				cout << "There isn't a direct flight but there is an indirect flight between these two cities." << endl;
				cout << "An example route would be ";
				while(!flight.isEmpty()) {// Displaying the indirect route
					cout << " - " << getCityNameFromID(flight.peek());
					flight.pop();
				}
				cout << endl;
			}
			else 
				cout << "There isn't a flight connecting between these cities" << endl;
			system("pause");	
			}

		if (citychoice == 98){
			cout << "Enter Password" << endl;
			string password;
			cin.ignore();
			getline(cin, password);
			if (password == "123")
				adminOptions();
			else{
				cout << "Wrong Password" << endl;
				system("pause");
			}
		}
		
		if (citychoice == 99)
			break;
		
		while (next == true) {
			system("cls");
			cout << "Choose a type of attraction" << endl;
			cout << "1. Sports" << endl << "2. Culture" << endl << "3. Shopping" << endl << "4. All attractions" << endl << "5. Go back"<< endl << endl;
			while (!(cin >> typechoice)/* error checking for non-int */ && cout << endl << endl || typechoice < 1 || (typechoice > 5)) {
				cout << endl << "Enter a valid option" << endl << endl;
				cin.clear();
				cin.ignore();
			}
			cout << endl;
			
			if(typechoice == 5)
				break;
			
			while(true) {
				system("cls");
				cout << "Type an attraction ID to find out more info" << endl;
				
				LinkedList<Attraction*> attractions;
				attractions = returnAttraction(citychoice, typechoice);	
				
				cout << "Enter 99 to go back" << endl;
				cout << endl;
				while (!(cin >> attractionchoice)/* error checking for non-int */ && cout << endl << endl || 
					(!displayAttractionInfo(attractionchoice, attractions, false)/* check if attractionchoice from user input is contained in the list of attractions */ && 
					attractionchoice != 99) ) {
					cout << endl << "Enter a valid option" << endl << endl;
					cin.clear();
					cin.ignore();
				}
				cout << endl;
				
				if (attractionchoice == 99)
					break;
				displayAttractionInfo(attractionchoice, attractions, true);
				cout << endl;
				system("pause");
				cout << endl;
			}
		}
	}
}

void adminOptions(){
	system("cls");
	bool optionmenu = true;
	bool changemenu = false;
	
	while (optionmenu == true){
		system("cls");
		int optionchoice;
		cout << "Select an option" << endl;
		cout << "1. Add " << endl;
		cout << "2. Remove" << endl;
		cout << "3. Change " << endl;
		cout << "4. Save to txt file. " << endl;
		cout << "5. Load from txt file." << endl;
		cout << "6. Go back." << endl;
		
		while (!(cin >> optionchoice)/* error checking for non-int */ && cout << endl << endl || 
			(optionchoice < 0) || (optionchoice > 6) ) {
			cout << endl << "Enter a valid option" << endl << endl;
			cin.clear();
			cin.ignore();
		}
		if (optionchoice < 4) // Go to next menu for add, change and remove
			changemenu = true;
		
		if (optionchoice == 4){ // Saving, store size of cities, cities data, size of attractions in cities, data of attractions, flight data
			string filename;
			cout << endl << "Name your file" << endl << endl;
			cin.ignore();
			getline(cin,filename);			
			
			ofstream myfile (filename);
			if (myfile.is_open())
			{
				int sizeOfCities = cities.getLength();
				int sizeOfAttractions[sizeOfCities];
				int noOfSports, noOfCulture, noOfShopping;
				myfile << sizeOfCities << endl;
				for (int i = 0; i < sizeOfCities; i++)
					myfile << cities[i].getName() << endl;
				
				for (int i = 0; i < sizeOfCities; i++){
					sizeOfAttractions[i] = cities[i].attractions.getLength();
					myfile << sizeOfAttractions[i] << endl;
				}
				
				for (int i = 0; i < sizeOfCities; i++){
					for (int j = 0; j < sizeOfAttractions[i]; j++){
						if (cities[i].attractions[j]->gettypeID() == 1){
							myfile << 1 << endl;
							myfile << cities[i].attractions[j]->getid() << endl;						
							myfile << cities[i].attractions[j]->getname() << endl;
							myfile << cities[i].attractions[j]->getDetail() << endl;
							Sport* temp = (Sport*)(cities[i].attractions[j]);
							myfile << temp->getageLimit() << endl;	
						}
						else if (cities[i].attractions[j]->gettypeID() == 2){
							myfile << 2 << endl;
							myfile << cities[i].attractions[j]->getid() << endl;						
							myfile << cities[i].attractions[j]->getname() << endl;
							myfile << cities[i].attractions[j]->getDetail() << endl;
							Culture* temp = (Culture*)(cities[i].attractions[j]);
							myfile << temp->getentranceFee() << endl;	
						}
						else if (cities[i].attractions[j]->gettypeID() == 3){
							myfile << 3 << endl;
							myfile << cities[i].attractions[j]->getid() << endl;						
							myfile << cities[i].attractions[j]->getname() << endl;
							myfile << cities[i].attractions[j]->getDetail() << endl;
							Shopping* temp = (Shopping*)(cities[i].attractions[j]);
							myfile << temp->getmalls().getLength() << endl;	
							for (int k = 0; k < temp->getmalls().getLength(); k++)
								myfile << temp->getmalls()[k] << endl;
								
						}
					}
					
				}
				int size = cities.getLength();
				
				for (int i = 0; i < size ; i++){
					for (int j = 0; j < size ; j++)
						myfile << matrix[i][j] << endl;
				}
				
				
				myfile.close();
			}
			else 
				cout << "Unable to open file";
			
			cout << "Data saved!" << endl;
			system("pause");
		}
		
		if (optionchoice == 5){ // Loading data
			string filename;
			cout << endl << "Input the name of the save file" << endl << endl;
			cin.ignore();
			getline(cin,filename);
			
			LoadFile(filename);
			cout << "Data loaded" << endl;
			system("pause");

		}
		
		if (optionchoice == 6) // exit admin menu
			break;
		
		while (changemenu == true){
			int changechoice;
			bool optionforchange = true;
			system("cls");
			cout << "Select an option you would like to add,remove or change" << endl;
			cout << "1.City Data" << endl;
			cout << "2.Attraction Data" << endl;
			cout << "3.Flight information " << endl;
			cout << "4.Go back." << endl;
			while (!(cin >> changechoice)/* error checking for non-int */ && cout << endl << endl || 
			(changechoice < 0) || (changechoice > 4) ) {
				cout << endl << "Enter a valid option" << endl << endl;
				cin.clear();
				cin.ignore();
			}
			if (optionchoice == 1 && changechoice == 1){
				system("cls");
				cout << "Enter a cityname to add." << endl;
				cout << "Enter \"back\" to go back" << endl;
				string addName;
				cin.ignore();
				getline(cin,addName);
				if (addName != "back")
					cities.insert(cities.getLength() + 1,City(addName,cities.getLength() + 1));
				// change flight data
				int** newMatrix;
				newMatrix = createMatrix();
				for (int i = 0; i < cities.getLength() - 1 ; i++){
					for (int j = 0; j < cities.getLength() - 1; j++)
						newMatrix[i][j] = matrix[i][j];
				}
				
				// clean up for old matrix data
				for (int i = 0; i < cities.getLength() ; i++){
					delete[] matrix[i];
				}
				delete[] matrix;
				
				matrix = newMatrix;
				
				// for (int i = 0; i < cities.getLength() ; i++){
					// for (int j = 0; j < cities.getLength() ; j++)
						// cout <<  matrix[i][j];
					// cout << endl;
				// }
				if (addName != "back") {
					cout << "Data added!" << endl;
					system("Pause");
				}
			}
			
			if (optionchoice == 1 && changechoice == 2){ // adding attractions into cities
				while (optionforchange == true) {
					int citychoice, typechoice, attractionchoice;
					bool next;
					next = false;
					system("cls");
					cout << endl;
					cout << "Enter a city id to add the attraction into." << endl;
					displayCities();
					cout << "Enter 99 to go back" << endl << endl;
					while (!(cin >> citychoice) /* error checking for non-int */&& cout << endl << endl || citychoice < 0 || (citychoice >= cities.getLength() + 1 && (citychoice != 99))) { 
						cout << endl << "Enter a valid option" << endl << endl;
						cin.clear();
						cin.ignore();
					}
					if (citychoice <= cities.getLength() && citychoice > 0)
						next = true;
					
					if (citychoice == 99)
						break;
					
					while (next == true) {
						system("cls");
						cout << "Choose a type of attraction to add" << endl;
						cout << "1. Sports" << endl << "2. Culture" << endl << "3. Shopping" << endl << "4. Go back"<< endl << endl;
						while (!(cin >> typechoice)/* error checking for non-int */ && cout << endl << endl || typechoice < 1 || (typechoice > 4)) {
							cout << endl << "Enter a valid option" << endl << endl;
							cin.clear();
							cin.ignore();
						}
						cout << endl;
						
						if (typechoice == 1){
							cout << "Enter a sport attraction name." << endl;
							string addName,details;
							int ageLimit;
							cin.ignore();
							getline(cin,addName);
							cout << "Enter the ageLimit for this attraction." << endl;
							while (!(cin >> ageLimit) && cout << endl << endl || ageLimit < 0 || (ageLimit > 122)) {
								cout << endl << "Enter a valid option" << endl << endl;
								cin.clear();
								cin.ignore();
							}
							cout << "Enter the details for this attraction" << endl;
							cin.ignore();
							getline(cin,details);							
							if (addName != ""){
								Attraction* newsport = new Sport(addName, cities[citychoice - 1].attractions.getLength() + 1, typechoice, ageLimit);
								newsport->addDetail(details);
								City* temp = new City;
								*temp = cities[citychoice - 1];
								temp->attractions.insert(temp->attractions.getLength() + 1,newsport);
								cities.setEntry(citychoice,*temp);
								delete temp;
								cout << "Data entered!" << endl;
								system("pause");
								optionforchange = false;
								next = false;
							}					
						}
						
						if (typechoice == 2){
							cout << "Enter a culture attraction name." << endl;
							string addName,details;
							double entrancefee;
							cin.ignore();
							getline(cin,addName);
							cout << "Enter the entrance fee for this attraction." << endl;
							while (!(cin >> entrancefee) && cout << endl << endl ) {
								cout << endl << "Enter a valid option" << endl << endl;
								cin.clear();
								cin.ignore();
							}
							cout << "Enter the details for this attraction" << endl;
							cin.ignore();
							getline(cin,details);							
							if (addName != ""){
								Attraction* newCulture = new Culture(addName, cities[citychoice - 1].attractions.getLength() + 1, typechoice, entrancefee);
								newCulture->addDetail(details);
								City* temp = new City;
								*temp = cities[citychoice - 1];
								temp->attractions.insert(temp->attractions.getLength() + 1,newCulture);
								cities.setEntry(citychoice,*temp);
								delete temp;
								cout << "Data entered!" << endl;
								system("pause");
								optionforchange = false;
								next = false;
							}					
						}

						if (typechoice == 3){
							cout << "Enter a shopping type attraction name." << endl;
							string addName,details;
							cin.ignore();
							getline(cin,addName);
							cout << "Enter the details for this attraction" << endl;
							getline(cin,details);							
							if (addName != ""){
								Shopping* newShopping = new Shopping(addName, cities[citychoice - 1].attractions.getLength() + 1, typechoice);
								newShopping->addDetail(details);
								cout << "Enter the name of the malls in the area. Enter \"back\" to stop adding" << endl;
								string nameOfMall;
								getline(cin,nameOfMall);
								if (nameOfMall != "back")
									cout << endl << "Enter Next Mall:" << endl;
								while (nameOfMall != "back"){
									newShopping->addMalls(nameOfMall);									
									getline(cin,nameOfMall);
									if (nameOfMall != "back")
										cout << endl << "Enter Next Mall:" << endl;
								}
								City* temp = new City;
								*temp = cities[citychoice - 1];
								temp->attractions.insert(temp->attractions.getLength() + 1,newShopping);
								cities.setEntry(citychoice,*temp);
								delete temp;
								cout << "Data entered!" << endl;
								system("pause");
								optionforchange = false;
								next = false;
							}					
						}						
						
						if(typechoice == 4)
							break;
						
					}
				}
			}
			
			if (optionchoice == 1 && changechoice == 3){
				system("cls");
				cout << "Enter two city ids that are connected by flight. (from, to)" << endl;
				displayCities();
				int cityid1, cityid2;
				while (!(cin >> cityid1 && cin >> cityid2) /* error checking for non-int */&& cout << endl << endl || cityid1 < 0 || cityid2 < 0 || (cityid1 >= cities.getLength() + 1) || (cityid2 >= cities.getLength() + 1) || cityid1 == cityid2) { 
					cout << endl << "Enter a valid option" << endl << endl;
					cin.clear();
					cin.ignore();
				}				
				enterMatrix(cityid1,cityid2);
				cout << "Data added!" << endl;
				system("pause");
			}
			
			if (optionchoice == 2 && changechoice == 1){
				system("cls");
				cout << "Enter a city id to remove." << endl;
				displayCities();
				cout << "Enter 99 to go back" << endl;
				
				
				int removecity;
				while (!(cin >> removecity) /* error checking for non-int */&& cout << endl << endl || removecity < 0 || (removecity >= cities.getLength() + 1 && (removecity != 99))) { 
						cout << endl << "Enter a valid option" << endl << endl;
						cin.clear();
						cin.ignore();
					}
								
					
				if (removecity == 99)
					break;
				
				cities.remove(removecity);
				for (int i = 0; i < cities.getLength(); i++){ // to ensure the index of cities and cityid is related
					City* temp = new City;
					*temp = cities[i];
					temp->setid(i + 1);
					cities.setEntry(i + 1,*temp);
					delete temp;				
				}
				
				// change flight data
				int** newMatrix;
				newMatrix = createMatrix();
				int tempX = 0; // counting system to add data into newMatrix without index error.
				int tempY = 0;
				for (int i = 0; i < cities.getLength() + 1; i++){
					for (int j = 0; j < cities.getLength() + 1; j++){
						if((i != removecity - 1)&&(j != removecity - 1)) {// dont copy the removed city flight information
							newMatrix[tempX][tempY] = matrix[i][j];
							tempY += 1;
						}
					}
					if((i != removecity - 1))
						tempX += 1;
					tempY = 0;
				}
				
				// clean up for old matrix data
				for (int i = 0; i < cities.getLength() + 1 ; i++){
					delete[] matrix[i];
				}
				delete[] matrix;
				
				matrix = newMatrix;
				
				// for (int i = 0; i < cities.getLength() ; i++){
					// for (int j = 0; j < cities.getLength() ; j++)
						// cout <<  matrix[i][j];
					// cout << endl;
				// }				
				cout << "Data removed!" << endl;
				system("Pause");
			}
			
			if (optionchoice == 2 && changechoice == 2){
				int citytoremove, attractiontoremove;
				bool next;
				next = false;
				system("cls");
				cout << endl;
				cout << "Enter a city id to remove attraction from." << endl;
				displayCities();
				cout << "Enter 99 to go back" << endl << endl;
				while (!(cin >> citytoremove) /* error checking for non-int */&& cout << endl << endl || citytoremove < 0 || (citytoremove >= cities.getLength() + 1 && (citytoremove != 99))) { 
					cout << endl << "Enter a valid option" << endl << endl;
					cin.clear();
					cin.ignore();
				}
				if (citytoremove <= cities.getLength() && citytoremove > 0)
					next = true;
				
				if (citytoremove == 99)
					break;
				

				system("cls");
				cout << "Choose an attraction id to remove" << endl;
				returnAttraction(citytoremove,4);
				cout << "Enter 99 to go back" << endl;
				while (!(cin >> attractiontoremove) /* error checking for non-int */&& cout << endl << endl || attractiontoremove < 0 || (attractiontoremove > cities[citytoremove-1].attractions.getLength() && (attractiontoremove != 99))) { 
					cout << endl << "Enter a valid option" << endl << endl;
					cin.clear();
					cin.ignore();
				}					
				
				cout << endl;
				
				City* temp = new City;
				*temp = cities[citytoremove-1];
				temp->attractions.remove(attractiontoremove);
				
				for (int i = 0; i < temp->attractions.getLength(); i++){ // to ensure the there is no gaps between the attraction id.
					temp->attractions[i]->setid(i + 1);			
				}	
				
				cities.setEntry(citytoremove,*temp);						
				delete temp;						
			
				
				if(attractiontoremove == 99)
					break;
				
				cout << "Data removed!" << endl;
				system("pause");
				break;
				
				
			}
			
			if (optionchoice == 2 && changechoice == 3){
				system("cls");
				cout << "Enter two city ids that are connected by flight to remove the flight data.(from, to)" << endl;
				displayCities();
				int cityid1, cityid2;
				while (!(cin >> cityid1 && cin >> cityid2) /* error checking for non-int */&& cout << endl << endl || cityid1 < 0 || cityid2 < 0 || (cityid1 >= cities.getLength() + 1) || (cityid2 >= cities.getLength() + 1) || cityid1 == cityid2) { 
					cout << endl << "Enter a valid option" << endl << endl;
					cin.clear();
					cin.ignore();
				}				
				matrix[cityid1-1][cityid2-1] = 0;
				cout << "Data removed!" << endl;
				system("pause");
			}
			
			if (optionchoice == 3 && changechoice == 1){
				system("cls");
				cout << "Enter a city id to change the city name. However the flight information and the attractions will be carried over." << endl;
				displayCities();
				cout << "Enter \"back\" to go back" << endl;
				int changecity;
				while (!(cin >> changecity) /* error checking for non-int */&& cout << endl << endl || changecity < 0 || (changecity >= cities.getLength() + 1 )) { 
						cout << endl << "Enter a valid option" << endl << endl;
						cin.clear();
						cin.ignore();
					}
				cout << "Enter a name to change into" << endl;
				string changeName;
				cin.ignore();
				getline(cin,changeName);
				
				City* temp = new City;
				*temp = cities[changecity - 1];
				temp->changeCName(changeName);
				cities.setEntry(changecity,*temp);
				delete temp;
				cout << "Data changed!" << endl;
				system("pause");
							
			}
			
			if (optionchoice == 3 && changechoice == 2){
				while (optionforchange == true) {
					int citychoice, typechoice, attractionchoice;
					bool next;
					next = false;
					system("cls");
					cout << endl;
					cout << "Enter a city id to change the attraction from." << endl;
					displayCities();
					cout << "Enter 99 to go back" << endl << endl;
					while (!(cin >> citychoice) /* error checking for non-int */&& cout << endl << endl || citychoice < 0 || (citychoice >= cities.getLength() + 1 && (citychoice != 99))) { 
						cout << endl << "Enter a valid option" << endl << endl;
						cin.clear();
						cin.ignore();
					}
					if (citychoice <= cities.getLength() && citychoice > 0)
						next = true;
					
					if (citychoice == 99)
						break;
					
					while (next == true) {
						system("cls");
						cout << "Enter an attraction id to change the data. " << endl;
						returnAttraction(citychoice,4);
						while (!(cin >> attractionchoice)/* error checking for non-int */ && cout << endl << endl || attractionchoice < 1 || (attractionchoice > cities[citychoice - 1].attractions.getLength())) {
							cout << endl << "Enter a valid option" << endl << endl;
							cin.clear();
							cin.ignore();
						}
						cout << endl;
						
						if (cities[citychoice - 1].attractions[attractionchoice - 1]->gettypeID() == 1){
							cout << "Enter a sport attraction name." << endl;
							string addName,details;
							int ageLimit;
							cin.ignore();
							getline(cin,addName);
							cout << "Enter the ageLimit for this attraction." << endl;
							while (!(cin >> ageLimit) && cout << endl << endl || ageLimit < 0 || (ageLimit > 122)) {
								cout << endl << "Enter a valid option" << endl << endl;
								cin.clear();
								cin.ignore();
							}
							cout << "Enter the details for this attraction" << endl;
							cin.ignore();
							getline(cin,details);							
							if (addName != ""){
								Attraction* newsport = new Sport(addName, attractionchoice, 1, ageLimit);
								newsport->addDetail(details);
								City* temp = new City;
								*temp = cities[citychoice - 1];
								temp->attractions.setEntry(attractionchoice,newsport);
								cities.setEntry(citychoice,*temp);
								delete temp;
								cout << "Data changed!" << endl;
								system("pause");
								optionforchange = false;
								next = false;
							}					
						}
						
						if (cities[citychoice - 1].attractions[attractionchoice - 1]->gettypeID() == 2){
							cout << "Enter a culture attraction name." << endl;
							string addName,details;
							double entrancefee;
							cin.ignore();
							getline(cin,addName);
							cout << "Enter the entrance fee for this attraction." << endl;
							while (!(cin >> entrancefee) && cout << endl << endl ) {
								cout << endl << "Enter a valid option" << endl << endl;
								cin.clear();
								cin.ignore();
							}
							cout << "Enter the details for this attraction" << endl;
							cin.ignore();
							getline(cin,details);							
							if (addName != ""){
								Attraction* newCulture = new Culture(addName, attractionchoice, 2, entrancefee);
								newCulture->addDetail(details);
								City* temp = new City;
								*temp = cities[citychoice - 1];
								temp->attractions.setEntry(attractionchoice,newCulture);
								cities.setEntry(citychoice,*temp);
								delete temp;
								cout << "Data changed!" << endl;
								system("pause");
								optionforchange = false;
								next = false;
							}					
						}

						if (cities[citychoice - 1].attractions[attractionchoice - 1]->gettypeID() == 3){
							cout << "Enter a shopping type attraction name." << endl;
							string addName,details;
							cin.ignore();
							getline(cin,addName);
							cout << "Enter the details for this attraction" << endl;
							getline(cin,details);							
							if (addName != ""){
								Shopping* newShopping = new Shopping(addName, attractionchoice, 3);
								newShopping->addDetail(details);
								cout << "Enter the name of the malls in the area. Enter \"back\" to stop adding" << endl;
								string nameOfMall;
								getline(cin,nameOfMall);
								if (nameOfMall != "back")
									cout << endl << "Enter Next Mall:" << endl;
								while (nameOfMall != "back"){
									newShopping->addMalls(nameOfMall);									
									getline(cin,nameOfMall);
									if (nameOfMall != "back")
										cout << endl << "Enter Next Mall:" << endl;
								}
								City* temp = new City;
								*temp = cities[citychoice - 1];
								temp->attractions.setEntry(attractionchoice,newShopping);
								cities.setEntry(citychoice,*temp);
								delete temp;
								cout << "Data changed!" << endl;
								system("pause");
								optionforchange = false;
								next = false;
							}					
						}						
						
						if(typechoice == 4)
							break;
						
					}
				}
			}
			
			if (optionchoice == 3 && changechoice == 3){
				system("cls");
				cout << "Enter two city ids that are connected by flight to change the flight data. Connected will be unconnected and vice versa. (from, to)" << endl;
				displayCities();
				int cityid1, cityid2;
				while (!(cin >> cityid1 && cin >> cityid2) /* error checking for non-int */&& cout << endl << endl || cityid1 < 0 || cityid2 < 0 || (cityid1 >= cities.getLength() + 1) || (cityid2 >= cities.getLength() + 1) || cityid1 == cityid2) { 
					cout << endl << "Enter a valid option" << endl << endl;
					cin.clear();
					cin.ignore();
				}				
				if (matrix[cityid1-1][cityid2-1] = 0)
					matrix[cityid1-1][cityid2-1] = 1;
				else if (matrix[cityid1-1][cityid2-1] = 1)
					matrix[cityid1-1][cityid2-1] = 0;
				cout << "Data changed!" << endl;
				system("pause");
			}
			
			if (changechoice == 4){
				changemenu = false;
				break;
			}
				
		}
	}
	
};

int main()
{	
	
	
	LoadFile("abcde");
	MainMenu(cities );


    return 0;
}