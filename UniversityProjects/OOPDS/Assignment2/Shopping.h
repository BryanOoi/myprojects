#ifndef _Shopping
#define _Shopping
#include <string>
#include "LinkedList.h"
#include <iostream>
#include "Attraction.h"

using namespace std; 

class Shopping : public Attraction { 
private:
	LinkedList<string> malls;
public:
	Shopping(string name , int id , int typeID);
	void showMalls(LinkedList<string> malls, int size);
	void addMalls(string mallname);
	virtual void display();
	LinkedList<string> getmalls();


};

#include "Shopping.cpp"
#endif