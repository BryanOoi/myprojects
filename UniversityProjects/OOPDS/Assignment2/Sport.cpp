#include "Sport.h"

Sport :: Sport(string name = "Null", int id = 0, int typeID = 0, int ageLimit = 0): Attraction(name, id, typeID), ageLimit(ageLimit){}

void Sport :: display()/* Shows the important info for the attraction*/ { 
	cout << "This sport attraction has an age limit of " << ageLimit << endl; 
	cout << getDetail() << endl;
	}
	
int Sport :: getageLimit(){return ageLimit;};