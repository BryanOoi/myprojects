#ifndef _Culture
#define _Culture
#include <iostream>
#include "Attraction.h"

using namespace std;

class Culture: public Attraction {
private:
	double entranceFee;
public:
	Culture(string name , int id , int typeID , double entranceFee);
	virtual void display();
	double getentranceFee();

};

#include "Culture.cpp"
#endif