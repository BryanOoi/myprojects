function SimulationTable(ProbList,ProbSize,ProbListb,ProbSizeb,ProbListc,ProbSizec,ProbList2,ProbSize2,ProbList3,ProbSize3,ServiceTimeLista,ServiceTimeListb,ServiceTimeListc,ArrivalTimeList,ItemNumberList,NumberOfCustomers,ServiceTime,ArrivalTime,ItemType,ItemPrice,NumberOfServer,MaximumQuantity)
    % MaximumQuantity - Maximum quantity of items to purchase.
    
    % initialise all the necessary arrays for all the columns
    
    % ProbList - ServiceTimea- server1
    % ProbList2 - ArrivalTimeList - arrival
    % ProbListb - ServiceTimeb - server2
    % ProbListc - ServiceTimec - server3
    % ProbList3 - ItemNumberList -item
    
    toShow = ones([1,NumberOfCustomers]);% default show values in server1
    
    RNIA = ArrivalTime; % Random Number for InterArrival Time
    
    IAT = 1:NumberOfCustomers; % InterArrival Time generated from the random numbers e.g 1/2/3/4
    for i = 1:NumberOfCustomers;
        IAT(i) = returnMinute(ProbList2,ProbSize2,ArrivalTimeList,RNIA(i));
    end
    
    AT = 1:NumberOfCustomers; % Cumulative Arrival Time
    
    RNST = ServiceTime; % Random Number for Service Time
    RNI = ItemType; % Random Number for Item
    
    ST = 1:NumberOfCustomers; % Service Time for server 1 . ST2 and ST3 later will be for server 2 and 3

     
    TSB = 1:NumberOfCustomers; % Time service Begin for server 1 ,TSB2 and TSB3 later will be for server 2 and 3
    TSE = 1:NumberOfCustomers; % Time service End for server 1 ,TSE2 and TSE3 later will be for server 2 and 3
    IN = 1:NumberOfCustomers; % Item number e.g 1/2/3/4
    WT = 1:NumberOfCustomers; % Waiting time
    TS = 1:NumberOfCustomers; % Time spend 
    
    Arrivalminute = 0;
    ServiceBegin = 0;
    ServiceEnd = 0;
    
    for i = 1:NumberOfCustomers
        Arrivalminute = Arrivalminute + returnMinute(ProbList2,ProbSize2,ArrivalTimeList,ArrivalTime(i));
   
   
        if ServiceEnd > Arrivalminute 
            ServiceBegin = ServiceEnd;
            WT(i) = ServiceEnd - Arrivalminute;
        elseif ServiceEnd <= Arrivalminute
            ServiceBegin = Arrivalminute;
            WT(i) = 0;
        end
   
        ServiceEnd = ServiceBegin + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
        
        Item = returnMinute(ProbList3,ProbSize3,ItemNumberList,ItemType(i));
        
        AT(i) = Arrivalminute;
        TSB(i) = ServiceBegin;
        ST(i) = returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
        TSE(i) = ServiceEnd;
        IN(i) = Item;
        TS(i) = WT(i) + ST(i);
    end
    
    QN = floor(MaximumQuantity*rand(1,NumberOfCustomers)); 
    TP = 1:NumberOfCustomers;
    
    
    for i = 1:NumberOfCustomers
        TP(i) = QN(i) * ItemPrice(IN(i));
        
    end
    
    if NumberOfServer == 1
    % Draw table for 1 server
        printf('__________________________________________________________________________________________\n')
        printf('|n |RN for |IA  |Arrival|RN for |Service|Time   |Time   |Wait|Time |Rn  |Item  |Quan|Total|\n');
        printf('|  |IA time|time|time   |Service|time   |service|service|ing |spend|for |number|tity|price|\n');
        printf('|  |       |    |       |time   |       |begins |ends   |time|     |item|      |    |     |\n');
    
        for count = 1:NumberOfCustomers
            printf('__________________________________________________________________________________________\n')
            a = num2str(count);
            b = num2str(RNIA(count));
            c = num2str(IAT(count));
            d = num2str(AT(count));
            e = num2str(RNST(count));
            f = num2str(ST(count));
            g = num2str(TSB(count));
            h = num2str(TSE(count));
            i = num2str(WT(count));
            j = num2str(TS(count));
            k = num2str(RNI(count));
            l = num2str(IN(count));
            m = num2str(QN(count));
            n = num2str(TP(count));
            printf('|%2s|%-7s|%-4s|%-7s|%-7s|%-7s|%-7s|%-7s|%-4s|%-5s|%-4s|%-6s|%-4s|%-5s|\n',a,b,c,d,e,f,g,h,i,j,k,l,m,n)
        end
    
        printf('__________________________________________________________________________________________\n')
   
    
    
    
    elseif NumberOfServer == 2
        
        Arrivalminute = 0;
        ServiceBegin1 = 0;
        ServiceBegin2 = 0;
        ServiceEnd1 = 0;
        ServiceEnd2 = 0;
    
       
       for i = 1:NumberOfCustomers
           Arrivalminute = Arrivalminute + returnMinute(ProbList2,ProbSize2,ArrivalTimeList,ArrivalTime(i));
       
           if ServiceEnd1 <= Arrivalminute % if server 1 is free
                ServiceBegin1 = Arrivalminute; % these blocks are to display the adequate rows
                ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                WT(i) = 0;
                toShow(i) = 1; % only display the relevant column
                TS(i) = returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                
           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 <= Arrivalminute % if server 1 is busy and server 2 is free
                ServiceBegin2 = Arrivalminute;
                ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                WT(i) = 0;
                toShow(i) = 2;
                TS(i) = returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));

           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 > Arrivalminute % if server 1 and 2 is busy
               QueueToGo = [ServiceEnd1,ServiceEnd2]; 
               NextFreeServer = find(QueueToGo == min(QueueToGo),1); % find the next free server with the lowest ServiceTimeEnd
           
               if NextFreeServer == 1    
               
                   ServiceBegin1 = ServiceEnd1;
                   WT(i) = ServiceEnd1 - Arrivalminute;
                   ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                   toShow(i) = 1;
                   TS(i) = WT(i) + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                   
               elseif NextFreeServer == 2
                   
                   ServiceBegin2 = ServiceEnd2;
                   WT(i) = ServiceEnd2 - Arrivalminute;
                   ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                   toShow(i) = 2;
                   TS(i) = WT(i) + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
               end
                
                
           end
           
           
           
           
      
        
            Item = returnMinute(ProbList3,ProbSize3,ItemNumberList,ItemType(i));
            
            AT(i) = Arrivalminute;
            TSB(i) = ServiceBegin1;
            TSB2(i) = ServiceBegin2;
            ST(i) = returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
            ST2(i) = returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
            TSE(i) = ServiceEnd1;
            TSE2(i) = ServiceEnd2;
            IN(i) = Item;
            
        end
        
        
        
        
        
        
        printf('______________________________________________________________________________________________________________\n')
        printf('|                               |Server1             |Server2             |                                 |\n')
        printf('______________________________________________________________________________________________________________\n')
        printf('|n |RN for |IA  |Arrival|RN for |Time   |S.  |Time   |Time   |S.  |Time   |Wait|Time |Rn  |Item  |Quan|Total|\n');
        printf('|  |IA time|time|time   |Service|service|Time|service|service|Time|service|ing |spend|for |number|tity|price|\n');
        printf('|  |       |    |       |time   |begins |    |ends   |begins |    |ends   |time|     |item|      |    |     |\n');
        for count = 1:NumberOfCustomers
            printf('______________________________________________________________________________________________________________\n')
            a = num2str(count);
            b = num2str(RNIA(count));
            c = num2str(IAT(count));
            d = num2str(AT(count));
            e = num2str(RNST(count));
            f = num2str(ST(count));
            g = num2str(TSB(count));
            h = num2str(TSE(count));
            i = num2str(WT(count));
            j = num2str(TS(count));
            k = num2str(RNI(count));
            l = num2str(IN(count));
            m = num2str(QN(count));
            n = num2str(TP(count));
            f2 = num2str(ST2(count));
            x = num2str(TSB2(count));
            y = num2str(TSE2(count));
            
            
            if toShow(count) == 1 % dont display server 2 data if server 1 is handling the queue
                x = '';
                y = '';
                f2 = '';
            elseif toShow(count) == 2; % vice versa
                f = '';
                g = '';
                h = '';   
            end
            printf('|%2s|%-7s|%-4s|%-7s|%-7s|%-7s|%-4s|%-7s|%-7s|%-4s|%-7s|%-4s|%-5s|%-4s|%-6s|%-4s|%-5s|\n',a,b,c,d,e,g,f,h,x,f2,y,i,j,k,l,m,n)
        end
    
        printf('______________________________________________________________________________________________________________\n')       

    
   
   
   
   
    elseif NumberOfServer == 3
        
        Arrivalminute = 0;
        ServiceBegin1 = 0;
        ServiceBegin2 = 0;
        ServiceBegin3 = 0;
        ServiceEnd1 = 0;
        ServiceEnd2 = 0;
        ServiceEnd3 = 0;
    
       
       for i = 1:NumberOfCustomers
           Arrivalminute = Arrivalminute + returnMinute(ProbList2,ProbSize2,ArrivalTimeList,ArrivalTime(i));
       
           if ServiceEnd1 <= Arrivalminute % if server 1 is free
                ServiceBegin1 = Arrivalminute; % these blocks are to display the adequate rows
                ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                WT(i) = 0;
                toShow(i) = 1; % only display the relevant column
                TS(i) = returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                
           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 <= Arrivalminute % if server 1 is busy and server 2 is free
                ServiceBegin2 = Arrivalminute;
                ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                WT(i) = 0;
                toShow(i) = 2;
                TS(i) = returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));

           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 > Arrivalminute & ServiceEnd3 <= Arrivalminute % if server 1 is busy and server 2 is busy , server 3 is free
                ServiceBegin3 = Arrivalminute;
                ServiceEnd3 = ServiceBegin3 + returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));
                WT(i) = 0;
                toShow(i) = 3;
                TS(i) = returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));
                
           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 > Arrivalminute & ServiceEnd3 > Arrivalminute  % if all are busy  
                   
               QueueToGo = [ServiceEnd1,ServiceEnd2,ServiceEnd3]; 
               NextFreeServer = find(QueueToGo == min(QueueToGo),1); % find the next free server with the lowest ServiceTimeEnd
           
               if NextFreeServer == 1    
               
                   ServiceBegin1 = ServiceEnd1;
                   WT(i) = ServiceEnd1 - Arrivalminute;
                   ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                   toShow(i) = 1;
                   TS(i) = WT(i) + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                   
               elseif NextFreeServer == 2
                   ServiceBegin2 = ServiceEnd2;
                   WT(i) = ServiceEnd2 - Arrivalminute;
                   ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                   toShow(i) = 2;
                   TS(i) = WT(i) + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                   
               elseif NextFreeServer == 3
                   ServiceBegin3 = ServiceEnd3;
                   WT(i) = ServiceEnd3 - Arrivalminute;
                   ServiceEnd3 = ServiceBegin3 + returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));
                   toShow(i) = 3;
                   TS(i) = WT(i) + returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));                 
               end        
           
           end
           
           
           
           
      
        
            Item = returnMinute(ProbList3,ProbSize3,ItemNumberList,ItemType(i));
            
            AT(i) = Arrivalminute;
            TSB(i) = ServiceBegin1;
            TSB2(i) = ServiceBegin2;
            TSB3(i) = ServiceBegin3;
            ST(i) = returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
            ST2(i) = returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
            ST3(i) = returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));
            TSE(i) = ServiceEnd1;
            TSE2(i) = ServiceEnd2;
            TSE3(i) = ServiceEnd3;
            IN(i) = Item;
            
        end
        
        
        
        
        
        
        printf('_________________________________________________________________________________________________________________________________\n')
        printf('|                               |Server1             |Server2             |Server3             |                                 |\n')
        printf('_________________________________________________________________________________________________________________________________\n')
        printf('|n |RN for |IA  |Arrival|RN for |Time   |S.  |Time   |Time   |S.  |Time   |Time   |S.  |Time   |Wait|Time |Rn  |Item  |Quan|Total|\n');
        printf('|  |IA time|time|time   |Service|service|Time|service|service|Time|service|service|Time|service|ing |spend|for |number|tity|price|\n');
        printf('|  |       |    |       |time   |begins |    |ends   |begins |    |ends   |begins |    |ends   |time|     |item|      |    |     |\n');
        for count = 1:NumberOfCustomers
            printf('_________________________________________________________________________________________________________________________________\n')
            a = num2str(count);
            b = num2str(RNIA(count));
            c = num2str(IAT(count));
            d = num2str(AT(count));
            e = num2str(RNST(count));
            f = num2str(ST(count));
            g = num2str(TSB(count));
            h = num2str(TSE(count));
            i = num2str(WT(count));
            j = num2str(TS(count));
            k = num2str(RNI(count));
            l = num2str(IN(count));
            m = num2str(QN(count));
            n = num2str(TP(count));
            f2 = num2str(ST2(count));
            x = num2str(TSB2(count));
            y = num2str(TSE2(count));
            ab = num2str(ST3(count));
            ac = num2str(TSB3(count));
            ad = num2str(TSE3(count));
            
            
            if toShow(count) == 1 % only display server 1 data in the row. 
                x = '';
                y = '';
                f2 = '';
                ab = '';
                ac = '';
                ad = '';
            elseif toShow(count) == 2;
                f = '';
                g = '';
                h = '';
                ab = '';
                ac = '';
                ad = '';
            elseif toShow(count) == 3;
                f = '';
                g = '';
                h = '';
                x = '';
                y = '';
                f2 = '';             
            end
            printf('|%2s|%-7s|%-4s|%-7s|%-7s|%-7s|%-4s|%-7s|%-7s|%-4s|%-7s|%-7s|%-4s|%-7s|%-4s|%-5s|%-4s|%-6s|%-4s|%-5s|\n',a,b,c,d,e,g,f,h,x,f2,y,ac,ab,ad,i,j,k,l,m,n)
        end
    
        printf('_________________________________________________________________________________________________________________________________\n')       
    
        
    end
    
    % Display evaluations of the table.
    printf('\nThe average time a customer has to wait in queue is %.3f minutes.\n', sum(WT)/NumberOfCustomers);
    
    waitingCustomers = 0; % counting the number of customers who has to wait
    
 
    for i = 1:NumberOfCustomers
        if WT(i) > 0
            waitingCustomers = waitingCustomers + 1;
        end
    end
    
    printf('Probability customer has to wait in queue is %.3f.\n',waitingCustomers / NumberOfCustomers);
    

    TotalSTime = 0; % calculating the service time in total, have to distinguish which server was doing the service.
    for i = 1:NumberOfCustomers
        if toShow(i) == 1
            TotalSTime = TotalSTime + ST(i);
        elseif toShow(i) == 2
            TotalSTime = TotalSTime + ST2(i);
        elseif toShow(i) == 3
            TotalSTime = TotalSTime + ST3(i);
        end
    end
    
    printf('Average Service Time is %.3f minutes.\n',TotalSTime/NumberOfCustomers);
    
    printf('Average Interarrival Time is %.3f minutes.\n',sum(IAT)/(NumberOfCustomers - 1));
    
    printf('Average Time a customer spend in system is %.3f minutes.\n',sum(TS)/NumberOfCustomers);
    
    
    if NumberOfServer == 1
        printf('Total Sales Generated is RM%.2f\n',sum(TP));
    
        printf('\n\n In conclusion:  ');
        
        if(sum(WT)/NumberOfCustomers > TotalSTime/NumberOfCustomers ) 
            printf('The average waiting time for each customer is higher than their time being serviced,therefore more servers are needed to reduce the waiting time\n');
        elseif(sum(WT)/NumberOfCustomers < 1)
            printf('The customers waited for an average of less than 1 minute. This shows low peak hour and additional servers will not provide any benefit.\n');
        elseif(TotalSTime/NumberOfCustomers > sum(WT)/NumberOfCustomers)
            printf('The customers do not have to wait as long as their service time. More servers can help but it is not necassarily needed.\n');
        
         
        end   
        
        
        
    elseif NumberOfServer == 2
        
        TimeServer1Busy = 0;
        TimeServer2Busy = 0;
        TotalSalesServer1 = 0;
        TotalSalesServer2 = 0;
       
        for i = 1:NumberOfCustomers
            if toShow(i) == 1
                TimeServer1Busy = TimeServer1Busy + ST(i);
                TotalSalesServer1 = TotalSalesServer1 + TP(i);
            elseif toShow(i) == 2
                TimeServer2Busy = TimeServer2Busy + ST2(i);
                TotalSalesServer2 = TotalSalesServer2 + TP(i);             
            end
        end
        
        TimeElapsed = max([TSE(NumberOfCustomers),TSE2(NumberOfCustomers)]);
        
        
        printf('Percentage of time server 1 is busy is %.3f percent \n',(TimeServer1Busy/TimeElapsed) * 100);
        printf('Percentage of time server 2 is busy is %.3f percent \n',(TimeServer2Busy/TimeElapsed) * 100);
        
        printf('Total Sales Generated For Server 1 is RM%.2f\n',TotalSalesServer1);
        printf('Total Sales Generated For Server 2 is RM%.2f\n',TotalSalesServer2);
        
        printf('\n\n In conclusion:  ');
        
        if(sum(WT)/NumberOfCustomers > TotalSTime/NumberOfCustomers ) 
            printf('The average waiting time for each customer is higher than their time being serviced,therefore more servers are needed to reduce the waiting time\n');
        elseif(sum(WT)/NumberOfCustomers < 1)
            printf('The customers waited for an average of less than 1 minute. This shows low peak hour and additional servers will not provide any benefit.\n');
        end
        
        ServerEffectiveness = [TotalSalesServer1/(TimeServer1Busy/TimeElapsed) , TotalSalesServer2/(TimeServer2Busy/TimeElapsed)];
        
        printf('The most effective server for "money generated" per "time spend on counter" is server %d\n',find(ServerEffectiveness == max(ServerEffectiveness)));
        
        
        
       
        
    elseif NumberOfServer == 3

        TimeServer1Busy = 0;
        TimeServer2Busy = 0;
        TimeServer3Busy = 0;
        TotalSalesServer1 = 0;
        TotalSalesServer2 = 0;
        TotalSalesServer3 = 0;
        
        for i = 1:NumberOfCustomers
            if toShow(i) == 1
                TimeServer1Busy = TimeServer1Busy + ST(i);
                TotalSalesServer1 = TotalSalesServer1 + TP(i);
            elseif toShow(i) == 2
                TimeServer2Busy = TimeServer2Busy + ST2(i);
                TotalSalesServer2 = TotalSalesServer2 + TP(i);  
            elseif toShow(i) == 3
                TimeServer3Busy = TimeServer3Busy + ST3(i);
                TotalSalesServer3 = TotalSalesServer3 + TP(i);  
            end
        end 
        
        TimeElapsed = max([TSE(NumberOfCustomers),TSE2(NumberOfCustomers),TSE3(NumberOfCustomers)]);
        
        printf('Percentage of time server 1 is busy is %.3f percent \n',(TimeServer1Busy/TimeElapsed) * 100);
        printf('Percentage of time server 2 is busy is %.3f percent \n',(TimeServer2Busy/TimeElapsed) * 100);
        printf('Percentage of time server 3 is busy is %.3f percent \n',(TimeServer3Busy/TimeElapsed) * 100);
        
        printf('Total Sales Generated For Server 1 is RM%.2f\n',TotalSalesServer1);
        printf('Total Sales Generated For Server 2 is RM%.2f\n',TotalSalesServer2);
        printf('Total Sales Generated For Server 3 is RM%.2f\n',TotalSalesServer3);
        
        printf('\n\n In conclusion:  ');
        
        ServerEffectiveness = [TotalSalesServer1/(TimeServer1Busy/TimeElapsed) , TotalSalesServer2/(TimeServer2Busy/TimeElapsed) , TotalSalesServer3/(TimeServer3Busy/TimeElapsed)];
        
        printf('The most effective server for "money generated" per "time spend on counter" is server %d\n',find(ServerEffectiveness == max(ServerEffectiveness)));
    
 
 
    end       
    
    