function DisplayText(ProbList,ProbSize,ProbListb,ProbSizeb,ProbListc,ProbSizec,ProbList2,ProbSize2,ProbList3,ProbSize3,ServiceTimeLista,ServiceTimeListb,ServiceTimeListc,ArrivalTimeList,ItemNumberList,NumberOfCustomers,ServiceTime,ArrivalTime,ItemType,ItemPrice,NumberOfServer)
    Arrivalminute = 0;
    ServiceBegin = 0;
    ServiceEnd = 0;

    if (NumberOfServer == 1)
        for i = 1:NumberOfCustomers
           Arrivalminute = Arrivalminute + returnMinute(ProbList2,ProbSize2,ArrivalTimeList,ArrivalTime(i));
           
           
           if ServiceEnd > Arrivalminute
               ServiceBegin = ServiceEnd;
           elseif ServiceEnd < Arrivalminute
               ServiceBegin = Arrivalminute;
           end
           
           ServiceEnd = ServiceBegin + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
           
           Item = returnMinute(ProbList3,ProbSize3,ItemNumberList,ItemType(i));
           

               
           printf('Arrival of customer number %d at minute %d \n',i,Arrivalminute);
           printf('Service of customer number %d starts at minute %d \n',i,ServiceBegin);
           printf('The Service Lasts for %d minutes .\n',returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i)));
           printf('Departure of customer number %d at minute %d \n',i,ServiceEnd);
           printf('The customer selects item/service %d .\n\n',Item);
        end
    
    elseif (NumberOfServer == 2)
        Arrivalminute = 0;
        ServiceBegin1 = 0;
        ServiceBegin2 = 0;
        ServiceEnd1 = 0;
        ServiceEnd2 = 0;
    
       
       for i = 1:NumberOfCustomers
           Arrivalminute = Arrivalminute + returnMinute(ProbList2,ProbSize2,ArrivalTimeList,ArrivalTime(i));
       
           if ServiceEnd1 <= Arrivalminute
                ServiceBegin1 = Arrivalminute;
                ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                Queue = 1;
                
           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 <= Arrivalminute
                ServiceBegin2 = Arrivalminute;
                ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                Queue = 2;

           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 > Arrivalminute
                
               QueueToGo = [ServiceEnd1,ServiceEnd2]; 
               NextFreeServer = find(QueueToGo == min(QueueToGo),1); % find the next free server with the lowest ServiceTimeEnd
           
               if NextFreeServer == 1    
               
                   ServiceBegin1 = ServiceEnd1;
                   ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                   Queue = 1;
                
                   
               elseif NextFreeServer == 2
                   
                   ServiceBegin2 = ServiceEnd2;                   
                   ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                   Queue = 2;
                   
               end
                
                
                
                
           end
           
           
      
           
      
        
            Item = returnMinute(ProbList3,ProbSize3,ItemNumberList,ItemType(i));
            

            
            
           printf('Arrival of customer number %d at minute %d \n',i,Arrivalminute);
           
           if Queue == 1
               printf('Service of customer number %d starts at minute %d at queue %d.\n',i,ServiceBegin1,1);
               printf('The Service Lasts for %d minutes .\n',returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i)));
               printf('Departure of customer number %d at minute %d \n',i,ServiceEnd1);
           elseif Queue == 2
               printf('Service of customer number %d starts at minute %d at queue %d.\n',i,ServiceBegin2,2);
               printf('The Service Lasts for %d minutes .\n',returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i)));
               printf('Departure of customer number %d at minute %d \n',i,ServiceEnd2);
           end
           

           
           printf('The customer selects item/service %d .\n\n',Item);
        
        
       end
    
   
    elseif (NumberOfServer == 3)
        
        Arrivalminute = 0;
        ServiceBegin1 = 0;
        ServiceBegin2 = 0;
        ServiceBegin3 = 0;
        ServiceEnd1 = 0;
        ServiceEnd2 = 0;
        ServiceEnd3 = 0;
    
       
       for i = 1:NumberOfCustomers
           Arrivalminute = Arrivalminute + returnMinute(ProbList2,ProbSize2,ArrivalTimeList,ArrivalTime(i));
       
           if ServiceEnd1 <= Arrivalminute
                ServiceBegin1 = Arrivalminute;
                ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                Queue = 1; % only display the relevant column
                
           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 <= Arrivalminute
                ServiceBegin2 = Arrivalminute;
                ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                Queue = 2;

           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 > Arrivalminute & ServiceEnd3 <= Arrivalminute
                ServiceBegin3 = Arrivalminute;
                ServiceEnd3 = ServiceBegin3 + returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));
                Queue = 3;
                
           elseif ServiceEnd1 > Arrivalminute & ServiceEnd2 > Arrivalminute & ServiceEnd3 > Arrivalminute    

               QueueToGo = [ServiceEnd1,ServiceEnd2,ServiceEnd3]; 
               NextFreeServer = find(QueueToGo == min(QueueToGo),1); % find the next free server with the lowest ServiceTimeEnd
           
               if NextFreeServer == 1    
               
                   ServiceBegin1 = ServiceEnd1;
                   ServiceEnd1 = ServiceBegin1 + returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i));
                   Queue = 1;
                   
               elseif NextFreeServer == 2
                   ServiceBegin2 = ServiceEnd2;
                   ServiceEnd2 = ServiceBegin2 + returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i));
                   Queue = 2;
                   
               elseif NextFreeServer == 3
                   ServiceBegin3 = ServiceEnd3;
                   ServiceEnd3 = ServiceBegin3 + returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i));
                   Queue = 3;                
               end       
           
           end
           
           
           Item = returnMinute(ProbList3,ProbSize3,ItemNumberList,ItemType(i));
           
           printf('Arrival of customer number %d at minute %d \n',i,Arrivalminute);
           
           if Queue == 1
               printf('Service of customer number %d starts at minute %d at queue %d.\n',i,ServiceBegin1,1);
               printf('The Service Lasts for %d minutes .\n',returnMinute(ProbList,ProbSize,ServiceTimeLista,ServiceTime(i)));
               printf('Departure of customer number %d at minute %d \n',i,ServiceEnd1);
           elseif Queue == 2
               printf('Service of customer number %d starts at minute %d at queue %d.\n',i,ServiceBegin2,2);
               printf('The Service Lasts for %d minutes .\n',returnMinute(ProbListb,ProbSizeb,ServiceTimeListb,ServiceTime(i)));
               printf('Departure of customer number %d at minute %d \n',i,ServiceEnd2);
           elseif Queue == 3
               printf('Service of customer number %d starts at minute %d at queue %d.\n',i,ServiceBegin3,3);
               printf('The Service Lasts for %d minutes .\n',returnMinute(ProbListc,ProbSizec,ServiceTimeListc,ServiceTime(i)));
               printf('Departure of customer number %d at minute %d \n',i,ServiceEnd3);
           end
           

           
           printf('The customer selects item/service %d .\n\n',Item);           
           
      
        
            
 
            
        end   
       
    
    end
    
