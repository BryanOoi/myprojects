function main()
    a = 2; % for LCG, a and b are changable
    b = 3;
    m = 100; % must be 100
    
    
    % Every value below are changable according to specification of the queue.
  
    % Data for server 1
    
    ProbSize = 5;
    ProbList = [0.3,0.14,0.14,0.25,0.17];
    ServiceTimeLista = [5,6,7,8,9]; 
    
    % Data for server 2
    
    ProbSizeb = 4;
    ProbListb = [0.35,0.25,0.20,0.20];
    ServiceTimeListb = [4,5,6,7];
    
    % Data for server 3
    
    ProbSizec = 4;
    ProbListc = [0.35,0.25,0.20,0.20]; 
    ServiceTimeListc = [3,4,5,6];
    
    % Data for Arrival Time
     
    
    ProbSize2 = 4;
    ProbList2 = [0.25,0.40,0.20,0.15];
    ArrivalTimeList = [1,2,3,4];
    
    % Data for Item Type
    
    ProbSize3 = 4;
    ProbList3 = [0.25,0.25,0.25,0.25];
    ItemPrice = [1.62,5.78,3.74,10.25] ;
    ItemNumberList = 1:ProbSize3; % must be in order for simulation table to work
    MaximumQuantity = 10; % Randomly generated up to this number of quantity of items bought
    
    %%%%%%% USER INPUT BLOCK
    
    NumberOfServer = input('Choose 1 for off peak hours,2 for moderate peak hours, or 3 for high peak hours --- ');
    NumberOfCustomers = input('Select the number of Customers --- ');
    RandomGenMethod = input('Select a random number generation method. (1 for LCG,2 for FreeMat rand function) --- ');
    
    if RandomGenMethod == 1  % Produce a matrix of 1:NoOfCusters of random numbers by LCG
        ServiceTime = 1:NumberOfCustomers;
        %ServiceTime(1) = randi(1,100);
        ServiceTime(1) = 15;
        for i = 2:NumberOfCustomers;
            ServiceTime(i) = mod(a*ServiceTime(i-1)+ b, 100);
        end
        ArrivalTime = 1:NumberOfCustomers;
        ArrivalTime(1) = randi(1,100);
        %ArrivalTime(1) = 15;
        for i = 2:NumberOfCustomers;
            ArrivalTime(i) = mod(a*ArrivalTime(i-1)+ b, 100);
        end
        ItemType = 1:NumberOfCustomers;
        ItemType(1) = randi(1,100);
        %ItemType(1) = 15;
        for i = 2:NumberOfCustomers;
            ItemType(i) = mod(a*ItemType(i-1)+ b, 100);
        end
    end
    
    if RandomGenMethod == 2 % Produce a matrix of 1:NoOfCusters of random numbers by rand function from 1 to 100
        ServiceTime = floor(100*rand(1,NumberOfCustomers)) + 1;
        ArrivalTime = floor(100*rand(1,NumberOfCustomers)) + 1; 
        ItemType = floor(100*rand(1,NumberOfCustomers)) + 1;    
    end
  
    ArrivalTime(1) = 0; % 1st Arrival time is always 0
    
    
%     ServiceTime(1) = 95; 
%     ServiceTime(2) = 21;
%     ServiceTime(3) = 51;
%     ServiceTime(4) = 92;
%     ServiceTime(5) = 89;
%     
%     
%     ArrivalTime(1) = 0;
%     ArrivalTime(2) = 26;
%     ArrivalTime(3) = 98;
%     ArrivalTime(4) = 90;
%     ArrivalTime(5) = 26;
%     
%     ItemType(1) = 40;
%     ItemType(2) = 60;
%     ItemType(3) = 20;
%     ItemType(4) = 80;
%     ItemType(5) = 67;
    
    %%%%%%% DISPLAY TEXT
    
    printf('\n\n');
    
    DisplayText(ProbList,ProbSize,ProbListb,ProbSizeb,ProbListc,ProbSizec,ProbList2,ProbSize2,ProbList3,ProbSize3,ServiceTimeLista,ServiceTimeListb,ServiceTimeListc,ArrivalTimeList,ItemNumberList,NumberOfCustomers,ServiceTime,ArrivalTime,ItemType,ItemPrice,NumberOfServer)
    
    
    %%%%%%% DISPLAY TABLES 
    
    printf('Table for Service Time\n\n');
    if NumberOfServer == 1
        serviceTable2(ProbList,ProbSize,ServiceTimeLista);
    elseif NumberOfServer == 2
        printf('Service Table for server 1.\n\n')
        serviceTable2(ProbList,ProbSize,ServiceTimeLista);
        printf('Service Table for server 2.\n\n')
        serviceTable2(ProbListb,ProbSizeb,ServiceTimeListb);
    elseif NumberOfServer == 3
        printf('Service Table for server 1.\n\n')
        serviceTable2(ProbList,ProbSize,ServiceTimeLista);
        printf('Service Table for server 2.\n\n')
        serviceTable2(ProbListb,ProbSizeb,ServiceTimeListb);
        printf('Service Table for server3. \n\n')
        serviceTable2(ProbListc,ProbSizec,ServiceTimeListc);
    end        
    
    printf('\nTable for Inter-Arrival Time\n\n')
    ArrivalTable2(ProbList2,ProbSize2,ArrivalTimeList);
    
    printf('\nTable for Item Type and Price\n\n')
    ItemTable2(ProbList3,ProbSize3,ItemNumberList,ItemPrice);
    
%     ServiceTime(1) = 95; 
%     ServiceTime(2) = 21;
%     ServiceTime(3) = 51;
%     ServiceTime(4) = 92;
%     ServiceTime(5) = 89;
%     ServiceTime(6) = 38;
%     ServiceTime(7) = 13;
%     ServiceTime(8) = 61;
%     ServiceTime(9) = 50;
%     ServiceTime(10) = 49;
%     ServiceTime(11) = 39;
%     ServiceTime(12) = 53;
%    
%     
%     ArrivalTime(1) = 0;
%     ArrivalTime(2) = 26;
%     ArrivalTime(3) = 98;
%     ArrivalTime(4) = 90;
%     ArrivalTime(5) = 26;
%     ArrivalTime(6) = 42;
%     ArrivalTime(7) = 74;
%     ArrivalTime(8) = 80;
%     ArrivalTime(9) = 68;
%     ArrivalTime(10) = 22;
%     ArrivalTime(11) = 48;
%     ArrivalTime(12) = 34;
%    
    
    printf('\nTable for simulation of queue \n\n')
    
    SimulationTable(ProbList,ProbSize,ProbListb,ProbSizeb,ProbListc,ProbSizec,ProbList2,ProbSize2,ProbList3,ProbSize3,ServiceTimeLista,ServiceTimeListb,ServiceTimeListc,ArrivalTimeList,ItemNumberList,NumberOfCustomers,ServiceTime,ArrivalTime,ItemType,ItemPrice,NumberOfServer,MaximumQuantity)