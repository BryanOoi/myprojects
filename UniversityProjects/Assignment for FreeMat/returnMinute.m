function output = returnMinute(ProbList,size,ValueList,Comparator)
    % output the arrival time/service time/ item type based on a probabilty list and a random value 'Comparator '
    if (Comparator == 0)
        output = 0; % for 1st arrival time only
    
    else 
        div = 1:size+1;
        count = 0;
        count2 = 0;
        for i = 1:size
            count = count2;
            count2 = count2 + ProbList(i) * 100;
            if (count < Comparator & Comparator <= count2)
                output = ValueList(i);
            end
        end
   
    end
 