function ItemTable2(ProbList,SizeOfProbList,ItemNumberList,feeList)
    printf('______________________________________________\n')
    printf('|Item number|Probability|CDF |Range |Price/fee|\n')
    
    % calculation for values in 3rd column
    CDF(1) = ProbList(1);
    for i = 2:SizeOfProbList
        CDF(i) = CDF(i-1) + ProbList(i);
    end    
        
    count = 0;
    count2 = 0;
    
    
    for i = 1:SizeOfProbList
        
        count = count2;
        count2 = count2 + ProbList(i) * 100;
        tempa = num2str(count + 1);
        tempb = num2str(count2);
        
        
        a = num2str(ItemNumberList(i));
        b = num2str(ProbList(i));
        c = num2str(CDF(i));
        d = [tempa,'-',tempb];
        e = num2str(feeList(i));
        printf('______________________________________________\n')
        printf('|%-11s|%-11s|%-4s|%-6s|%-9s|\n',a,b,c,d,e);
        
        
    
    end
    printf('______________________________________________\n')