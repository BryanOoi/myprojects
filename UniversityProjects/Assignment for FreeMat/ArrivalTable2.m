function ArrivalTable2(ProbList,SizeOfProbList,ArrivalTimeList)
    printf('____________________________________________\n')
    printf('|Inter-Arrival Time|Probability|CDF |Range |\n')
    
    % calculation for values in 3rd column
    CDF(1) = ProbList(1);
    for i = 2:SizeOfProbList
        CDF(i) = CDF(i-1) + ProbList(i);
    end    
        
    count = 0;
    count2 = 0;
    
    
    for i = 1:SizeOfProbList
        
        count = count2;
        count2 = count2 + ProbList(i) * 100;
        tempa = num2str(count + 1);
        tempb = num2str(count2);
        
        
        a = num2str(ArrivalTimeList(i));
        b = num2str(ProbList(i));
        c = num2str(CDF(i));
        d = [tempa,'-',tempb];
        printf('____________________________________________\n')
        printf('|%-18s|%-11s|%-4s|%-6s|\n',a,b,c,d);
        
        
    
    end
    printf('____________________________________________\n')