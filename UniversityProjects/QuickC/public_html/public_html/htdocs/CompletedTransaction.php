<?php

include "db2.php";

$totalList=mysqli_real_escape_string($conn,$_POST['totalList']);

if($totalList > 0)
{
	for ($x = 0; $x < $totalList ; $x++) {
		$stmt = $conn->prepare("INSERT INTO `completedtransaction` (`CustID`,`SuppID`,`Price`,`CarName`,`Duration`,`Completed`,`Timestamp`) VALUES (?, ?, ?, ?, ? ,? ,?)");
		$stmt2 = $conn->prepare("delete from `transaction` where `Timestamp`= ?");
		
		$stmt->bind_param("ssdssis", $CustID,$SuppID,$Price,$CarName,$Duration,$Completed,$Timestamp);
		$stmt2->bind_param("s", $Timestamp);
		
		$str1 = 'SuppID' + $x;
		$str2 = 'Duration' + $x;
		$str3 = 'Price' + $x;
		$str4 = 'CarName' + $x;
		$str5 = 'Completed' + $x;
		$str6 = 'Timestamp' + $x;
		
		$SuppID = $_POST["SuppID$x"];
		$CustID = $_POST["CustID"];
		$Duration = $_POST["Duration$x"];
		$Price = $_POST["Price$x"];
		$CarName = $_POST["CarName$x"];
		$Completed = $_POST["completed$x"];
		$Timestamp = $_POST["Timestamp$x"];
		
		$stmt->execute();
		$stmt2->execute();
		
		$stmt->close();
		$stmt2->close();
	 }
} 



?>